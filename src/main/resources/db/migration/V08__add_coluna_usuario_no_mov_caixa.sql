ALTER TABLE movimento_caixa ADD COLUMN id_usuario BIGINT(20) DEFAULT NULL;
ALTER TABLE movimento_caixa ADD CONSTRAINT fk_movimentp_caixa_usuario FOREIGN KEY(id_usuario) REFERENCES usuario (id);