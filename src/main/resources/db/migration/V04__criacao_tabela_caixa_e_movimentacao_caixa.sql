CREATE TABLE caixa(
	id_caixa bigint(20) NOT NULL AUTO_INCREMENT,
	data_abertura datetime DEFAULT NULL,
	data_fechamento datetime DEFAULT NULL,
	valor_abertura decimal(15,6) DEFAULT NULL,
	PRIMARY KEY (id_caixa)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE movimento_caixa(
	id_movimento_caixa bigint(20) NOT NULL AUTO_INCREMENT,
	hora datetime DEFAULT NULL,
	valor_movimento_caixa decimal(15,6) DEFAULT NULL,
	id_caixa bigint(20) NOT NULL,
	PRIMARY KEY (id_movimento_caixa),
	KEY fk_movimento_caixa_caixa (id_caixa),
	CONSTRAINT fk_movimento_caixa_caixa FOREIGN KEY (id_caixa) REFERENCES caixa (id_caixa)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;