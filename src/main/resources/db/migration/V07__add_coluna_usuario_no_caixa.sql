ALTER TABLE caixa ADD COLUMN id_usuario BIGINT(20) DEFAULT NULL;
ALTER TABLE caixa ADD CONSTRAINT fk_caixa_usuario FOREIGN KEY(id_usuario) REFERENCES usuario (id);