/*CRIAÇÃO DAS TABELAS*/
CREATE TABLE armazem (
  id_armazem bigint(20) NOT NULL AUTO_INCREMENT,
  nome_armazem varchar(255) DEFAULT NULL,
  PRIMARY KEY (id_armazem)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE cfop (
  id_cfop bigint(20) NOT NULL AUTO_INCREMENT,
  cfop_descricao varchar(350) DEFAULT NULL,
  cfop_natureza varchar(5) DEFAULT NULL,
  tipo_cfop varchar(1) DEFAULT NULL,
  PRIMARY KEY (id_cfop)
) ENGINE=InnoDB AUTO_INCREMENT=536 DEFAULT CHARSET=utf8;

CREATE TABLE estados (
  id_estado bigint(20) NOT NULL AUTO_INCREMENT,
  codigo_uf varchar(255) DEFAULT NULL,
  nome varchar(255) DEFAULT NULL,
  regiao varchar(255) DEFAULT NULL,
  uf varchar(255) DEFAULT NULL,
  PRIMARY KEY (id_estado)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

CREATE TABLE cidades (
  id_cidade bigint(20) NOT NULL AUTO_INCREMENT,
  codigo_municipio varchar(255) DEFAULT NULL,
  nome varchar(255) DEFAULT NULL,
  id_estado bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_cidade),
  KEY FK_CIDADE_ESTADO (id_estado),
  CONSTRAINT FK_CIDADE_ESTADO FOREIGN KEY (id_estado) REFERENCES estados (id_estado)
) ENGINE=InnoDB AUTO_INCREMENT=5566 DEFAULT CHARSET=utf8;

CREATE TABLE endereco (
  id_endereco bigint(20) NOT NULL AUTO_INCREMENT,
  bairro varchar(100) NOT NULL,
  cep varchar(9) NOT NULL,
  complemento varchar(100) NOT NULL,
  logradouro varchar(255) NOT NULL,
  numero varchar(10) NOT NULL,
  tipo varchar(255) DEFAULT NULL,
  id_cidade bigint(20) NOT NULL,
  PRIMARY KEY (id_endereco),
  KEY fk_endereco_cidade (id_cidade),
  CONSTRAINT fk_endereco_cidade FOREIGN KEY (id_cidade) REFERENCES cidades (id_cidade)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE empresa (
  id_empresa bigint(20) NOT NULL AUTO_INCREMENT,
  cnpj varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  ie varchar(255) DEFAULT NULL,
  nome_fantasia varchar(255) DEFAULT NULL,
  razao_social varchar(255) DEFAULT NULL,
  tipo_empresa varchar(255) NOT NULL,
  id_endereco bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_empresa),
  KEY fk_empresa_endereco (id_endereco),
  CONSTRAINT fk_empresa_endereco FOREIGN KEY (id_endereco) REFERENCES endereco (id_endereco)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE telefone (
  id_telefone bigint(20) NOT NULL AUTO_INCREMENT,
  numero varchar(255) NOT NULL,
  PRIMARY KEY (id_telefone)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE empresa_telefone (
  id_empresa bigint(20) NOT NULL,
  id_telefone bigint(20) NOT NULL,
  PRIMARY KEY (id_empresa,id_telefone),
  UNIQUE KEY UK_oex8nivmh53n180g7nogmtxb3 (id_telefone),
  CONSTRAINT FKlcgxmg57e14rogpxfb1bcga79 FOREIGN KEY (id_telefone) REFERENCES telefone (id_telefone),
  CONSTRAINT FKo02n76r942it950640nw59d9x FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  ativo bit(1) NOT NULL,
  cpf varchar(255) NOT NULL,
  data_criacao datetime DEFAULT NULL,
  data_nascimento datetime DEFAULT NULL,
  email varchar(50) NOT NULL,
  foto longblob,
  nome_completo varchar(45) NOT NULL,
  password varchar(40) NOT NULL,
  perfil varchar(255) NOT NULL,
  user varchar(16) NOT NULL,
  id_empresa bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_usuario_empresa (id_empresa),
  CONSTRAINT fk_usuario_empresa FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE cliente (
  id_cliente bigint(20) NOT NULL AUTO_INCREMENT,
  cpf_cnpj varchar(18) DEFAULT NULL,
  data_nascimento datetime DEFAULT NULL,
  email varchar(50) NOT NULL,
  informacoes varchar(255) DEFAULT NULL,
  nome varchar(255) NOT NULL,
  tipo_cliente varchar(2) NOT NULL,
  id_empresa bigint(20) DEFAULT NULL,
  id bigint(20) DEFAULT NULL,
  cliente_fornecedor varchar(1) DEFAULT NULL,
  empresa_cliente varchar(255) DEFAULT NULL,
  empresa_setor varchar(255) DEFAULT NULL,
  PRIMARY KEY (id_cliente),
  KEY fk_cliente_empresa (id_empresa),
  KEY fk_cliente_usuario (id),
  CONSTRAINT fk_cliente_empresa FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa),
  CONSTRAINT fk_cliente_usuario FOREIGN KEY (id) REFERENCES usuario (id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE cliente_endereco (
  id_cliente bigint(20) NOT NULL,
  id_endereco bigint(20) NOT NULL,
  UNIQUE KEY UK_6m0tp42c7g7amo8lpom6ua1w3 (id_endereco),
  KEY FKk8j7xgk7nud9pfieoylj91skv (id_cliente),
  CONSTRAINT FKk8j7xgk7nud9pfieoylj91skv FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente),
  CONSTRAINT FKptq1rfvtoheg151utkaxmogo1 FOREIGN KEY (id_endereco) REFERENCES endereco (id_endereco)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE cliente_telefone (
  id_cliente bigint(20) NOT NULL,
  id_telefone bigint(20) NOT NULL,
  PRIMARY KEY (id_cliente,id_telefone),
  UNIQUE KEY UK_f68uvpedslfeohqcndrd7pdcl (id_telefone),
  CONSTRAINT FK3n6ny9ebd1y1wipthlm4g66hf FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente),
  CONSTRAINT FKb2lw8b5pipqcm6wgqe8o0akon FOREIGN KEY (id_telefone) REFERENCES telefone (id_telefone)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE condicao_pagamento (
  id_condicao_pagamento bigint(20) NOT NULL AUTO_INCREMENT,
  formula_condicao varchar(255) DEFAULT NULL,
  nome_condicao varchar(255) DEFAULT NULL,
  PRIMARY KEY (id_condicao_pagamento)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE configuracao_entrada_saida (
  id_configuracao_entrada_saida bigint(20) NOT NULL AUTO_INCREMENT,
  calculca_cofins varchar(1) DEFAULT NULL,
  calculca_pis varchar(1) DEFAULT NULL,
  calculca_icms varchar(1) DEFAULT NULL,
  codigo_conf_entrada_saida varchar(255) DEFAULT NULL,
  tipo_cst varchar(13) NOT NULL,
  descricao varchar(255) DEFAULT NULL,
  gera_financeiro varchar(1) DEFAULT NULL,
  usa_estoque varchar(1) DEFAULT NULL,
  id_cfop bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_configuracao_entrada_saida),
  KEY fk_config_ent_saida_cfop (id_cfop),
  CONSTRAINT fk_config_ent_saida_cfop FOREIGN KEY (id_cfop) REFERENCES cfop (id_cfop)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE conta (
  id_conta bigint(20) NOT NULL AUTO_INCREMENT,
  data_inicio datetime NOT NULL,
  nome_banco varchar(255) DEFAULT NULL,
  numero_banco varchar(255) DEFAULT NULL,
  valor_entrada decimal(7,2) DEFAULT NULL,
  valor_final decimal(7,2) DEFAULT NULL,
  valor_inicial decimal(7,2) DEFAULT NULL,
  valor_total decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (id_conta)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE unidade (
  id_unidade bigint(20) NOT NULL AUTO_INCREMENT,
  descricao varchar(255) NOT NULL,
  sigla varchar(5) NOT NULL,
  PRIMARY KEY (id_unidade)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE grupo_produtos (
  id_grupo_produto bigint(20) NOT NULL AUTO_INCREMENT,
  descricao varchar(255) DEFAULT NULL,
  PRIMARY KEY (id_grupo_produto)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE produto (
  id_produto bigint(20) NOT NULL AUTO_INCREMENT,
  cod_barras varchar(13) DEFAULT NULL,
  cod_produto varchar(255) NOT NULL,
  descricao_produto varchar(50) DEFAULT NULL,
  foto longblob,
  inativo bit(1) NOT NULL,
  indisponivel bit(1) NOT NULL,
  nome_fornecedor varchar(255) DEFAULT NULL,
  nome_produto varchar(80) NOT NULL,
  preco_compra decimal(15,6) NOT NULL,
  preco_venda decimal(15,6) NOT NULL,
  quantidade decimal(19,2) NOT NULL,
  tipo_produto varchar(1) NOT NULL,
  id_grupo_produto bigint(20) DEFAULT NULL,
  id_unidade bigint(20) DEFAULT NULL,
  id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_produto),
  UNIQUE KEY UK_1a45qyse7sbmr1macrtjtqxt4 (cod_produto),
  KEY fk_produto_grupo (id_grupo_produto),
  KEY fk_produto_unidade (id_unidade),
  KEY fk_produto_usuario (id),
  CONSTRAINT fk_produto_grupo FOREIGN KEY (id_grupo_produto) REFERENCES grupo_produtos (id_grupo_produto),
  CONSTRAINT fk_produto_unidade FOREIGN KEY (id_unidade) REFERENCES unidade (id_unidade),
  CONSTRAINT fk_produto_usuario FOREIGN KEY (id) REFERENCES usuario (id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE paramentro (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  numero_venda bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_pnkjqcjgopsl3nokvx3nx3f15 (numero_venda)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE plano_contas (
  id_plano_contas bigint(20) NOT NULL AUTO_INCREMENT,
  codigo varchar(255) DEFAULT NULL,
  descricao varchar(255) DEFAULT NULL,
  id_sub_plano_conta bigint(20) DEFAULT NULL,
  titulo bit(1) NOT NULL,
  descricao_completa varchar(255) DEFAULT NULL,
  receita_despesa varchar(1) NOT NULL,
  PRIMARY KEY (id_plano_contas)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

CREATE TABLE preco (
  id_preco bigint(20) NOT NULL AUTO_INCREMENT,
  acrescimo_porcentagem_preco decimal(15,6) DEFAULT NULL,
  acrescimo_valor_preco decimal(15,6) DEFAULT NULL,
  desconto_porcentagem_preco decimal(15,6) DEFAULT NULL,
  desconto_valor_preco decimal(15,6) DEFAULT NULL,
  preco_total decimal(15,6) DEFAULT NULL,
  tipo_preco varchar(255) DEFAULT NULL,
  valor_preco decimal(15,6) DEFAULT NULL,
  PRIMARY KEY (id_preco)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE estoque (
  id_estoque bigint(20) NOT NULL AUTO_INCREMENT,
  reserva decimal(15,6) DEFAULT NULL,
  saldo_estoque decimal(15,6) DEFAULT NULL,
  id_armazem bigint(20) DEFAULT NULL,
  id_empresa bigint(20) DEFAULT NULL,
  id_preco bigint(20) DEFAULT NULL,
  id_produto bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_estoque),
  KEY fk_estoque_armazem (id_armazem),
  KEY fk_estoque_empresa (id_empresa),
  KEY fk_estoque_preco (id_preco),
  KEY fk_estoque_produto (id_produto),
  CONSTRAINT fk_estoque_armazem FOREIGN KEY (id_armazem) REFERENCES armazem (id_armazem),
  CONSTRAINT fk_estoque_empresa FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa),
  CONSTRAINT fk_estoque_preco FOREIGN KEY (id_preco) REFERENCES preco (id_preco),
  CONSTRAINT fk_estoque_produto FOREIGN KEY (id_produto) REFERENCES produto (id_produto)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE fornecedor (
  id_fornecedor bigint(20) NOT NULL AUTO_INCREMENT,
  cnpj varchar(18) DEFAULT NULL,
  cpf varchar(14) DEFAULT NULL,
  email varchar(50) NOT NULL,
  nome_fantasia varchar(255) NOT NULL,
  observacao varchar(255) DEFAULT NULL,
  razao_social varchar(255) NOT NULL,
  id_endereco bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_fornecedor),
  KEY fk_fornecedor_endereco (id_endereco),
  CONSTRAINT fk_fornecedor_endereco FOREIGN KEY (id_endereco) REFERENCES endereco (id_endereco)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE fornecedor_telefone (
  id_fornecedor bigint(20) NOT NULL,
  id_telefone bigint(20) NOT NULL,
  PRIMARY KEY (id_fornecedor,id_telefone),
  UNIQUE KEY UK_1knya01p5cug99qbqlgqh1grh (id_telefone),
  CONSTRAINT FK6gludpfswug2ie8duu9kol1rs FOREIGN KEY (id_fornecedor) REFERENCES fornecedor (id_fornecedor),
  CONSTRAINT FKlect6l5k45n8f3dr53811wacp FOREIGN KEY (id_telefone) REFERENCES telefone (id_telefone)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE movimento (
  id_movimento bigint(20) NOT NULL AUTO_INCREMENT,
  data_cancelamento_movimento datetime DEFAULT NULL,
  data_movimento datetime DEFAULT NULL,
  desconto_movimento decimal(15,6) DEFAULT NULL,
  frete_movimento decimal(15,6) DEFAULT NULL,
  id_movimento_origem bigint(20) DEFAULT NULL,
  numero_movimento varchar(255) DEFAULT NULL,
  status varchar(1) NOT NULL,
  total_movimento decimal(15,6) DEFAULT NULL,
  id_cliente bigint(20) DEFAULT NULL,
  id_condicao_pagamento bigint(20) DEFAULT NULL,
  id_usuario bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_movimento),
  KEY fk_movimento_cliente (id_cliente),
  KEY fk_movimento_condicao_pagamento (id_condicao_pagamento),
  KEY fk_movimento_usuario (id_usuario),
  CONSTRAINT fk_movimento_cliente FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente),
  CONSTRAINT fk_movimento_condicao_pagamento FOREIGN KEY (id_condicao_pagamento) REFERENCES condicao_pagamento (id_condicao_pagamento),
  CONSTRAINT fk_movimento_usuario FOREIGN KEY (id_usuario) REFERENCES usuario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE item_movimento (
  id_item_movimento bigint(20) NOT NULL AUTO_INCREMENT,
  desconto_item_movimento decimal(15,6) DEFAULT NULL,
  quantidade_item_movimento decimal(15,6) DEFAULT NULL,
  total_item_movimento decimal(15,6) DEFAULT NULL,
  valor_item_movimento decimal(15,6) DEFAULT NULL,
  id_produto bigint(20) NOT NULL,
  id_movimento bigint(20) NOT NULL,
  PRIMARY KEY (id_item_movimento),
  KEY fk_item_movimento_produto (id_produto),
  KEY fk_item_movimento_movimento (id_movimento),
  CONSTRAINT fk_item_movimento_movimento FOREIGN KEY (id_movimento) REFERENCES movimento (id_movimento),
  CONSTRAINT fk_item_movimento_produto FOREIGN KEY (id_produto) REFERENCES produto (id_produto)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE lancamento (
  id_lancamento bigint(20) NOT NULL AUTO_INCREMENT,
  data_baixa datetime DEFAULT NULL,
  data_operacao datetime NOT NULL,
  data_vencimento datetime NOT NULL,
  descricao varchar(255) NOT NULL,
  documento varchar(255) NOT NULL,
  intervalo int(11) DEFAULT NULL,
  observacao varchar(255) DEFAULT NULL,
  parcela int(11) DEFAULT NULL,
  tipo_lancamento varchar(1) DEFAULT NULL,
  valor decimal(19,2) NOT NULL,
  id_cliente bigint(20) NOT NULL,
  id_condicao_pagamento bigint(20) DEFAULT NULL,
  id_empresa bigint(20) DEFAULT NULL,
  id_movimento bigint(20) DEFAULT NULL,
  id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_lancamento),
  KEY fk_lancamento_cliente (id_cliente),
  KEY fk_lancamento_condicao_pagamento (id_condicao_pagamento),
  KEY fk_lancamento_empresa (id_empresa),
  KEY fk_lancamento_movimento (id_movimento),
  KEY fk_lancamento_usuario (id),
  CONSTRAINT fk_lancamento_cliente FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente),
  CONSTRAINT fk_lancamento_condicao_pagamento FOREIGN KEY (id_condicao_pagamento) REFERENCES condicao_pagamento (id_condicao_pagamento),
  CONSTRAINT fk_lancamento_empresa FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa),
  CONSTRAINT fk_lancamento_movimento FOREIGN KEY (id_movimento) REFERENCES movimento (id_movimento),
  CONSTRAINT fk_lancamento_usuario FOREIGN KEY (id) REFERENCES usuario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE menu (
  id_menu bigint(20) NOT NULL AUTO_INCREMENT,
  icon varchar(45) DEFAULT NULL,
  icon_color varchar(255) DEFAULT NULL,
  menu_link varchar(45) DEFAULT NULL,
  menu_name varchar(45) DEFAULT NULL,
  menu_type int(11) DEFAULT NULL,
  queue int(11) DEFAULT NULL,
  top_menu_id int(11) DEFAULT NULL,
  PRIMARY KEY (id_menu)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

CREATE TABLE usuario_menu (
  id_usuario bigint(20) NOT NULL,
  id_menu bigint(20) NOT NULL,
  PRIMARY KEY (id_usuario,id_menu),
  KEY FK34dxm02oqg0us6kfaqmnnxfw9 (id_menu),
  CONSTRAINT FK34dxm02oqg0us6kfaqmnnxfw9 FOREIGN KEY (id_menu) REFERENCES menu (id_menu),
  CONSTRAINT FK4y2hksc3xlr04aj3r5g0ct8tt FOREIGN KEY (id_usuario) REFERENCES usuario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE vendedor (
  id_vendedor bigint(20) NOT NULL AUTO_INCREMENT,
  nome_vendedor varchar(255) DEFAULT NULL,
  id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id_vendedor),
  KEY fk_vendedor_usuario (id),
  CONSTRAINT fk_vendedor_usuario FOREIGN KEY (id) REFERENCES usuario (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**/