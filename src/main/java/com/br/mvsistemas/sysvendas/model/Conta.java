package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "conta")
public class Conta implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 910887211239227843L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_conta")
	private Long id;
	
	@Column(name = "numero_banco")
	private String numeroBanco;
	
	@Column(name = "nome_banco")
	private String nomeBanco;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Column(name = "valor_inicial", precision = 7, scale = 2)
	private BigDecimal valorInicial;
	
	@Column(name = "valor_final", precision = 7, scale = 2)
	private BigDecimal valorFinal;
	
	@Column(name = "valor_entrada", precision = 7, scale = 2)
	private BigDecimal valorEntrada;
	
	@Column(name = "valor_total", precision = 7, scale = 2)
	private BigDecimal valorTotal;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
