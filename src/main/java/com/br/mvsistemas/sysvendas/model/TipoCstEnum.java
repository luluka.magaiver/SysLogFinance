package com.br.mvsistemas.sysvendas.model;

public enum TipoCstEnum {
	
	ZERO			("00" ,"Tributada integralmente"),
	DEZ				("10" ,"Tributada e com cobrança do ICMS por substituição tributária"),
	VINTE			("20" ,"Com redução de base de cálculo"),
	TRINTA			("30" ,"Isenta ou não tributada e com cobrança do ICMS por substituição tributária"),
	QUARENTA		("40" ,"Isenta"),
	QUARENTA_UM		("41" ,"Não tributada"),
	CINQUENTA		("50" ,"Suspensão"),
	CINQUENTA_UM	("51" ,"Diferimento"),
	SESSENTA		("60" ,"ICMS cobrado anteriormente por substituição tributária"),
	SETENTA			("70" ,"Com redução de base de cálculo e cobrança do ICMS por substituição tributária"),
	NOVENTA			("90" ,"Outras");
	
	private String numero;
	private String descricao;

	TipoCstEnum(String numero,String descricao) {
		this.descricao = descricao;
		this.numero = numero;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getNumero() {
		return numero;
	}
	
}
