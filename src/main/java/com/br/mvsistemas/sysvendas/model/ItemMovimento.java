package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "item_movimento")
public class ItemMovimento implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4113004362101948710L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_item_movimento")
	private Long id;
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_item_movimento_movimento")
	@JoinColumn(name = "id_movimento", nullable = false)
	private Movimento movimento;
	
	@Getter
	@Setter
	@ManyToOne  @ForeignKey(name="fk_item_movimento_produto")
	@JoinColumn(name = "id_produto", nullable = false)
	private Produto itemMovimento;
	
	@Getter
	@Setter
	@Column(name = "quantidade_item_movimento", precision = 15, scale = 6)
	private BigDecimal quantidadeItemMovimento;
	
	@Getter
	@Setter
	@Column(name = "valor_item_movimento", precision = 15, scale = 6)
	private BigDecimal valorItemMovimento;
	
	@Getter
	@Setter
	@Column(name = "desconto_item_movimento", precision = 15, scale = 6)
	private BigDecimal descontoItemMovimento;
	
	@Getter
	@Setter
	@Column(name = "total_item_movimento", precision = 15, scale = 6)
	private BigDecimal totalItemMovimento;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
