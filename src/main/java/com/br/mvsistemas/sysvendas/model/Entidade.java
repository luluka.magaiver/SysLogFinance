package com.br.mvsistemas.sysvendas.model;

import java.io.Serializable;

public interface Entidade extends Serializable{
	public Long getId();
	public void setId(Long id);
	
}
