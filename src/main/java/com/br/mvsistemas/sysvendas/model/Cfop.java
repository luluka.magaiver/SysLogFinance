package com.br.mvsistemas.sysvendas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "cfop")
@XmlRootElement
public class Cfop implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5686633906463102135L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cfop")
	private Long id;
	
	@Column(name = "cfop_natureza",length = 5)
	private String cfopNatureza;
	
	@Column(name = "cfop_descricao",length = 350)
	private String cfopDescricao;
	
	@Column(length = 1, name = "tipo_cfop", nullable = true)
	private String tipoCfop;
	

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCfopNatureza() {
		return cfopNatureza;
	}

	public void setCfopNatureza(String cfopNatureza) {
		this.cfopNatureza = cfopNatureza;
	}

	public String getCfopDescricao() {
		return cfopDescricao;
	}

	public void setCfopDescricao(String cfopDescricao) {
		this.cfopDescricao = cfopDescricao;
	}

	public String getTipoCfop() {
		return tipoCfop;
	}

	public void setTipoCfop(String tipoCfop) {
		this.tipoCfop = tipoCfop;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cfop other = (Cfop) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
