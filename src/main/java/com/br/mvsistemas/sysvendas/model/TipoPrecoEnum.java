package com.br.mvsistemas.sysvendas.model;

public enum TipoPrecoEnum {

	ATACADO("Atacado"),
	VAREJO("Varejo");
	
	private String descricao;
	
	TipoPrecoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
