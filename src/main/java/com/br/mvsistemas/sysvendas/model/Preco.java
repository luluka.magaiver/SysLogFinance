package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "preco")
public class Preco implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 192134293980262879L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_preco")
	private Long id;
	
	@Getter
	@Setter
	@Column(name = "valor_preco", precision = 15, scale = 6)
	private BigDecimal valorPreco;
	
	@Getter
	@Setter
	@Column(name = "desconto_valor_preco", precision = 15, scale = 6)
	private BigDecimal descontoValorPreco;
	
	@Getter
	@Setter
	@Column(name = "acrescimo_valor_preco", precision = 15, scale = 6)
	private BigDecimal acrescimoValorPreco;
	
	@Getter
	@Setter
	@Column(name = "desconto_porcentagem_preco", precision = 15, scale = 6)
	private BigDecimal descontoPorcentagemPreco;
	
	@Getter
	@Setter
	@Column(name = "acrescimo_porcentagem_preco", precision = 15, scale = 6)
	private BigDecimal acrescimoPorcentagemPreco;
	
	@Getter
	@Setter
	@Column(name = "preco_total", precision = 15, scale = 6)
	private BigDecimal precoTotal;
	
	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_preco")
	private TipoPrecoEnum tipoPreco;
	
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Preco other = (Preco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
