package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "movimento")
public class Movimento implements Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1886117495698480212L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_movimento")
	private Long id;
	
	@Getter
	@Setter
	@Column(name = "id_movimento_origem", nullable = true)
	private Long idMovimentoOrgem;

	@Getter
	@Setter
	@Column(name = "numero_movimento")
	private String numeroMovimento;

	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name = "fk_movimento_cliente")
	@JoinColumn(name = "id_cliente", nullable = true)
	private Cliente clienteMovimento = new Cliente();

	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name = "fk_movimento_usuario")
	@JoinColumn(name = "id_usuario", nullable = true)
	private Usuario usuarioMovimento = new Usuario();

	@Getter
	@Setter
	@OneToMany(mappedBy = "movimento", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	private List<ItemMovimento> itensMovimento = new ArrayList<ItemMovimento>();
	
	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name = "fk_movimento_condicao_pagamento")
	@JoinColumn(name = "id_condicao_pagamento", nullable = true)
	private CondicaoPagamento condicaoPagamento = new CondicaoPagamento();

	@Getter
	@Setter
	@Column(name = "total_movimento", precision = 15, scale = 6)
	private BigDecimal totalMovimento = BigDecimal.ZERO;

	@Getter
	@Setter
	@Column(name = "desconto_movimento", precision = 15, scale = 6)
	private BigDecimal descontoMovimento = BigDecimal.ZERO;

	@Getter
	@Setter
	@Column(name = "frete_movimento", precision = 15, scale = 6)
	private BigDecimal freteMovimento = BigDecimal.ZERO;

	@Getter
	@Setter
	@Column(name = "data_movimento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataMovimento;

	@Getter
	@Setter
	@Column(name = "data_cancelamento_movimento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCancelamentoMovimento;

	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false, length = 1)
	private TipoStatusMovimento status;

//	@Getter
//	@Setter
//	@ManyToOne
//	@ForeignKey(name = "FK_MOVIEMNTO_VENDEDOR")
//	@JoinColumn(name = "id_vendedor", nullable = true)
//	private Vendedor vendedor = new Vendedor();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimento other = (Movimento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
