package com.br.mvsistemas.sysvendas.model;


/**
 * @author Marcus Vinicius
 */
public enum TipoEmpresaEnum {
	MATRIZ("Matriz"),
	FILIAL("Filial");

	private String descricao;

	TipoEmpresaEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
