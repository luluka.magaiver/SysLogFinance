package com.br.mvsistemas.sysvendas.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "plano_contas")
public class PlanoContas implements Entidade{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2122198393620525125L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_plano_contas")
	private Long id;
	
	@Getter
	@Setter
	private String codigo;
	
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	@Column(name = "descricao_completa")
	private String descricaoCompleta;
	
	@Getter
	@Setter
	private boolean titulo;
	
	@Getter
	@Setter
	@Column(name = "id_sub_plano_conta")
	private Long idSuPlanoContas;
	
	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	@Column(name = "receita_despesa", nullable = false, length=1)
	private TipoPlanoContas receitaDespesa;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoContas other = (PlanoContas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
