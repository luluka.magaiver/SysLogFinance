package com.br.mvsistemas.sysvendas.model;

public enum TipoMeiosPagamentoEnum {
	
	DINHEIRO("Dinheiro"),
	CARTAO("Cartão"),
	CHEQUE("Cheque"),
	BOLETO("Boleto"),
	DEPOSITO_BANCARIO("Depósito bancário"),
	PROMISSORIA("Promissoria");
	
	private String descricao;
	
	TipoMeiosPagamentoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
