package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "caixa")
public class Caixa implements Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1036798918038249831L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_caixa")
	private Long id;

	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_abertura")
	private Date dataAbertuda;

	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_fechamento")
	private Date dataFechamento;

	@Getter
	@Setter
	@Column(name = "valor_abertura")
	private BigDecimal valorAbertura;
	
	@Getter
	@Setter
	@Column(name = "valor_fechamento")
	private BigDecimal valorFechamento;
	
	@Getter
	@Setter
	private String aberto;
	
	@Getter
    @Setter
	@ManyToOne
	@ForeignKey(name="fk_caixa_usuario")
	@JoinColumn(name = "id_usuario", nullable = true)
	private Usuario usuario = new Usuario();
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caixa other = (Caixa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
