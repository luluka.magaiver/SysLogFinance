package com.br.mvsistemas.sysvendas.model;

public enum TipoPlanoContas {
	R("Receita"),
	D("Despesa");
	
	private String descricao;
	
	TipoPlanoContas(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
