package com.br.mvsistemas.sysvendas.model;

public enum TipoGrupoEnum {
	
	GRAOS("Grãos"),
	BEBIDAS("Bebidas"),
	CARNES("CARNES");
	
	private String descricao;
	
	TipoGrupoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
