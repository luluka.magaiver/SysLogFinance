package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "estoque")
public class Estoque implements Entidade{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1111408730869114564L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estoque")
	private Long id;
	
	
	@Column(name = "saldo_estoque", precision = 15, scale = 6)
	@Getter @Setter private BigDecimal saldoEstoque;
	
	@Getter
	@Setter
	@Column(name = "reserva", precision = 15, scale = 6)
	private BigDecimal reserva;
	
	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name="fk_estoque_produto")
	@JoinColumn(name = "id_produto", nullable = true)
	private Produto produto = new Produto();
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_estoque_empresa")
	@JoinColumn(name = "id_empresa", nullable = true)
	private Empresa empresa = new Empresa();
	
	@Getter
	@Setter
	@ManyToOne  @ForeignKey(name="fk_estoque_armazem")
	@JoinColumn(name = "id_armazem", nullable = true)
	private Armazem Armazem = new Armazem();
	
	@Getter
	@Setter
	@ManyToOne  @ForeignKey(name="fk_estoque_preco")
	@JoinColumn(name = "id_preco", nullable = true)
	private Preco preco = new Preco();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estoque other = (Estoque) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
