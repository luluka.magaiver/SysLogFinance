package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "lancamento")
public class Lancamento implements Entidade, Comparable<Lancamento>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5189629963467430692L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_lancamento")
	private Long id;
	
	@NotNull
	@Getter
	@Setter
	@ManyToOne  @ForeignKey(name="fk_lancamento_cliente")
	@JoinColumn(name = "id_cliente", nullable = true)
	private Cliente cliente = new Cliente();
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_lancamento_empresa")
	@JoinColumn(name = "id_empresa", nullable = true)
	private Empresa empresa = new Empresa();
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_lancamento_usuario")
	@JoinColumn(name = "id", nullable = true)
	private Usuario usuario = new Usuario();
//	
//	@Getter
//	@Setter
//	@Column(nullable = true)
//	private Long movimento;
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_lancamento_movimento")
	@JoinColumn(name = "id_movimento", nullable = true)
	private Movimento movimento = new Movimento();
	
	@NotBlank
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	private String observacao;
	
	@NotBlank
	@Getter
	@Setter
	private String documento;
	
	@NotNull
	@Getter
	@Setter
	private BigDecimal valor;
	
	@Getter
	@Setter
	@Column(name = "valor_pago")
	private BigDecimal valorPago;
	
	@Getter
	@Setter
	private Integer parcela;
	
	@Getter
	@Setter
	private Integer intervalo;
	
	@Getter
	@Setter
	@Column(name="tipo_lancamento", length = 1)
	private String tipoLancamento;
	
	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name="fk_lancamento_condicao_pagamento")
	@JoinColumn(name = "id_condicao_pagamento", nullable = true)
	private CondicaoPagamento condicaoPagamento = new CondicaoPagamento();
	
	@NotNull
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_vencimento")
	private Date dataVencimento;
	
	@NotNull
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_operacao")
	private Date dataOperacao;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_baixa")
	private Date dataBaixa;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamento other = (Lancamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(Lancamento o) {
		return this.getId().compareTo(o.getId());
	}
}
