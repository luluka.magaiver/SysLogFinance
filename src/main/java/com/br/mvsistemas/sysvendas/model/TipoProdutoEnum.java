package com.br.mvsistemas.sysvendas.model;

public enum TipoProdutoEnum {
	
	V("Venda"),
	S("Serviço"),
	M("Materia Prima"),
	C("Consumo");
	
	private String descricao;
	
	TipoProdutoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
