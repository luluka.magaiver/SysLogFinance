package com.br.mvsistemas.sysvendas.model;

public enum TipoStatusMovimento {

	V("Venda"),
	D("Devolução"),
	C("Cancelado");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoStatusMovimento(String descricao) {
		this.descricao = descricao;
	}
}
