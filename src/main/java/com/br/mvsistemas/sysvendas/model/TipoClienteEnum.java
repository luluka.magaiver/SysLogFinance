package com.br.mvsistemas.sysvendas.model;


/**
 * @author Marcus Vinicius
 */
public enum TipoClienteEnum {
	PF("Pessoa Fisica"),
	PJ("Pessoa Juridica");

	private String descricao;

	TipoClienteEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
