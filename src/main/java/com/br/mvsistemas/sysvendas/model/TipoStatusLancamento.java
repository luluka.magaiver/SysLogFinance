package com.br.mvsistemas.sysvendas.model;

public enum TipoStatusLancamento {

	R("Receber"),
	Q("Quitado"),
	P("Parcialmente Qauitado");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoStatusLancamento(String descricao){
		this.descricao = descricao;
	}
}
