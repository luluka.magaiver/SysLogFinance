package com.br.mvsistemas.sysvendas.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Marcus Vinicius
 */
@Entity
@Table(name = "cliente")
public class Cliente implements Entidade{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private Long id;

    @NotEmpty
    @Getter
    @Setter
	@Column(nullable = false)
	private String nome;

    @Getter
    @Setter
	@Column(name="cpf_cnpj", nullable = true, length = 18)
	private String cpfCnpj;
    
	@Getter
    @Setter
    @Size(max = 50)
	private String email;
    
    @Getter
    @Setter
	@Column(length = 255, nullable = true)
	private String informacoes;
    
    @Getter
    @Setter
	@Column(length = 255, nullable = true, name = "empresa_cliente")
	private String empresaCliente;
    
    @Getter
    @Setter
	@Column(length = 1, nullable = true, name="cliente_fornecedor")
	private String clienteFornecedor;
    
    @Getter
    @Setter
	@Column(length = 255, nullable = true, name = "empresa_setor")
	private String empresaSetor;
	
    @Getter
    @Setter
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_nascimento")
	private Date dataNascimento;

    @Getter
    @Setter
	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL) 
	@JoinTable(name = "cliente_endereco", joinColumns = @JoinColumn(name = "id_cliente"), inverseJoinColumns = @JoinColumn(name = "id_endereco"))
	private List<Endereco> enderecos;
	
    @Getter
    @Setter
	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL) 
	@JoinTable(name = "cliente_telefone", joinColumns = @JoinColumn(name = "id_cliente"), inverseJoinColumns = @JoinColumn(name = "id_telefone"))
	private Set<Telefone> telefones;
	
    @Getter
    @Setter
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_cliente", nullable = false, length = 2)
	private TipoClienteEnum tipo;
	
    @Getter
    @Setter
	@ManyToOne  @ForeignKey(name="fk_cliente_empresa")
	@JoinColumn(name = "id_empresa", nullable = true)
	private Empresa empresa = new Empresa();
	
    @Getter
    @Setter
	@ManyToOne
	@ForeignKey(name="fk_cliente_usuario")
	@JoinColumn(name = "id", nullable = true)
	private Usuario usuario = new Usuario();
    
    @Getter
    @Setter
    @Transient
    private Integer mesAniversario;
	
	public Cliente() {}

    public Cliente(TipoClienteEnum tipo){
        this.tipo = tipo;
    }
    
    @Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	

	
}
