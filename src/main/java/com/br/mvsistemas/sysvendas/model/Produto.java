package com.br.mvsistemas.sysvendas.model;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "produto")
public class Produto implements Entidade{

	/**
	 */
	private static final long serialVersionUID = -4616179445187140305L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_produto")
	private Long id;
	
	@NotNull
	@Getter
	@Setter
	@Column(name = "cod_produto", unique = true)
	private String codProduto;
	
	@Getter
	@Setter
	@Column(name = "cod_barras", length = 13)
	private String codBarras;
	
	@Getter
	@Setter
	@Column(name="nome_fornecedor")
	private String nomeFornecedor;
	
	@NotNull
	@Getter
	@Setter
	@Column(name = "nome_produto", length = 80)
	private String nomeProduto;
	
	@Getter
	@Setter
	@Column(name = "descricao_produto", length = 50)
	private String descricaoProduto;
	
	@NotNull
	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_produto", nullable = false, length=1)
	private TipoProdutoEnum tipoProduto;
	
	@NotNull
	@Getter
	@Setter
	@Column(name = "preco_compra", precision = 15, scale = 6)
	private BigDecimal precoCompra;
	
	
	@NotNull
	@Getter
	@Setter
	@Column(name = "preco_venda", precision = 15, scale = 6)
	private BigDecimal precoVenda;
	
	@Getter
	@Setter
	@Transient
	private BigDecimal precoVendido;
	
	@NotNull
	@Getter
	@Setter
	@Column(name = "quantidade")
	private BigDecimal quantidade;
	
	
	@Getter
	@Setter
	private boolean inativo;
	
	@Getter
	@Setter
	private boolean indisponivel;
	
	@Getter
	@Setter
	@ManyToOne  @ForeignKey(name="fk_produto_grupo")
	@JoinColumn(name = "id_grupo_produto", nullable = true)
	private GrupoProduto grupoProduto = new GrupoProduto();
	
	@Lob
	@Getter
	@Setter
	private byte[] foto;
	
	@Getter
	@Setter
	@Column(name = "foto_produto")
	private String fotoProduto;
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_produto_unidade")
	@JoinColumn(name = "id_unidade", nullable = true)
	private Unidade unidade = new Unidade();
	
	@Getter
	@Setter
	@ManyToOne @ForeignKey(name="fk_produto_usuario")
	@JoinColumn(name = "id", nullable = true)
	private Usuario usuario = new Usuario();
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public StreamedContent getInputImageProduto() {

		if (foto != null) {
			if (foto.length != 0) {
				return new DefaultStreamedContent(new ByteArrayInputStream(this.getFoto()), "");
			} else {
				return new DefaultStreamedContent();
			}
		} else {
			return new DefaultStreamedContent();
		}

	}
	
	public boolean isSaldoEstoqueSuficiente(){
		return !isSaldoEstoqueInsuficiente();
	}
	
	public boolean isSaldoEstoqueInsuficiente(){
		return (this.quantidade.equals(BigDecimal.ZERO)) || (this.quantidade.intValue() < 0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
