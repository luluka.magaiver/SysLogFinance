package com.br.mvsistemas.sysvendas.model;

public enum TipoSimNaoEnum {

	S("Sim"),
	N("Não");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoSimNaoEnum(String descricao) {
		this.descricao = descricao;
	}
}
