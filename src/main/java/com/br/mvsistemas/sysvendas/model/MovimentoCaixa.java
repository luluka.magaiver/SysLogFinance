package com.br.mvsistemas.sysvendas.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "movimento_caixa")
public class MovimentoCaixa implements Entidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2262496797414537186L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_movimento_caixa")
	private Long id;

	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date hora;

	@Getter
	@Setter
	@Column(name = "valor_movimento_caixa")
	private BigDecimal valorMovimentoCaixa;

	@Getter
	@Setter
	@ManyToOne
	@ForeignKey(name = "fk_movimento_caixa_caixa")
	@JoinColumn(name = "id_caixa", nullable = true)
	private Caixa caixa = new Caixa();
	
	@Getter
    @Setter
	@ManyToOne
	@ForeignKey(name="fk_movimento_caixa_usuario")
	@JoinColumn(name = "id_usuario", nullable = true)
	private Usuario usuario = new Usuario();
		
	@Getter
	@Setter
	@Column(name = "entrada_saida", length = 1)
	private String entradaSaida;
	
	@Getter
	@Setter
	private String observacao;
	

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovimentoCaixa other = (MovimentoCaixa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
