package com.br.mvsistemas.sysvendas.model;

public enum TipoMovimento {

	VENDA("Venda"),
	ORCAMENTO("Orçamento"),
	PEDIDO("Pedido"),
	ENTRADA("Entrada"),
	SAIDA("Saida");
	
	private String descricao;
	
	TipoMovimento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
