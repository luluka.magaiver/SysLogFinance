package com.br.mvsistemas.sysvendas.model;

/**
 * @author Marcus Vinicius
 */
public enum TipoSexoEnum {
	MASCULINO("Masculino"), 
	FEMININO("Feminino");
	
	private String descricao;

	TipoSexoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	

}
