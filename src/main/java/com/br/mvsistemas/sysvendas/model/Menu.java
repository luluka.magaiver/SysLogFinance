package com.br.mvsistemas.sysvendas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "menu")
public class Menu implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7048570359772791315L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_menu")
    private Long id;
	
    @Size(max = 45)
    @Column(name = "menu_name")
    private String menuName;
    
    @Column(name = "top_menu_id")
    private Integer topMenuId;
    
    @Column(name = "menu_type")
    private Integer menuType;
    
    @Size(max = 45)
    @Column(name = "menu_link")
    private String menuLink;
    
    @Size(max = 45)
    @Column(name = "icon")
    private String icon;
    
    @Column(name = "icon_color")
    private String iconColor;
    
    @Column(name = "queue")
    private Integer queue;
    
    @Override
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public Integer getTopMenuId() {
		return topMenuId;
	}
	public void setTopMenuId(Integer topMenuId) {
		this.topMenuId = topMenuId;
	}
	public Integer getMenuType() {
		return menuType;
	}
	public void setMenuType(Integer menuType) {
		this.menuType = menuType;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getIconColor() {
		return iconColor;
	}
	public void setIconColor(String iconColor) {
		this.iconColor = iconColor;
	}
	public Integer getQueue() {
		return queue;
	}
	public void setQueue(Integer queue) {
		this.queue = queue;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menu other = (Menu) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
}
