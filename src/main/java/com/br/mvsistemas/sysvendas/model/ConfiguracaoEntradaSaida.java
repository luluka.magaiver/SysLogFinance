package com.br.mvsistemas.sysvendas.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "configuracao_entrada_saida")
public class ConfiguracaoEntradaSaida implements Entidade{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1765586111633247958L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_configuracao_entrada_saida")
	private Long id;
	
	@Column(name = "codigo_conf_entrada_saida")
	private String codigoConfEntradaSaida;
	
	@ManyToOne @ForeignKey(name="fk_config_ent_saida_cfop")
	@JoinColumn(name = "id_cfop", nullable = true)
	private Cfop cfop = new Cfop();
	
	private String descricao;
	
	@Column(name = "gera_financeiro",length = 1)
	private String geraFinanceiro;//uma posição S ou N
	
	@Column(name = "usa_estoque",length = 1)
	private String usaEstoque;//uma posicao S ou N
	
	@Column(name = "calculca_icms",length = 1)
	private String calculcaIcms;//uma posicao S ou N
	
	@Column(name = "calculca_pis",length = 1)
	private String calculaPis;//uma posicao S ou N
	
	@Column(name = "calculca_cofins",length = 1)
	private String calculaCofins;//uma posicao S ou N
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_cst", nullable = false, length = 13)
	private TipoCstEnum cst;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoConfEntradaSaida() {
		return codigoConfEntradaSaida;
	}

	public void setCodigoConfEntradaSaida(String codigoConfEntradaSaida) {
		this.codigoConfEntradaSaida = codigoConfEntradaSaida;
	}

	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getGeraFinanceiro() {
		return geraFinanceiro;
	}

	public void setGeraFinanceiro(String geraFinanceiro) {
		this.geraFinanceiro = geraFinanceiro;
	}

	public String getUsaEstoque() {
		return usaEstoque;
	}

	public void setUsaEstoque(String usaEstoque) {
		this.usaEstoque = usaEstoque;
	}

	public String getCalculcaIcms() {
		return calculcaIcms;
	}

	public void setCalculcaIcms(String calculcaIcms) {
		this.calculcaIcms = calculcaIcms;
	}

	public String getCalculaPis() {
		return calculaPis;
	}

	public void setCalculaPis(String calculaPis) {
		this.calculaPis = calculaPis;
	}

	public String getCalculaCofins() {
		return calculaCofins;
	}

	public void setCalculaCofins(String calculaCofins) {
		this.calculaCofins = calculaCofins;
	}

	public TipoCstEnum getCst() {
		return cst;
	}

	public void setCst(TipoCstEnum cst) {
		this.cst = cst;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracaoEntradaSaida other = (ConfiguracaoEntradaSaida) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
