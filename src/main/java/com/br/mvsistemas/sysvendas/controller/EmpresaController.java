package com.br.mvsistemas.sysvendas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.json.simple.parser.ParseException;

import com.br.mvsistemas.sysvendas.dto.EmpresaDTO;
import com.br.mvsistemas.sysvendas.model.Cidades;
import com.br.mvsistemas.sysvendas.model.Empresa;
import com.br.mvsistemas.sysvendas.model.Telefone;
import com.br.mvsistemas.sysvendas.model.TipoEmpresaEnum;
import com.br.mvsistemas.sysvendas.service.CidadeService;
import com.br.mvsistemas.sysvendas.service.EmpresaService;
import com.br.mvsistemas.sysvendas.service.EnderecoService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;


@Named
@ViewScoped
public class EmpresaController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1188093964678532128L;
	
	
	private Empresa empresa;
	private Empresa empresaSelecionada;
	private Telefone telefone;
	private Telefone telefoneSelecionado;
	private List<Empresa> listaEmpresas;
	private boolean ativo = false;
	private List<Cidades> listaDeCidades;
	private EmpresaDTO pesquisaEmpresa;
	
	@Inject
	private CidadeService cidadeService;
	
	@Inject
	private EmpresaService empresaService;
	
	@Inject
	private EnderecoService enderecoService;
	
	@Inject
	private FacesMessages mensage;
		
	@PostConstruct
	public void consultar(){
		pesquisaEmpresa = new EmpresaDTO();
		empresa = new Empresa();
		telefone = new Telefone();
		//listaEmpresas = empresaService.listaEmpresas();
	}
	
	public void salvar(){
		
//		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
//        Set<ConstraintViolation<Empresa>> errors = validator.validate(this.empresa);
//
//        for (ConstraintViolation<Empresa> error : errors) {
//           mensage.error(error.getMessage());
//        }
//
//        if(!errors.isEmpty()){
//            return; 
//        }
		
		if (empresaService.salvar(empresa)) {
			consultar();
			desativar();
		}
	}
	
	public List<Cidades> listarCidades(String query) {
		return cidadeService.todasCidades(query, listaDeCidades, mensage);
	}
	
	public void buscarEndereco(){
		enderecoService.preencherEndereco(null, empresa, empresa.getEndereco());
	}
	
	public void buscarPorCnpj() {
		try {
			empresa = empresaService.buscaCnpj(empresa.getCnpj().replaceAll("\\D", ""));
		} catch (IOException | ParseException e) {
			if (e.getMessage().startsWith("504")) {
				mensage.error("Tempo Esgotado, CNPJ não encontrado na Base de Dados");
			}
			//e.printStackTrace();
		}
	}
	
	public void consultarEmpresas() {
		List<Empresa> emp = empresaService.consultaDeEmpresa(pesquisaEmpresa.getFantasia(), pesquisaEmpresa.getRazao(), 
				pesquisaEmpresa.getCnpj(), pesquisaEmpresa.getIe());
		if (emp.size() <= 0) {
			mensage.alert("Nenhuma Empresa encontrada...");
			listaEmpresas = null;
			return;
		} else {
			listaEmpresas = emp;
		}
	}
	
	public void excluir(ActionEvent actionEvent){
		empresaSelecionada = (Empresa) actionEvent.getComponent().getAttributes().get("empresaExcluida");
		try {
			empresaService.excluir(empresaSelecionada);
			consultarEmpresas();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editar(ActionEvent actionEvent){
		empresa = (Empresa) actionEvent.getComponent().getAttributes().get("empresaEditada");
		ativar();
		//FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuarioEditado", usuarioEdicao);
	}
	
	public void addTel() {
		enderecoService.adicionarTelefone(null, empresa, telefone);
	}
	
	public void delTel() {
		enderecoService.removerTelefone(null, empresa, telefoneSelecionado);
	}
	
	public void ativar(){
		ativo = true;
	}
	
	public void desativar(){
		ativo = false;
		empresa = new Empresa();
	}
	
	public List<Empresa> getListaEmpresas() {
		return listaEmpresas;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa getEmpresaSelecionada() {
		return empresaSelecionada;
	}
	
	public void setEmpresaSelecionada(Empresa empresaSelecionada) {
		this.empresaSelecionada = empresaSelecionada;
	}
	
	public TipoEmpresaEnum[] getTiposEmpresas(){
		return TipoEmpresaEnum.values();
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	public List<Cidades> getListaDeCidades() {
		return listaDeCidades;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public Telefone getTelefoneSelecionado() {
		return telefoneSelecionado;
	}

	public void setTelefoneSelecionado(Telefone telefoneSelecionado) {
		this.telefoneSelecionado = telefoneSelecionado;
	}
	
	public EmpresaDTO getPesquisaEmpresa() {
		return pesquisaEmpresa;
	}
	
	public void setPesquisaEmpresa(EmpresaDTO pesquisaEmpresa) {
		this.pesquisaEmpresa = pesquisaEmpresa;
	}
}
