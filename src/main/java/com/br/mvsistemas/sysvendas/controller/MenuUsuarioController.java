package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.dao.MenuDao;
import com.br.mvsistemas.sysvendas.daoimpl.MenuDaoImpl;
import com.br.mvsistemas.sysvendas.model.Menu;


@Named
@ViewScoped
public class MenuUsuarioController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 942747256135164375L;
	
	private List<Menu> menuList;
	private String pageLink;
    private String pageName;
    private String searhText;
    
    private MenuDao menuDao;
   
	public MenuUsuarioController() {
        pageLink = "blankPage";
        menuDao = new MenuDaoImpl();
    }
	
	public void setPage(String link, String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> map = context.getViewRoot().getViewMap();
        List<String> list = new ArrayList<>();

        for (String key : map.keySet()) {
            if (!key.equals("menuController")) {
                list.add(key);
            }
        }

        if (list != null && !list.isEmpty()) {
            for (String get : list) {
                map.remove(get);
            }
        }

        setPageLink(link);
        setPageName(name);
    }
	
	public List<Menu> getMenuList() {
		if (menuList == null) {
			menuList = menuDao.list();
		}
		return menuList;
	}
	
	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getSearhText() {
		return searhText;
	}

	public void setSearhText(String searhText) {
		this.searhText = searhText;
	}

	
}
