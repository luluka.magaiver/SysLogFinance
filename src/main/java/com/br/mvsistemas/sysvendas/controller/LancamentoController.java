package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.TabChangeEvent;

import com.br.mvsistemas.sysvendas.dao.LancamentoDao;
import com.br.mvsistemas.sysvendas.daoimpl.LancamentoDaoImpl;
import com.br.mvsistemas.sysvendas.dto.LancamentoDTO;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;
import com.br.mvsistemas.sysvendas.model.TipoStatusLancamento;
import com.br.mvsistemas.sysvendas.model.TipoStatusMovimento;
import com.br.mvsistemas.sysvendas.service.CaixaService;
import com.br.mvsistemas.sysvendas.service.ClienteService;
import com.br.mvsistemas.sysvendas.service.LancamentoService;
import com.br.mvsistemas.sysvendas.service.MovimentoCaixaService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class LancamentoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8528240437121933551L;

	@Inject
	private FacesMessages mensage;
	@Inject
	private LancamentoService lancamentoService;
	@Inject
	private ClienteService clienteService;
	@Inject
	private CaixaService caixaService;
	@Inject
	private MovimentoCaixaService movCaixaService;

	private LancamentoDao<Lancamento> lancamentoDao;
	private List<Lancamento> listaLancamento;
	private List<Cliente> listaClientes;
	@Getter
	private List<Cliente> listaFornecedores;
	private Lancamento lancamento;
	private boolean ativo = false;
	private static Integer PARCELA = 0;
	private boolean baixa = false;
	private static String IS_NULL = "is null";
	private static String IS_NOT_NULL = "is not null";
	private int opcaoLancamento;
	private BigDecimal valorBaixaParcial;

	private Date dataAtual;

	private LancamentoDTO pesquisaLancamento;

	private String opcao;

	@Getter
	@Setter
	private String tipoLancamento;

	@PostConstruct
	public void consultar() {
		dataAtual = new Date();
		lancamento = new Lancamento();
		lancamentoDao = new LancamentoDaoImpl();
		pesquisaLancamento = new LancamentoDTO();
		pesquisaLancamento.setOpcao("1");
		pesquisaLancamento.setTipoLancamento("T");
		setOpcaoLancamento(1);
		setOpcao("2");
		pesquisaLancamento.setDateIncial(primeiaDataDoMes());
		pesquisaLancamento.setDateFinal(new Date());
	}

	public void consutaDeLancamentos() {
		List<Lancamento> lancamentos = lancamentoService.consultaDeLancamentos(pesquisaLancamento.getNomeCliente(),
				pesquisaLancamento.getDocumento(), pesquisaLancamento.getDateIncial(),
				pesquisaLancamento.getDateFinal(), pesquisaLancamento.getOpcao(),
				pesquisaLancamento.getTipoLancamento());

		if (lancamentos.size() <= 0) {
			mensage.alert("Nenhum Lançamento Encontrado");
			listaLancamento = null;
			return;
		} else {
			listaLancamento = lancamentos;
		}
	}

	public List<Cliente> listarClientes(String query) {
		return clienteService.todosClientes(query, listaClientes, mensage);
	}

	public List<Cliente> listarFornecedores(String query) {
		return clienteService.todosClientes(query, listaFornecedores, mensage);
	}

	public List<CondicaoPagamento> getCondicaoDePagamentos() {
		return lancamentoService.listaCondicaoPagamento();
	}

	public void ativar() {
		if (caixaService.isCaixaAberto()) {
			ativo = true;
			consultar();
		} else {
			mensage.alert("Não existe caixa aberto!");
		}

	}

	public void desativar() {
		ativo = false;
		lancamento = new Lancamento();
	}

	public boolean isAtivo() {
		return ativo;
	}

	public boolean isBaixa() {
		return baixa;
	}

	public void salvar() {
		if (lancamento.getCondicaoPagamento().getNomeCondicao().equalsIgnoreCase("A VISTA")) {
			lancamento.setDataBaixa(new Date());
		}
		lancamento.setMovimento(null);

		lancamentoService.lancar(lancamento, opcao);

		if (lancamento.getCondicaoPagamento().getNomeCondicao().equalsIgnoreCase("A Vista")) {

			MovimentoCaixa movCaixa = new MovimentoCaixa();

			movCaixa.setCaixa(caixaService.caixaAberto());
			movCaixa.setHora(lancamento.getDataOperacao());
			movCaixa.setUsuario(lancamento.getUsuario());
			movCaixa.setValorMovimentoCaixa(lancamento.getValor());
			

			if (lancamento.getTipoLancamento().equals("R")) {
				movCaixa.setEntradaSaida("E");
			} else if (lancamento.getTipoLancamento().equals("P")) {
				movCaixa.setEntradaSaida("S");
			}
			
			movCaixa.setObservacao("Lançamento de "+movCaixa.getEntradaSaida() == "E" ? "Entrada" : "Saida"+" de Nº: "+lancamento.getDocumento()+" no valor de: "+lancamento.getValor());

			movCaixaService.registrarMovimentoCaixa(movCaixa);
		}

		desativar();
	}

	public void baixarTitulo(ActionEvent actionEvent) {

		if (opcaoLancamento == 1) {
			lancamento.setDataBaixa(new Date());
			lancamento.setValorPago(lancamento.getValor());
			lancamentoService.salvar(lancamento, " Baixado!");
			
			MovimentoCaixa movCaixa = new MovimentoCaixa();

			movCaixa.setCaixa(caixaService.caixaAberto());
			movCaixa.setHora(lancamento.getDataOperacao());
			movCaixa.setUsuario(lancamento.getUsuario());
			movCaixa.setValorMovimentoCaixa(lancamento.getValor());

			if (lancamento.getTipoLancamento().equals("R")) {
				movCaixa.setEntradaSaida("E");
			} else if (lancamento.getTipoLancamento().equals("P")) {
				movCaixa.setEntradaSaida("S");
			}
			
			movCaixaService.registrarMovimentoCaixa(movCaixa);
			
			getValoresVencidos();
			consutaDeLancamentos();
		} else {
			BigDecimal valorRestante = BigDecimal.ZERO;
			StringBuilder documento = new StringBuilder();
			StringBuilder observacao = new StringBuilder();

			valorRestante = valorRestante.add(lancamento.getValor().subtract(valorBaixaParcial));
			Lancamento lancamentoBaixaParcial = new Lancamento();

			lancamentoBaixaParcial.setCliente(lancamento.getCliente());
			lancamentoBaixaParcial.setCondicaoPagamento(lancamento.getCondicaoPagamento());
			lancamentoBaixaParcial.setDataBaixa(new Date());
			lancamentoBaixaParcial.setDataOperacao(lancamento.getDataOperacao());
			lancamentoBaixaParcial.setDataVencimento(lancamento.getDataVencimento());
			lancamentoBaixaParcial.setDescricao(lancamento.getDescricao());
			lancamentoBaixaParcial.setDocumento(documento.append(lancamento.getDocumento()).append("_P").toString());
			lancamentoBaixaParcial.setEmpresa(lancamento.getEmpresa());
			lancamentoBaixaParcial.setIntervalo(lancamento.getIntervalo());
			lancamentoBaixaParcial.setTipoLancamento(lancamento.getTipoLancamento());
			lancamentoBaixaParcial.setUsuario(lancamento.getUsuario());
			lancamentoBaixaParcial.setMovimento(lancamento.getMovimento());
			lancamentoBaixaParcial.setObservacao(
					observacao.append(lancamento.getObservacao()).append(" Titulo Baixado Parcialmente").toString());
			lancamentoBaixaParcial.setParcela(lancamento.getParcela());
			lancamentoBaixaParcial.setUsuario(lancamento.getUsuario());
			lancamentoBaixaParcial.setValor(valorBaixaParcial);
			lancamentoBaixaParcial.setValorPago(valorBaixaParcial);

			lancamento.setValor(valorRestante);

			lancamentoService.salvar(lancamentoBaixaParcial, " Baixado Parcialmente");

			lancamentoService.salvar(lancamento, null);

			MovimentoCaixa movCaixa = new MovimentoCaixa();

			movCaixa.setCaixa(caixaService.caixaAberto());
			movCaixa.setHora(lancamentoBaixaParcial.getDataOperacao());
			movCaixa.setUsuario(lancamentoBaixaParcial.getUsuario());
			movCaixa.setValorMovimentoCaixa(lancamentoBaixaParcial.getValor());

			if (lancamentoBaixaParcial.getTipoLancamento().equals("R")) {
				movCaixa.setEntradaSaida("E");
			} else if (lancamentoBaixaParcial.getTipoLancamento().equals("P")) {
				movCaixa.setEntradaSaida("S");
			}

			movCaixaService.registrarMovimentoCaixa(movCaixa);

			getValoresVencidos();
			consutaDeLancamentos();
		}
	}

	public void excluir(ActionEvent actionEvent) {
		lancamento = (Lancamento) actionEvent.getComponent().getAttributes().get("lancamentoExcluido");
		lancamentoService.deletar(lancamento);
		getValoresVencidos();
		consutaDeLancamentos();
	}

	public void lancamentoBaixaPacialOuTotal(ActionEvent event) {
		lancamento = (Lancamento) event.getComponent().getAttributes().get("tituloBaixado");
	}

	public String getValorTotalDosLancamento() {
		BigDecimal valor = BigDecimal.ZERO;
		if (listaLancamento == null) {
			return NumberFormat.getCurrencyInstance().format(valor);
		} else {
			for (Lancamento lancamento : listaLancamento) {
				valor = valor.add(lancamento.getValor());
			}
			return NumberFormat.getCurrencyInstance().format(valor);
		}

	}

	public String getValoresVencidos() {
		BigDecimal valorVencido = lancamentoDao.valorTotalVencidos();

		if (valorVencido != null) {
			return NumberFormat.getCurrencyInstance().format(lancamentoDao.valorTotalVencidos());
		} else {
			return NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO.doubleValue());
		}
	}

	public String getValorEmAberto() {
		BigDecimal valorAberto = lancamentoDao.valorTotalDosLancamento(IS_NULL);

		if (valorAberto != null) {
			return NumberFormat.getCurrencyInstance().format(lancamentoDao.valorTotalDosLancamento(IS_NULL));
		} else {
			return NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO.doubleValue());
		}
	}

	public String getValoresPagos() {
		BigDecimal valorPago = lancamentoDao.valorTotalDosLancamento(IS_NOT_NULL);

		if (valorPago != null) {
			return NumberFormat.getCurrencyInstance().format(lancamentoDao.valorTotalDosLancamento(IS_NOT_NULL));
		} else {
			return NumberFormat.getCurrencyInstance().format(BigDecimal.ZERO.doubleValue());
		}
	}

	public void onTabChange(TabChangeEvent event) {
		String nome = event.getTab().getTitle();

		if (nome.equals("Cadastro")) {
			pesquisaLancamento = new LancamentoDTO();
		}

		if (nome.equals("Consulta")) {
			desativar();
		}
	}

	public Date getDataAtual() {
		return dataAtual;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public List<Lancamento> getListaLancamento() {
		return listaLancamento;
	}

	public void setListaLancamento(List<Lancamento> listaLancamento) {
		this.listaLancamento = listaLancamento;
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public static Integer getPARCELA() {
		return PARCELA;
	}

	public static void setPARCELA(Integer pARCELA) {
		PARCELA = pARCELA;
	}

	public LancamentoDTO getPesquisaLancamento() {
		return pesquisaLancamento;
	}

	public String getOpcao() {
		return opcao;
	}

	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

	public int getOpcaoLancamento() {
		return opcaoLancamento;
	}

	public void setOpcaoLancamento(int opcaoLancamento) {
		this.opcaoLancamento = opcaoLancamento;
	}

	public BigDecimal getValorBaixaParcial() {
		return valorBaixaParcial;
	}

	public void setValorBaixaParcial(BigDecimal valorBaixaParcial) {
		this.valorBaixaParcial = valorBaixaParcial;
	}

	// private double roundToDecimals(double d, int c) {
	// int temp=(int)((d*Math.pow(10,c)));
	// return (((double)temp)/Math.pow(10,c));
	// }
	//
	// public double valorParcelas(Integer qtdParcelas){
	// double d = lancamento.getValor().doubleValue()/qtdParcelas;
	// return roundToDecimals(d, 3);
	// }

	private Date primeiaDataDoMes() {
		Calendar primeiroDia = Calendar.getInstance();
		primeiroDia.set(Calendar.DAY_OF_MONTH, 1);

		return primeiroDia.getTime();
	}

	public boolean isVencido() {
		List<Lancamento> lista = listaLancamento;
		Collections.sort(lista);

		for (Lancamento lancamento : lista) {
			Date dataAtual = new Date();
			if (dataAtual.after(lancamento.getDataVencimento())) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
