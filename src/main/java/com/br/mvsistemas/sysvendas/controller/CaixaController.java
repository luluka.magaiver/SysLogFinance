package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.model.Caixa;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.CaixaService;
import com.br.mvsistemas.sysvendas.service.MovimentoCaixaService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class CaixaController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6674298744030860792L;

	private boolean ativo = false;

	@Inject
	private FacesMessages mensage;

	@Inject
	private CaixaService caixaService;
	
	@Inject
	private MovimentoCaixaService movimentoCaixaService;

	@Getter
	@Setter
	private Caixa caixa;

	@Getter
	private Caixa caixaSelecionado;

	private List<Caixa> listaCaixa;

	Date hoje = new Date();
	SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");

	@PostConstruct
	public void consultar() {
		caixa = new Caixa();
		caixaSelecionado = new Caixa();
		listaCaixa = caixaService.listaCaixa();
	}

	public boolean isVerificaCaixaAberto() {
		return caixaService.isCaixaAberto();
	}

	public void getAbrirCaixa() {
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("usuario");

		caixa.setUsuario(usuario);
		caixa.setAberto("A");
		caixa.setDataAbertuda(new Date());
		caixaService.salvar(caixa);
		
		MovimentoCaixa movCaixa = new MovimentoCaixa();
		movCaixa.setCaixa(caixa);
		movCaixa.setEntradaSaida("E");
		movCaixa.setHora(new Date());
		movCaixa.setUsuario(usuario);
		movCaixa.setValorMovimentoCaixa(caixa.getValorAbertura());
		movCaixa.setObservacao("Abertura de Caixa");
		
		movimentoCaixaService.registrarMovimentoCaixa(movCaixa);
		consultar();
		desativar();
	}

	public void getEditarCaixa(ActionEvent actionEvent) {
		caixaSelecionado = ((Caixa) actionEvent.getComponent().getAttributes().get("caixaEditado"));
	}

	public void getFecharCaixa() {
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("usuario");
		
		caixaSelecionado.setUsuario(usuario);
		caixaSelecionado.setAberto("F");
		caixaSelecionado.setDataFechamento(new Date());
		caixaService.fechar(caixaSelecionado);
		
		MovimentoCaixa movCaixa = new MovimentoCaixa();
		movCaixa.setCaixa(caixaSelecionado);
		movCaixa.setEntradaSaida("E");
		movCaixa.setHora(new Date());
		movCaixa.setUsuario(usuario);
		movCaixa.setValorMovimentoCaixa(caixaSelecionado.getValorFechamento());
		movCaixa.setObservacao("Fechamento de Caixa");
		
		movimentoCaixaService.registrarMovimentoCaixa(movCaixa);
		
		caixaSelecionado = null;
		consultar();
		desativar();
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void desativar() {
		ativo = false;
	}

	public void ativar() {
		if (isVerificaCaixaAberto()) {
			mensage.alert("Já existe um caixa aberto!");
		} else {
			ativo = true;
		}
	}

	public List<Caixa> getListaCaixa() {
		return listaCaixa;
	}

}
