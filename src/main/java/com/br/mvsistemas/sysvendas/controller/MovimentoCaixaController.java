package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.dto.MovimentoCaixaDTO;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;
import com.br.mvsistemas.sysvendas.service.MovimentoCaixaService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;


@Named
@ViewScoped
public class MovimentoCaixaController implements Serializable{


	private static final long serialVersionUID = 5156772763045446449L;
	
	@Inject
	private FacesMessages mensage;
	@Inject
	private MovimentoCaixaService movimentoCaixaSerivce;
	private List<MovimentoCaixa> listaMovimentoCaixa;
	private MovimentoCaixaDTO movimentoCaixaDto;
	
	@PostConstruct
	public void init() {
		movimentoCaixaDto = new MovimentoCaixaDTO();
		movimentoCaixaDto.setDateIncial(primeiaDataDoMes());
		movimentoCaixaDto.setDateFinal(new Date());
	}
	
	public void consultarMovimentoCaixa() {
		List<MovimentoCaixa> movimentosCaixas = movimentoCaixaSerivce.consultaDeMovimentoCaixa(movimentoCaixaDto.getDateIncial(), movimentoCaixaDto.getDateFinal());
		
		if (movimentosCaixas.size() <= 0) {
			mensage.alert("Nenhuma movimentação de caixa encontrado!");
			listaMovimentoCaixa = null;
			return;
		} else {
			listaMovimentoCaixa = movimentosCaixas;
			mensage.info("A consulta retornou "+listaMovimentoCaixa.size()+" dados!");
		}
	}
	
	private Date primeiaDataDoMes() {
		Calendar primeiroDia = Calendar.getInstance();
		primeiroDia.set(Calendar.DAY_OF_MONTH, 1);
		return primeiroDia.getTime();
	}
	
	public MovimentoCaixaDTO getMovimentoCaixaDto() {
		return movimentoCaixaDto;
	}
	
	public void setMovimentoCaixaDto(MovimentoCaixaDTO movimentoCaixaDto) {
		this.movimentoCaixaDto = movimentoCaixaDto;
	}
	
	public List<MovimentoCaixa> getListaMovimentoCaixa() {
		return listaMovimentoCaixa;
	}
}
