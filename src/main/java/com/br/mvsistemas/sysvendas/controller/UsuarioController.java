package com.br.mvsistemas.sysvendas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dto.UsuarioDTO;
import com.br.mvsistemas.sysvendas.model.Empresa;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.EmpresaService;
import com.br.mvsistemas.sysvendas.service.FotoService;
import com.br.mvsistemas.sysvendas.service.UsuarioService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.RenderizarImagem;

@Named
@ViewScoped
public class UsuarioController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1880853359711773749L;

	//private GenericDao<Usuario> usuarioDao;
	private Usuario usuario;
	private Usuario usuarioEdicao;
	private Usuario usuarioSelecionado;
	private List<Usuario> usuarioList;
	private List<Usuario> usuarioListDash;
	private List<Empresa> listaEmpresas;
	private UsuarioDTO pesquisaUsuario;
	private StreamedContent imagemUsuario;
	private Part foto;
	private boolean ativo = false;
	
	@Inject
	private FacesMessages mensage;
	
	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private EmpresaService empresaService;
	
	@Inject
	private FotoService fotoService;
	
	@PostConstruct
	public void init() {
		pesquisaUsuario = new UsuarioDTO();
		usuarioEdicao = new Usuario();
		listaEmpresas = empresaService.listaEmpresas();
		usuarioListDash = usuarioService.listaDeUsuariosDash();
	}

//	public void listaTodosUsuarios() {
//		usuarioDao = new GenericDaoImpl<>(Usuario.class);
//		usuarioList = usuarioDao.listScript("from Usuario u Where u.cpf <> '529.447.364-00'");
//		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("listaDeUsuarios", usuarioList);
//	}
	
	public void salvar() {
		try {
			usuarioEdicao.setDataCriacao(new Date());
			
			if (usuarioEdicao != null && StringUtils.isNotEmpty(usuarioEdicao.getFotoUsuario())) {
				try {
					fotoService.deletar(usuarioEdicao.getFotoUsuario());
				} catch (IOException e) {
					mensage.alert(e.getMessage());
				}
			}
			
			try {
				fotoService.recuperarFotoTemporaria(usuarioEdicao.getFotoUsuario());
			} catch (IOException e) {
				mensage.alert("Problemas ao recuperar foto temporaria!");
			}
			
			usuarioService.salvar(usuarioEdicao);
			usuarioEdicao = new Usuario();
			desativar();
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuarioEditado");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void excluir(ActionEvent actionEvent){
		usuarioSelecionado = (Usuario) actionEvent.getComponent().getAttributes().get("usuarioExcluido");
		try {
			usuarioService.deletar(usuarioSelecionado);
			consutaDeUsuario();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editar(ActionEvent actionEvent){
		usuarioEdicao = (Usuario) actionEvent.getComponent().getAttributes().get("usuarioEditado");
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuarioEditado", usuarioEdicao);
		ativar();
	}
	
	public void consutaDeUsuario() {
		List<Usuario> usuarios = usuarioService.consultaDeUsuarios(pesquisaUsuario.getNome());
		if (usuarios.size() <= 0 ) {
			mensage.alert("Nenhum Usuario Encontrado");
			usuarioList = null;
			return;
		} else {
			usuarioList = usuarios;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("listaDeUsuarios", usuarioList);
		}
	}
	
	public void upload(FileUploadEvent fileUploadEvent){
		UploadedFile uploadedFile = fileUploadEvent.getFile();
		try {
			byte[] fotoRenderizada = RenderizarImagem.renderizar(uploadedFile.getContents(), 100, 150);
			String foto = fotoService.salvarFotoTemp(uploadedFile.getFileName(),fotoRenderizada);
			usuarioEdicao.setFotoUsuario(foto);
		} catch (Exception e) {
			mensage.error(e.getMessage());
		}
	}
	
	public void removerFoto() {
		try {
			fotoService.deletarTemp(usuarioEdicao.getFotoUsuario());
		} catch (IOException e) {
			mensage.alert(e.getMessage());
		}
		
		usuarioEdicao.setFotoUsuario(null);
	}
	
	public void ativar(){
		ativo = true;
	}
	
	public void desativar(){
		ativo = false;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuarioEditado");
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioEdicao() {
		return usuarioEdicao;
	}

	public void setUsuarioEdicao(Usuario usuarioEdicao) {
		this.usuarioEdicao = usuarioEdicao;
	}

	public List<Usuario> getUsuarioList() {
		return usuarioList;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
		this.usuarioList = usuarioList;
	}

	public StreamedContent getImagemUsuario() {
		return imagemUsuario;
	}

	public void setImagemUsuario(StreamedContent imagemUsuario) {
		this.imagemUsuario = imagemUsuario;
	}

	public Part getFoto() {
		return foto;
	}

	public void setFoto(Part foto) {
		this.foto = foto;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}
	
	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public UsuarioDTO getPesquisaUsuario() {
		return pesquisaUsuario;
	}

	public void setPesquisaUsuario(UsuarioDTO pesquisaUsuario) {
		this.pesquisaUsuario = pesquisaUsuario;
	}
	
	public List<Empresa> getListaEmpresas() {
		return listaEmpresas;
	}

	public List<Usuario> getUsuarioListDash() {
		return usuarioListDash;
	}
	
	
	
}
