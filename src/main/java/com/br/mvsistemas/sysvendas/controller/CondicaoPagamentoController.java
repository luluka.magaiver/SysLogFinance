package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;
import com.br.mvsistemas.sysvendas.service.CondicaoPagamentoService;

@Named
@ViewScoped
public class CondicaoPagamentoController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5694254836433916569L;

	@Inject
	private CondicaoPagamentoService condicaoPagamentoService;
	private CondicaoPagamento condicaoPagamento;
	private List<CondicaoPagamento> listaCondicaoPagamento;
	private boolean ativo = false;
	
	
	@PostConstruct
	public void init(){
		condicaoPagamento = new CondicaoPagamento();
		listaCondicaoPagamento = condicaoPagamentoService.listaCondicaoPagamento();
	}
	
	public List<CondicaoPagamento> condicoesPagamentos(){
		return condicaoPagamentoService.listaCondicaoPagamento();
	}
	
	public void salvar(){
		condicaoPagamentoService.salvar(condicaoPagamento);
		init();
		desativar();
	}
	
	public void editar(ActionEvent actionEvent) {
		condicaoPagamento = ((CondicaoPagamento) actionEvent.getComponent().getAttributes().get("condicaoEditado"));
		ativar();
	}
	
	public void ativar() {
		ativo = true;
	}
	
	public void desativar() {
		ativo = false;
	}
	
	public void iniciar(){
		ativo = true;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	public CondicaoPagamento getCondicaoPagamento() {
		return condicaoPagamento;
	}
	
	public void setCondicaoPagamento(CondicaoPagamento condicaoPagamento) {
		this.condicaoPagamento = condicaoPagamento;
	}
	
	public List<CondicaoPagamento> getListaCondicaoPagamento() {
		return listaCondicaoPagamento;
	}
	
	public void setListaCondicaoPagamento(List<CondicaoPagamento> listaCondicaoPagamento) {
		this.listaCondicaoPagamento = listaCondicaoPagamento;
	}
	
}
