package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.model.Cfop;
import com.br.mvsistemas.sysvendas.model.ConfiguracaoEntradaSaida;
import com.br.mvsistemas.sysvendas.model.TipoClienteEnum;
import com.br.mvsistemas.sysvendas.model.TipoCstEnum;
import com.br.mvsistemas.sysvendas.model.TipoSimNaoEnum;
import com.br.mvsistemas.sysvendas.service.ConfiguracaoEntradaSaidaService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

@Named
@ViewScoped
public class ConfiguracaoEntradaSaidaController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2372228536534700229L;
	
	@Inject
	private FacesMessages mensage;
	
	@Inject
	private ConfiguracaoEntradaSaidaService entradaSaidaService;
	
	private ConfiguracaoEntradaSaida configuracaoEntradaSaida;
	
	private ConfiguracaoEntradaSaida configuracaoEntradaSaidaSelecionada;
	
	private List<ConfiguracaoEntradaSaida> entradaSaidas;
	
	private boolean ativo = false;
	
	@PostConstruct
	public void consultar(){
		configuracaoEntradaSaida = new ConfiguracaoEntradaSaida();
	}
	
	public void ativar(){
		ativo = true;
	}
	
	public void desativar(){
		configuracaoEntradaSaida = new ConfiguracaoEntradaSaida();
		ativo = false;
	}
	
	public void salvar(){
		entradaSaidaService.salvar(configuracaoEntradaSaida);
		desativar();
	}
	
	public void editar(ActionEvent actionEvent){
		configuracaoEntradaSaida = (ConfiguracaoEntradaSaida) actionEvent.getComponent().getAttributes().get("entradaSaidaEditada");
		ativar();
	}
	
	public void excluir(ActionEvent actionEvent){
		configuracaoEntradaSaidaSelecionada = (ConfiguracaoEntradaSaida) actionEvent.getComponent().getAttributes().get("entradaSaidaExcuida");
		
		try {
			entradaSaidaService.deletar(configuracaoEntradaSaidaSelecionada);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public TipoSimNaoEnum[] getSimNao(){
		return TipoSimNaoEnum.values();
	}
	
	public TipoCstEnum[] getCst(){
		return TipoCstEnum.values();
	}
	
	public ConfiguracaoEntradaSaida getConfiguracaoEntradaSaida() {
		return configuracaoEntradaSaida;
	}

	public void setConfiguracaoEntradaSaida(ConfiguracaoEntradaSaida configuracaoEntradaSaida) {
		this.configuracaoEntradaSaida = configuracaoEntradaSaida;
	}

	public List<Cfop> getCfops() {
		return entradaSaidaService.cfops();
	}

	public boolean isAtivo() {
		return ativo;
	}

	public ConfiguracaoEntradaSaida getConfiguracaoEntradaSaidaSelecionada() {
		return configuracaoEntradaSaidaSelecionada;
	}

	public void setConfiguracaoEntradaSaidaSelecionada(ConfiguracaoEntradaSaida configuracaoEntradaSaidaSelecionada) {
		this.configuracaoEntradaSaidaSelecionada = configuracaoEntradaSaidaSelecionada;
	}

	public List<ConfiguracaoEntradaSaida> getEntradaSaidas() {
		return entradaSaidas;
	}
	
}
