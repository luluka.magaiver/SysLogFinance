package com.br.mvsistemas.sysvendas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.br.mvsistemas.sysvendas.dao.ClienteDao;
import com.br.mvsistemas.sysvendas.dao.ProdutoDao;
import com.br.mvsistemas.sysvendas.daoimpl.ClienteDaoImpl;
import com.br.mvsistemas.sysvendas.dto.MovimentoDTO;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;
import com.br.mvsistemas.sysvendas.model.Estoque;
import com.br.mvsistemas.sysvendas.model.ItemMovimento;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.model.Produto;
import com.br.mvsistemas.sysvendas.model.TipoStatusMovimento;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.CaixaService;
import com.br.mvsistemas.sysvendas.service.ClienteService;
import com.br.mvsistemas.sysvendas.service.CondicaoPagamentoService;
import com.br.mvsistemas.sysvendas.service.ItemMovimentoService;
import com.br.mvsistemas.sysvendas.service.LancamentoService;
import com.br.mvsistemas.sysvendas.service.MovimentoService;
import com.br.mvsistemas.sysvendas.service.ProdutoService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.NegocioException;
import com.br.mvsistemas.sysvendas.util.SysVendasUtil;

import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;

@Named
@ViewScoped
public class MovimentoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5623934413465283079L;

	private boolean skip;
	private static Movimento impressaoMovimento;
	private Movimento movimento;
	@Getter
	@Setter
	private Cliente clienteRelatorio;
	@Getter
	@Setter
	private Movimento movimentoSelecionado;
	@Getter
	private List<ItemMovimento> listaItensMovimento;
	private Produto produtoSelecionado;
	private Cliente cliente;
	private List<Cliente> listaClientes;
	private List<Estoque> listaProdutos;
	private List<CondicaoPagamento> listaCondicaoPagamento;
	@Getter
	@Setter
	private List<Movimento> listaMovimentos;
	@Getter
	private TreeNode raiz;

	private ClienteDao clienteDao;
	private ProdutoDao produtoDao;
	private BigDecimal valorProduto;

	@NotNull
	private BigDecimal quantidadeProduto = BigDecimal.ZERO;
	private BigDecimal descontoProduto;
	private BigDecimal totalItem;
	private BigDecimal valorTotalItens = BigDecimal.ZERO;
	private String statusVenda;
	@Getter
	private MovimentoDTO pesquisaMovimento;

	private boolean ativo = false;

	@Inject
	private ClienteService clienteService;

	@Inject
	private ProdutoService produtoService;

	@Inject
	private FacesMessages mensagem;

	@Inject
	private MovimentoService movimentoService;
	
	@Inject
	private LancamentoService lancamentoService;

	@Inject
	private ItemMovimentoService itemMovimentoService;

	@Inject
	private CondicaoPagamentoService condicaoPagamentoService;
	
	@Inject
	private CaixaService caixaService;

	@PostConstruct
	public void init() {
		movimento = new Movimento();
		clienteDao = new ClienteDaoImpl();
		listaClientes = clienteDao.list();
		listaCondicaoPagamento = condicaoPagamentoService.listaCondicaoPagamento();
		pesquisaMovimento = new MovimentoDTO();
		raiz = new DefaultTreeNode("Raiz", null);
		// setStatusVenda("Venda ");
	}

	public void getIniciarVenda() {
		if (caixaService.isCaixaAberto()) {
			ativar();
			movimento.setDataMovimento(new Date());
			movimento.setStatus(TipoStatusMovimento.V);
			movimento.setNumeroMovimento(SysVendasUtil.numero());
			setStatusVenda(TipoStatusMovimento.V.getDescricao() + " Numero: " + movimento.getNumeroMovimento());
		} else {
			mensagem.alert("Não existe caixa aberto!");
		}
		
	}

	public void getFinalizarVenda() {
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("usuario");
		// if (movimento.getStatus() == TipoStatusMovimento.V) {
		// movimento.setTotalMovimento(valorTotalItens);
		// }

		impressaoMovimento = movimento;
		
		movimento.setTotalMovimento(valorTotalItens);

		if (movimento.getStatus() == TipoStatusMovimento.D) {
			movimento.setIdMovimentoOrgem(movimentoSelecionado.getId());
		}

		movimento.setUsuarioMovimento(usuario);
		
		movimentoService.Salvar(movimento);

		if (movimento.getStatus() == TipoStatusMovimento.D) {
			movimentoSelecionado.setIdMovimentoOrgem(movimento.getId());
			movimentoService.alterarIdMovimentoOrigem(movimentoSelecionado);
			
		}
		
		desativar();
	}
	
	
	public void impressaoVendaDevolucao(ActionEvent actionEvent) {
		movimentoSelecionado = (Movimento) actionEvent.getComponent().getAttributes().get("movimento");
		try {
			movimentoService.impressaoVendaDevolucao(movimentoSelecionado);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getSelecionouCliente(SelectEvent event) {
		Cliente c = clienteService.cliente(movimento.getClienteMovimento());
	}
	
	public void getSelecionouClienteRelatorio(SelectEvent event) {
		clienteRelatorio = clienteService.cliente(cliente);
	}

	public void getSelecionouProduto(SelectEvent event) {
		Produto p = produtoService.produto(produtoSelecionado);
		setValorProduto(p.getPrecoVenda().setScale(2, RoundingMode.HALF_EVEN));
	}

	public List<Cliente> clientes(String query) {
		String nome = "";
		nome.equalsIgnoreCase(query);
		listaClientes = clienteDao.list();
		List<Cliente> sugestoes = new ArrayList<Cliente>();
		for (Cliente c : listaClientes) {
			if (c.getNome().startsWith(nome)) {
				sugestoes.add(c);
			}
		}
		if (sugestoes.size() == 0) {
			mensagem.alert("Cliente Não encontrado!");
		}
		return sugestoes;
	}

	public List<Produto> produtos(String query) {
		String nome = query.toUpperCase();
		query.equalsIgnoreCase(query.toUpperCase());
		List<Produto> sugestoes = new ArrayList<Produto>();
		for (Produto p : produtoService.listaProdutos()) {
			if (p.getNomeProduto().startsWith(nome)) {
				sugestoes.add(p);
			}
		}
		if (sugestoes.size() == 0) {
			mensagem.alert("Produto Não encontrado!");
		}
		return sugestoes;
	}

	public String getValorTotalDosItemsMovimento() {
		valorTotalItens = BigDecimal.ZERO;

		if (movimento.getItensMovimento().size() == 0) {
			return NumberFormat.getCurrencyInstance().format(valorTotalItens);
		} else {
			for (ItemMovimento im : movimento.getItensMovimento()) {
				valorTotalItens = valorTotalItens.add(im.getTotalItemMovimento());
			}
			return NumberFormat.getCurrencyInstance().format(valorTotalItens);
		}

	}

	public void getInserirItemMovimento() {
		ItemMovimento item = new ItemMovimento();

		if (quantidadeProduto == null) {
			mensagem.alert("Informar quantidade vendida!");
			return;
		}

		if (quantidadeProduto.compareTo(produtoSelecionado.getQuantidade()) == 1) {
			mensagem.alert("Saldo de Estoque do produto Insuficiente");
			return;
		}

		itemMovimentoService.baixarEstoqueProduto(produtoSelecionado, quantidadeProduto);

		BigDecimal subtotal = quantidadeProduto.multiply(valorProduto);
		BigDecimal totalComDesconto = subtotal.multiply(descontoProduto == null ? BigDecimal.ZERO : descontoProduto)
				.divide(new BigDecimal(100.00)).setScale(2, RoundingMode.HALF_EVEN);

		setTotalItem(subtotal.subtract(totalComDesconto));

		item.setMovimento(movimento);
		item.setItemMovimento(produtoSelecionado);
		item.setQuantidadeItemMovimento(quantidadeProduto);
		item.setTotalItemMovimento(getTotalItem());
		item.setValorItemMovimento(valorProduto);
		item.setDescontoItemMovimento(descontoProduto == null ? BigDecimal.ZERO : descontoProduto);

		if (existeItemComProduto(item.getItemMovimento())) {
			mensagem.alert("Já existe um item no pedido com o produto informado.");
			zerarInserirItem();
		} else {
			movimento.getItensMovimento().add(item);
			zerarInserirItem();
		}

	}
	
	public void gerarRelatorioMovimentoCliente() {
		try {
			movimentoService.relatorioCompasDeCliente(clienteRelatorio);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void zerarInserirItem() {
		produtoSelecionado = new Produto();
		quantidadeProduto = BigDecimal.ZERO;
		valorProduto = BigDecimal.ZERO;
		descontoProduto = BigDecimal.ZERO;
		totalItem = BigDecimal.ZERO;
	}

	private boolean existeItemComProduto(Produto produto) {
		boolean existeItem = false;

		for (ItemMovimento item : movimento.getItensMovimento()) {
			if (produto.equals(item.getItemMovimento())) {
				existeItem = true;
				break;
			}
		}

		return existeItem;
	}

	public void roolbackEstoqueProduto(ActionEvent actionEvent) {
		ItemMovimento item = (ItemMovimento) actionEvent.getComponent().getAttributes().get("itensMovimento");
		itemMovimentoService.saldoEstoqueProduto(item.getItemMovimento(), item.getQuantidadeItemMovimento());
	}

	public void consultaDeMovimentos() {
		listaMovimentos = null;

		List<Movimento> movimentos = movimentoService.listaDeMovimentos(pesquisaMovimento.getNomeCliente(),
				pesquisaMovimento.getNumeroVenda(), pesquisaMovimento.getDateIncial(),
				pesquisaMovimento.getDateFinal());

		if (movimentos.size() <= 0) {
			mensagem.alert("Nenhum Movimento Encontrado");
			listaMovimentos = null;
			return;
		} else {
			listaMovimentos = movimentos;

			for (Movimento movimento : listaMovimentos) {
				TreeNode movimentoRaiz = new DefaultTreeNode(movimento, raiz);
				for (ItemMovimento item : movimento.getItensMovimento()) {
					new DefaultTreeNode(item, movimentoRaiz);
				}
			}
		}
	}

	public void itensMovimento(ActionEvent actionEvent) {
		movimentoSelecionado = (Movimento) actionEvent.getComponent().getAttributes().get("movimento");
	}

	public void devolucao(ActionEvent actionEvent) {
		movimentoSelecionado = (Movimento) actionEvent.getComponent().getAttributes().get("movimento");

		movimento.setNumeroMovimento(SysVendasUtil.numero());
		setStatusVenda(TipoStatusMovimento.D.getDescricao() + " Numero: " + movimento.getNumeroMovimento());
		movimento.setStatus(TipoStatusMovimento.D);

		movimento.setClienteMovimento(movimentoSelecionado.getClienteMovimento());
		movimento.setCondicaoPagamento(movimentoSelecionado.getCondicaoPagamento());
		movimento.setDataMovimento(new Date());
		movimento.setDescontoMovimento(movimentoSelecionado.getDescontoMovimento().negate());
		movimento.setFreteMovimento(movimentoSelecionado.getFreteMovimento().negate());
		movimento.setIdMovimentoOrgem(movimentoSelecionado.getId());

		for (ItemMovimento item : movimentoSelecionado.getItensMovimento()) {
			ItemMovimento itemNegativo = new ItemMovimento();
			itemNegativo.setDescontoItemMovimento(item.getDescontoItemMovimento().negate());
			itemNegativo.setItemMovimento(item.getItemMovimento());
			itemNegativo.setMovimento(movimento);
			itemNegativo.setQuantidadeItemMovimento(item.getQuantidadeItemMovimento());
			itemNegativo.setTotalItemMovimento(item.getTotalItemMovimento().negate());
			itemNegativo.setValorItemMovimento(item.getValorItemMovimento().negate());
			movimento.getItensMovimento().add(itemNegativo);
		}

		movimento.setTotalMovimento(valorTotalItens.negate());
		ativar();
	}

	public void excluir(ActionEvent actionEvent) {
		movimentoSelecionado = (Movimento) actionEvent.getComponent().getAttributes().get("movimento");
		boolean pode = movimentoService.isCanecelarMovimento(movimentoSelecionado);

		if (pode) {
			Lancamento lancamento = lancamentoService.conultarLancamentoPorMovimento(movimentoSelecionado.getId());
			try {
				lancamentoService.deletar(lancamento);
				movimentoService.deletar(movimentoSelecionado);
			} catch (Exception e) {
				new NegocioException("Erro no processo de Exclusão de Pedido");
			}
		} else {
			mensagem.alert("Impossivel deletar Pedido!");
		}
	}

	public void cancelarPedido(ActionEvent actionEvent) {
		movimentoSelecionado = (Movimento) actionEvent.getComponent().getAttributes().get("movimento");
		
		for (ItemMovimento i : movimentoSelecionado.getItensMovimento()) {
			itemMovimentoService.saldoEstoqueProduto(i.getItemMovimento(), i.getQuantidadeItemMovimento());
		}

		movimentoSelecionado.setDataCancelamentoMovimento(new Date());
		movimentoSelecionado.setStatus(TipoStatusMovimento.C);

		movimentoService.Salvar(movimentoSelecionado);
		
		for (Lancamento l : lancamentoService.consultarLancamentosPorMovimentos(movimentoSelecionado.getId())) {
			lancamentoService.deletar(l);
		}
		/*
		 * nesse caso, colocar o pedido em status de cancelado, com a data de
		 * cancelamento colcoar os itens em status de cancelado, voltando o saldo de
		 * estoque.
		 */
		mensagem.alert("Pedido Cancelado");

		consultaDeMovimentos();
	}
	

	public boolean isSkip() {
		return skip;
	}

	public BigDecimal getDescontoProduto() {
		return descontoProduto;
	}

	public void setDescontoProduto(BigDecimal descontoProduto) {
		this.descontoProduto = descontoProduto;
	}

	public BigDecimal getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(BigDecimal totalItem) {
		this.totalItem = totalItem;
	}

	public BigDecimal getQuantidadeProduto() {
		return quantidadeProduto;
	}

	public void setQuantidadeProduto(BigDecimal quantidadeProduto) {
		this.quantidadeProduto = quantidadeProduto;
	}

	public BigDecimal getValorProduto() {
		return valorProduto;
	}

	public void setValorProduto(BigDecimal valorProduto) {
		this.valorProduto = valorProduto;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	public Movimento getMovimento() {
		return movimento;
	}

	public void setMovimento(Movimento movimento) {
		this.movimento = movimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public List<CondicaoPagamento> getListaCondicaoPagamento() {
		return listaCondicaoPagamento;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public String getStatusVenda() {
		return statusVenda;
	}

	public void setStatusVenda(String statusVenda) {
		this.statusVenda = statusVenda;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void ativar() {
		ativo = true;
	}

	public void desativar() {

		if (movimento.getItensMovimento().size() > 0) {
			for (ItemMovimento i : movimento.getItensMovimento()) {
				itemMovimentoService.saldoEstoqueProduto(i.getItemMovimento(), i.getQuantidadeItemMovimento());
			}
		}

		movimento = new Movimento();
		ativo = false;
		produtoSelecionado = new Produto();
		quantidadeProduto = null;
		valorProduto = null;
		descontoProduto = null;
		totalItem = null;
		valorTotalItens = BigDecimal.ZERO;
		setStatusVenda("");
	}
}
