package com.br.mvsistemas.sysvendas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.br.mvsistemas.sysvendas.dao.ClienteDao;
import com.br.mvsistemas.sysvendas.dao.EnderecoDao;
import com.br.mvsistemas.sysvendas.daoimpl.ClienteDaoImpl;
import com.br.mvsistemas.sysvendas.dto.ClienteDTO;
import com.br.mvsistemas.sysvendas.model.Cidades;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Endereco;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Telefone;
import com.br.mvsistemas.sysvendas.model.TipoClienteEnum;
import com.br.mvsistemas.sysvendas.model.TipoMesEnum;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.CidadeService;
import com.br.mvsistemas.sysvendas.service.ClienteService;
import com.br.mvsistemas.sysvendas.service.EnderecoService;
import com.br.mvsistemas.sysvendas.service.LancamentoService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

@Named
@ViewScoped
public class ClienteController implements Serializable {
	private static final long serialVersionUID = -7579269054820351136L;
	@Inject
	private FacesMessages mensage;
	private EnderecoDao enderecoDao;
	private ClienteDao clienteDao;
	private Cliente clienteEdicao;
	private Cliente clienteSelecionado;
	private List<Cliente> listaClientes;
	private List<Cliente> listaClienteDashboard;
	private List<Cidades> listaCidades;
	private ClienteDTO pesquisaCliente;
	private Endereco endereco;
	private Endereco enderecoSelecionado;
	private Telefone telefone;
	private Telefone telefoneSelecionado;
	private List<String> list;
	private Integer mes;
	private boolean ativo = false;
	@Inject
	private CidadeService cidadeService;
	@Inject
	private ClienteService service;
	@Inject
	private EnderecoService enderecoService;
	@Inject
	private LancamentoService lancamentoService;

	@PostConstruct
	public void consultar() {
		endereco = new Endereco();
		telefone = new Telefone();
		clienteDao = new ClienteDaoImpl();
		listaClienteDashboard = clienteDao.list();
		clienteEdicao = new Cliente(TipoClienteEnum.PF);
		pesquisaCliente = new ClienteDTO();

		carregaTiposEnderecos();
	}

	private void carregaTiposEnderecos() {
		list = null;
		list = new ArrayList<>();
		list.add("PRINCIPAL");
		list.add("ENTREGA");
		list.add("CORRESPONDENCIA");

		Collections.sort(list);
	}

	public List<Cidades> listarCidades(String query) {
		return cidadeService.todasCidades(query, listaCidades, mensage);
	}

	public void consutaDeClientes() {
		List<Cliente> clientes = service.consultaDeClientes(pesquisaCliente.getNome(), pesquisaCliente.getEmrpesa(), pesquisaCliente.getSetor());
		if (clientes.size() <= 0) {
			mensage.alert("Nenhum Cliente Encontrado");
			listaClientes = null;
			return;
		}
		listaClientes = clientes;
	}

	public void excluir(ActionEvent actionEvent) {
		clienteSelecionado = ((Cliente) actionEvent.getComponent().getAttributes().get("clienteExcluido"));
		List<Lancamento> listasDeLancamentos = lancamentoService.lancamentosPorCliente(clienteSelecionado);

		if (listasDeLancamentos.size() <= 0) {
			try {
				service.deletar(clienteSelecionado);
				carregaTiposEnderecos();
				consultar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			mensage.alert("Inpossivel excluir Cliente com lançamentos vinculados...");
			return;
		}
	}

	public void editar(ActionEvent actionEvent) {
		clienteEdicao = ((Cliente) actionEvent.getComponent().getAttributes().get("clienteEditado"));
		ativar();
	}

	@SuppressWarnings("unchecked")
	public void aniversario() {
		try {
			List<Cliente> aniversarinates = clienteDao.aniversariantes(mes);
			
			if (aniversarinates.size() > 0 && aniversarinates != null) {
				
				FacesContext fc = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
				session.setAttribute("aniversariantes", aniversarinates);
				session.setAttribute("mesAniversariantes", mes);
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("RelatorioAniversariante");
			} else {
				mensage.alert("Nenhum aniversariante no mês informado!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void buscarEndereco() {
		enderecoService.preencherEndereco(clienteEdicao, null, endereco);
	}

	public void adicionarTelefone() {
		enderecoService.adicionarTelefone(clienteEdicao, null, telefone);
	}

	public void removerTelefone() {
		enderecoService.removerTelefone(clienteEdicao, null, telefoneSelecionado);
		novoTel();
	}

	public void adicionarEndereco() {
		enderecoService.adicionarEndereco(clienteEdicao, endereco);
		novoEnd();
	}

	public void removerEndereco() {
		enderecoService.removerEndereco(clienteEdicao, enderecoSelecionado, list);
	}

	public void novoEnd() {
		endereco = new Endereco();
	}

	public void novoTel() {
		telefone = new Telefone();
	}

	public void addTel() {
		enderecoService.adicionarTelefone(clienteEdicao, null, telefone);
	}

	public void delTel() {
		enderecoService.removerTelefone(clienteEdicao, null, telefoneSelecionado);
	}

	public TipoClienteEnum[] getTiposCliente() {
		return TipoClienteEnum.values();
	}

	public TipoMesEnum[] getTipoMes() {
		return TipoMesEnum.values();
	}

	public List<String> getTiposDeEnderecos() {
		if (clienteEdicao.getEnderecos() == null) {
			return list;
		}

		if (clienteEdicao.getEnderecos() != null) {
			for (int i = 0; i < clienteEdicao.getEnderecos().size(); i++) {
				for (int j = 0; j < list.size(); j++) {
					String itemDaLista = ((String) list.get(j)).toString();
					String itemDoEndereco = ((Endereco) clienteEdicao.getEnderecos().get(i)).getTipo();

					if (itemDoEndereco.equals(itemDaLista)) {
						list.remove(((Endereco) clienteEdicao.getEnderecos().get(i)).getTipo());
					}
				}
			}
		}

		return list;
	}

	public void salvar() {
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("usuario");

		clienteEdicao.setUsuario(usuario);
		clienteEdicao.setEmpresa(usuario.getEmpresa());

		if (service.salvar(clienteEdicao)) {
			carregaTiposEnderecos();
			desativar();
		}
	}

	public void ativar() {
		ativo = true;
	}

	public void desativar() {
		ativo = false;
		clienteEdicao = new Cliente(TipoClienteEnum.PF);
		pesquisaCliente = new ClienteDTO();
	}

	public boolean isAtivo() {
		return ativo;
	}

	public Cliente getClienteEdicao() {
		return clienteEdicao;
	}

	public void setClienteEdicao(Cliente clienteEdicao) {
		this.clienteEdicao = clienteEdicao;
	}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public List<Cliente> getListaClienteDashboard() {
		return listaClienteDashboard;
	}

	public ClienteDTO getPesquisaCliente() {
		return pesquisaCliente;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Endereco getEnderecoSelecionado() {
		return enderecoSelecionado;
	}

	public void setEnderecoSelecionado(Endereco enderecoSelecionado) {
		this.enderecoSelecionado = enderecoSelecionado;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public Telefone getTelefoneSelecionado() {
		return telefoneSelecionado;
	}

	public void setTelefoneSelecionado(Telefone telefoneSelecionado) {
		this.telefoneSelecionado = telefoneSelecionado;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public List<Cidades> getListaCidades() {
		return listaCidades;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

}
