package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.br.mvsistemas.sysvendas.model.PlanoContas;
import com.br.mvsistemas.sysvendas.model.TipoPlanoContas;
import com.br.mvsistemas.sysvendas.model.TipoProdutoEnum;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.PlanoContasService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class PlanoContasController implements Serializable {

	private static final long serialVersionUID = 8476820186553944604L;

	@Inject
	private PlanoContasService planoContasService;

	@Inject
	private FacesMessages mensagem;

	@Getter
	@Setter
	private TreeNode root;

	private TreeNode selectedNode;

	@Getter
	@Setter
	private List<PlanoContas> liste;

	@Getter
	@Setter
	private List<PlanoContas> altListe;

	@Getter
	@Setter
	private List<PlanoContas> altListe2;

	@Getter
	@Setter
	private PlanoContas planoContasSelecionado;
	
	@Getter
	@Setter
	private PlanoContas planoContas;

	@PostConstruct
	public void consultar() {
		this.root = new DefaultTreeNode("Raiz", null);
		liste = new ArrayList<>();
		liste = planoContasService.todos();
		recursive(liste, 0l, root);
		planoContas = new PlanoContas();
	}

	public void recursive(List<PlanoContas> liste, Long id, TreeNode node) {
		altListe2 = new ArrayList<PlanoContas>();
		altListe2 = altKategorileriGetir(id);
		for (PlanoContas c : altListe2) {

			TreeNode childNode = new DefaultTreeNode(c.getDescricaoCompleta(), node);
			recursive(altListe2, c.getId(), childNode);
		}
	}

	public List<PlanoContas> altKategorileriGetir(Long i) {
		altListe = new ArrayList<PlanoContas>();
		for (PlanoContas c : getListe()) {
			if (c.getIdSuPlanoContas() == i) {
				altListe.add(c);
			}
		}
		return altListe;
	}

	public void testeDeSelecionar() {
		if (selectedNode != null) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("planoContasSelecionado", planoContasService.planoContas(selectedNode.getData().toString()));
			
		}
		
		Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 350);
        options.put("height", 260);
        options.put("resizable", false);
        options.put("draggable", false);
//        options.put("contentWidth", "100%");
//        options.put("contentHeight", "100%");
        options.put("id", "dlgNovoPlano");
         
        RequestContext.getCurrentInstance().openDialog("/planoconta/viewNewPlanoContas", options, null);
        
        
	}  
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	
	
	public void mostrarDescricaoPlano(){
		planoContasSelecionado = (PlanoContas) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("planoContasSelecionado");
		
		//mensagem.info("Plano Selecionado: "+planoContasSelecionado.getDescricao());
	}
	
	public void salvar(){
		planoContasSelecionado = (PlanoContas) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("planoContasSelecionado");
		planoContas.setCodigo(planoContasSelecionado.getCodigo()+'.'+planoContas.getCodigo());
	}
	
	public TipoPlanoContas[] getTipoPlanoContas(){
		return TipoPlanoContas.values();
	}
}
