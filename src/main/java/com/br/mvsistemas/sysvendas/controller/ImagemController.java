package com.br.mvsistemas.sysvendas.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.omg.CORBA.portable.ApplicationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.br.mvsistemas.sysvendas.model.Produto;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.util.RenderizarImagem;

@ManagedBean
@SessionScoped
public class ImagemController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4189672229556159609L;

	private StreamedContent imagem;
	private StreamedContent imagemProduto;
	private StreamedContent imagemDataTable;
	private StreamedContent imagemDataTableProduto;
	private byte[] foto;
	private Usuario usuarioEditado;
	private Produto produtoEditado;
	
	public StreamedContent getImagem() {
		usuarioEditado = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioEditado");
		
		if (usuarioEditado != null) {
			if (usuarioEditado.getId() != null) {
				if (usuarioEditado.getFoto() != null) {
					ByteArrayInputStream bais = new ByteArrayInputStream(usuarioEditado.getFoto());
					imagem = new DefaultStreamedContent(bais);
				}
				return imagem;
			} else {
				return imagem;
			}
		}
		return imagem;
	}
	
	public void setImagem(StreamedContent imagem) {
		this.imagem = imagem;
	}
	
	public StreamedContent getImagemProduto() {
		produtoEditado = (Produto) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("produtoEditado");
		
		if (produtoEditado != null) {
			if (produtoEditado.getId() != null) {
				if (produtoEditado.getFoto() != null) {
					ByteArrayInputStream bais = new ByteArrayInputStream(produtoEditado.getFoto());
					imagemProduto = new DefaultStreamedContent(bais);
				}
				return imagemProduto;
			} else {
				return imagemProduto;
			}
		}
		return imagemProduto;
	}
	
	public void setImagemProduto(StreamedContent imagemProduto) {
		this.imagemProduto = imagemProduto;
	}
	
	public byte[] getFoto() {
		return foto;
	}
	
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	
	public void importarImagem(FileUploadEvent event) {
		if (event != null) {
			foto = event.getFile().getContents();
			try {
				ByteArrayInputStream bais = new ByteArrayInputStream(RenderizarImagem.renderizar(foto, 100, 150));
				if (usuarioEditado != null) {
					if (usuarioEditado.getFoto() != null) {
						usuarioEditado.setFoto(null);
					} 
				}
				imagem = new DefaultStreamedContent(bais);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("foto", foto);
			} catch (ApplicationException e) {
				e.printStackTrace();
			}
		} else {
			
		}
	}
	
	public void importarImagemProduto(FileUploadEvent event) {
		if (event != null) {
			foto = event.getFile().getContents();
			try {
				ByteArrayInputStream bais = new ByteArrayInputStream(RenderizarImagem.renderizar(foto, 300, 400));
				if (produtoEditado != null) {
					if (produtoEditado.getFoto() != null) {
						produtoEditado.setFoto(null);
					} 
				}
				imagemProduto = new DefaultStreamedContent(bais);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("foto", foto);
			} catch (ApplicationException e) {
				e.printStackTrace();
			}
		} else {
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public StreamedContent getImagemDataTable() {
		List<Usuario> listaUsuarios = (List<Usuario>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("listaDeUsuarios");
		String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("image_id");

		if (id != null && listaUsuarios != null && !listaUsuarios.isEmpty()) {
			Long imagemId = Long.parseLong(id);
			for (Usuario imgTemp : listaUsuarios) {
				if (imgTemp.getId() == imagemId) {
					return imgTemp.getInputImage();
				}
			}
		}

		return new DefaultStreamedContent();
	}
	
	@SuppressWarnings("unchecked")
	public StreamedContent getImagemDataTableProduto() {
		List<Produto> listaProdutos = (List<Produto>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("listaDeProdutos");
		String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("image_id");

		if (id != null && listaProdutos != null && !listaProdutos.isEmpty()) {
			Long imagemId = Long.parseLong(id);
			for (Produto imgTemp : listaProdutos) {
				if (imgTemp.getId() == imagemId) {
					return imgTemp.getInputImageProduto();
				}
			}
		}

		return new DefaultStreamedContent();
	}
	
	public void setImagemDataTableProduto(StreamedContent imagemDataTableProduto) {
		this.imagemDataTableProduto = imagemDataTableProduto;
	}

	public void setImagemDataTable(StreamedContent imagemDataTable) {
		this.imagemDataTable = imagemDataTable;
	}
	
}
