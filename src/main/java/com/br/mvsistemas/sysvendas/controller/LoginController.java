package com.br.mvsistemas.sysvendas.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.flywaydb.core.Flyway;
import org.postgresql.ds.PGPoolingDataSource;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dao.UsuarioDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.UsuarioDaoImpl;
import com.br.mvsistemas.sysvendas.model.Empresa;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@ManagedBean
@SessionScoped
public class LoginController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2389852877404456321L;

	private String cpf;
	private String password;

	private boolean loginIn;
	private int sessionCount;
	private List<Usuario> usuarioList;
	private List<Empresa> listaDeEmpresas;
	private Usuario usuario = new Usuario();
	private String ipAddress;
	private StreamedContent imagemUsuarioLogado;
	private UsuarioDao usuarioDao;
	private FacesMessages mensagem;
	private static Log log = LogFactory.getLog(LoginController.class);

	private GenericDao<Empresa> empresaDao;

	@PostConstruct
	public void init(){
		try {
			migrar();
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		//migrarPostgres();
		usuarioDao = new UsuarioDaoImpl();
		empresaDao = new GenericDaoImpl<>(Empresa.class);
		listaDeEmpresas = empresaDao.list();
	}

	public void login() {

		try {
			if (!cpf.equals("") && !cpf.equals(null) && !password.equals("") && !password.equals(null)) {
				usuario = usuarioDao.login(cpf, password);

				Calendar calendario = Calendar.getInstance();
				calendario.setTime(usuario.getDataCriacao());

				Locale localizacao = new Locale("pt", "BR");

				usuario.setMes(calendario.getDisplayName(Calendar.MONTH, Calendar.LONG, localizacao));
				usuario.setAno(String.valueOf(calendario.get(Calendar.YEAR)));

			}

			if (usuario != null) {
				loginIn = true;
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
				// log.info("Usuario "+usuario.getNomeCompleto()+" logado no IP:
				// "+InetAddress.getLocalHost().getHostAddress());
				log.info("Usuario " + usuario.getNomeCompleto() + " logado no IP: " + getMeuIP() + "\nSessão: "
						+ getMinhaSession());
			} else {
				mensagem.alert("Usuario não encontrado!");
			}

		} catch (IOException ex) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		try {
			loginIn = false;
			usuario = null;
			usuarioDao.closeTransaction();
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		} catch (IOException ex) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public StreamedContent getImagemUsuarioLogado() {
		if (usuario.getFoto() != null) {
			if (usuario.getFoto().length != 0) {
				return new DefaultStreamedContent(new ByteArrayInputStream(usuario.getFoto()), "");
			} else {
				return new DefaultStreamedContent();
			}
		} else {
			return new DefaultStreamedContent();
		}
	}

	public String getMeuIP() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		return request.getRemoteAddr();
	}

	public String getMinhaSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		return session.getId();
	}

	public void setImagemUsuarioLogado(StreamedContent imagemUsuarioLogado) {
		this.imagemUsuarioLogado = imagemUsuarioLogado;
	}

	public void migrar() throws SQLException, NamingException {

		//TODO: pegar esse contexto
		Context context = null;

		context = new InitialContext();
		DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/sysFinanceDB");

		// Inicialição do FlyWay
		Flyway flyway = new Flyway();
		flyway.setDataSource(ds);
		flyway.setBaselineOnMigrate(true);
		flyway.setEncoding("ISO-8859-1");
		flyway.setValidateOnMigrate(true);

		// executa Migração;
		flyway.migrate();
	}
	
	public void migrarPostgres() {
		// Criação do DataSource
		PGPoolingDataSource dataSource = new PGPoolingDataSource();
		dataSource.setUser("postgres");
		dataSource.setPassword("postgres");
		dataSource.setDatabaseName("finance");
		dataSource.setInitialConnections(10);
		dataSource.setPortNumber(5432);
		dataSource.setServerName("127.0.0.1:5432");
		 
		// Inicialição do FlyWay
		Flyway flyway = new Flyway();
		flyway.setDataSource(dataSource);
		flyway.setBaselineOnMigrate(true);
		flyway.setEncoding("ISO-8859-1");
		flyway.setValidateOnMigrate(true);
		 
		// executa Migração;
		flyway.migrate();
	}

	public DataSource getDataSource(String connectURI) {
		PoolingDataSource dataSource = null;
		ObjectPool connectionPool = new GenericObjectPool(null);
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, "root", "root");
		@SuppressWarnings("unused")
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory,
				connectionPool, null, null, false, true);
		dataSource = new PoolingDataSource(connectionPool);
		return dataSource;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoginIn() {
		return loginIn;
	}

	public void setLoginIn(boolean loginIn) {
		this.loginIn = loginIn;
	}

	public int getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(int sessionCount) {
		this.sessionCount = sessionCount;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public List<Usuario> getUsuarioList() {
		return usuarioList;
	}

	public List<Empresa> getListaDeEmpresas() {
		return listaDeEmpresas;
	}

	public void setListaDeEmpresas(List<Empresa> listaDeEmpresas) {
		this.listaDeEmpresas = listaDeEmpresas;
	}

}
