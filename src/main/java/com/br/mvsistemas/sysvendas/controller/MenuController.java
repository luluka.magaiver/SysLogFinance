package com.br.mvsistemas.sysvendas.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.br.mvsistemas.sysvendas.dao.MenuDao;
import com.br.mvsistemas.sysvendas.dao.UsuarioDao;
import com.br.mvsistemas.sysvendas.daoimpl.MenuDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.UsuarioDaoImpl;
import com.br.mvsistemas.sysvendas.model.Menu;
import com.br.mvsistemas.sysvendas.model.Usuario;


@Named
@ViewScoped
public class MenuController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 942747256135164375L;
	
	private List<Menu> menuList;
	private String pageLink;
    private String pageName;
    private String searhText;
    private Usuario usuario;
    
    private MenuDao menuDao;
    private UsuarioDao usuarioDao;
   
	public MenuController() {
        pageLink = "blankPage";
        menuDao = new MenuDaoImpl();
        usuarioDao = new UsuarioDaoImpl();
    }
	
	public void setPage(String link, String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> map = context.getViewRoot().getViewMap();
        List<String> list = new ArrayList<>();

        for (String key : map.keySet()) {
            if (!key.equals("menuController")) {
                list.add(key);
            }
        }

        if (list != null && !list.isEmpty()) {
            for (String get : list) {
                map.remove(get);
            }
        }

        setPageLink(link == "" ? "/errorPage/404" : link);
        setPageName(name);
    }
	
	@SuppressWarnings("unchecked")
	public List<Menu> getMenuList() {
		this.usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		List<Usuario> user = usuarioDao.list();
		if (menuList == null) {
			menuList = new ArrayList<>();
			for (Usuario usuario : user) {
				if (this.usuario.equals(usuario)) {
					for (Menu menu : usuario.getMenus()) {
						menuList.add(menu);
					}
				}
			}
		}
		return menuList;
	}
	
	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getSearhText() {
		return searhText;
	}

	public void setSearhText(String searhText) {
		this.searhText = searhText;
	}
}
