package com.br.mvsistemas.sysvendas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.br.mvsistemas.sysvendas.dao.CfopDao;
import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dao.ProdutoDao;
import com.br.mvsistemas.sysvendas.dao.UnidadeDao;
import com.br.mvsistemas.sysvendas.daoimpl.CfopDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.ProdutoDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.UnidadeDaoImpl;
import com.br.mvsistemas.sysvendas.dto.ProdutoDTO;
import com.br.mvsistemas.sysvendas.model.Cfop;
import com.br.mvsistemas.sysvendas.model.ConfiguracaoEntradaSaida;
import com.br.mvsistemas.sysvendas.model.Fornecedor;
import com.br.mvsistemas.sysvendas.model.GrupoProduto;
import com.br.mvsistemas.sysvendas.model.Produto;
import com.br.mvsistemas.sysvendas.model.TipoProdutoEnum;
import com.br.mvsistemas.sysvendas.model.Unidade;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.service.FotoService;
import com.br.mvsistemas.sysvendas.service.ProdutoService;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.RenderizarImagem;

@Named
@ViewScoped
public class ProdutoController implements Serializable{

	private static final long serialVersionUID = 2219563634132896534L;
	
	private boolean ativo = false;
	private Produto produtoEdicao;
	private Produto produtoSelecionado;
	private ProdutoDao produtoDao;
	private UnidadeDao unidadeDao;
	private CfopDao cfopDao;
	
	@Inject
	private ProdutoService servico;
	
	@Inject
	private FacesMessages mensagem;
	
	@Inject
	private FotoService fotoService;
	
	private ProdutoDTO pesquisaProduto;
	private List<Produto> listProdutos;
	private List<Unidade> listUnidades;
	private List<Fornecedor> listFornecedor;
	
	private List<Cfop> cfopSaida;
	private List<Cfop> cfopEntrada;
	
	@PostConstruct
	public void consultar() {
		produtoDao = new ProdutoDaoImpl();
		unidadeDao = new UnidadeDaoImpl();
		cfopDao = new CfopDaoImpl();
		produtoEdicao = new Produto();
		pesquisaProduto = new ProdutoDTO();
		listUnidades = unidadeDao.list();
		
		cfopSaida = cfopDao.cfopSaida();
		cfopEntrada = cfopDao.cfopEntrada();
	}

	public boolean isAtivo() {
		return ativo;
	}
	
	public void ativar(){
		ativo = true;
		produtoEdicao = new Produto();
	
		Long numero = produtoDao.ultimoIdDoProduto();
		
		if (numero == null) {
			produtoEdicao.setCodProduto(servico.proximoCodigo((long) 1));
		} else {
			produtoEdicao.setCodProduto(servico.proximoCodigo(numero+1));
		}
		
	}
	
	public void salvar(){
		produtoEdicao.setFoto((byte[]) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("foto"));
		produtoEdicao.setUsuario((Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario"));
		
		if (produtoEdicao != null && StringUtils.isNotEmpty(produtoEdicao.getFotoProduto())) {
			try {
				fotoService.deletar(produtoEdicao.getFotoProduto());
			} catch (IOException e) {
				mensagem.alert(e.getMessage());
			}
		}
		
		try {
			fotoService.recuperarFotoTemporaria(produtoEdicao.getFotoProduto());
		} catch (IOException e) {
			mensagem.alert("Problemas ao recuperar foto temporaria!");
		}
		
		servico.salvar(produtoEdicao);
		desativar();
	}
	
	public void excluir(ActionEvent actionEvent) {
		produtoSelecionado = (Produto) actionEvent.getComponent().getAttributes().get("produtoExcluido");
		servico.deletar(produtoSelecionado);
		mensagem.alert("Produto Excluido com Sucesso!!");
	}
	
	public void editar(ActionEvent actionEvent) {
		produtoEdicao = (Produto) actionEvent.getComponent().getAttributes().get("produtoEditado");
		ativo = true;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("produtoEditado", produtoEdicao);
	}
	
	public void desativar(){
		ativo = false;
		produtoEdicao = new Produto();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("produtoEditado");
	}
	
	public void consutaDeProdutos() {
		List<Produto> produtos = servico.consultaDeProdutos(pesquisaProduto.getCodigo().toString(),
				  pesquisaProduto.getCodBarras().toString(),
				  null,
				  pesquisaProduto.getNome().toString(),
				  pesquisaProduto.getDescricao().toString(),
				  null,
				  null,
				  null);
		
		if (produtos.size() <= 0) {
			mensagem.alert("Nenhum Produto Encontrado");
			listProdutos = null;
			return;
		} else {
			listProdutos = produtos;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("listaDeProdutos", listProdutos);
		}
		
	}
	
	public void upload(FileUploadEvent fileUploadEvent){
		UploadedFile uploadedFile = fileUploadEvent.getFile();
		
		try {
			byte[] fotoRenderizada = RenderizarImagem.renderizar(uploadedFile.getContents(), 100, 150);
			String foto = fotoService.salvarFotoTemp(uploadedFile.getFileName(),fotoRenderizada);
			produtoEdicao.setFotoProduto(foto);
		} catch (Exception e) {
			mensagem.error(e.getMessage());
		}
	}
	
	public void removerFoto() {
		try {
			fotoService.deletarTemp(produtoEdicao.getFotoProduto());
		} catch (IOException e) {
			mensagem.alert(e.getMessage());
		}
		
		produtoEdicao.setFotoProduto(null);
	}
	
	public List<ConfiguracaoEntradaSaida > getListaEntrada(){
		return servico.listaConfgEntradaSaida();
	}
	
	public List<ConfiguracaoEntradaSaida > getListaSaidaDentro(){
		return servico.listaConfgEntradaSaida();
	}
	
	public List<ConfiguracaoEntradaSaida > getListaSaidaFora(){
		return servico.listaConfgEntradaSaida();
	}
	
	public List<GrupoProduto> getListaGrupoProduto(){
		return servico.listaGrupo();
	}
	
//	public BigDecimal getPrecoSugeridoVenda(){
//		return servico.precoSugeridoVenda(produtoEdicao.getPrecoCusto() == null ? BigDecimal.ZERO : produtoEdicao.getPrecoCusto(), 
//						produtoEdicao.getPorcentagemLucro() == null ? BigDecimal.ZERO : produtoEdicao.getPorcentagemLucro(), 
//						produtoEdicao.getPorcentagemDespesas() == null ? BigDecimal.ZERO : produtoEdicao.getPorcentagemDespesas());
//	}
	
	public TipoProdutoEnum[] getTipoDoProduto(){
		return TipoProdutoEnum.values();
	}
	
	public Produto getProdutoEdicao() {
		return produtoEdicao;
	}
	
	public void setProdutoEdicao(Produto produtoEdicao) {
		this.produtoEdicao = produtoEdicao;
	}
	
	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}
	
	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
	public List<Produto> getListProdutos() {
		return listProdutos;
	}
	
	public List<Unidade> getListUnidades() {
		return listUnidades;
	}
	
	public List<Fornecedor> getListFornecedor() {
		return listFornecedor;
	}

	public void setListFornecedor(List<Fornecedor> listFornecedor) {
		this.listFornecedor = listFornecedor;
	}

	public ProdutoDTO getPesquisaProduto() {
		return pesquisaProduto;
	}
	
	public void setPesquisaProduto(ProdutoDTO pesquisaProduto) {
		this.pesquisaProduto = pesquisaProduto;
	}

	public List<Cfop> getCfopSaida() {
		return cfopSaida;
	}

	public List<Cfop> getCfopEntrada() {
		return cfopEntrada;
	}

}
