package com.br.mvsistemas.sysvendas.util;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;

public class RelatorioUtil {

	public static final String SEPARADOR = File.separator;

	public static final int PDF = 1;
	public static final int WORD = 2;
	public static final int EXCEL = 3;
	public static final int POWER_POINT = 4;
	public static final int HTML = 5;
	public static final int PLANILHA_OPEN_OFFICE = 6;

	private static Map<String, Object> parametros;
	private static Collection<?> dados;
	private static String diretorioRelatorioJasper;
	private static String nomeArquivoJasper;
	private static String nomeArquivoSaida;

	private static void atualizarParametros() {
		dados = (Collection<?>) parametros.get("dados");
		diretorioRelatorioJasper = (String) parametros.get("diretorioRelatorioJasper");
		nomeArquivoJasper = (String) parametros.get("nomeArquivoJasper");
		nomeArquivoSaida = (String) parametros.get("nomeArquivoSaida");
		parametros.remove("dados");
		parametros.remove("diretorioRelatorioJasper");
		parametros.remove("nomeArquivoJasper");
		parametros.remove("nomeArquivoSaida");

	}

	/**
	 * Metodo que gera o relatorio
	 * 
	 * @param param
	 * @throws JRException
	 * @throws IOException
	 * @throws NamingException
	 * @throws SQLException
	 */
	// public static void gerarPdf(Map<String, Object> param) {
	// parametros = param;
	// atualizarParametros();
	//
	// nomeArquivoSaida = nomeArquivoSaida + "-" +
	// DateTimeFormat.forPattern("yyyy-MM-dd_HH-mm-ss").print(new DateTime());
	// try {
	// InputStream inputStream = (InputStream)
	// RelatorioUtil.class.getResourceAsStream(diretorioRelatorioJasper +
	// nomeArquivoJasper + ".jasper");
	// JasperPrint jp = JasperFillManager.fillReport(inputStream, param, new
	// JRBeanCollectionDataSource(dados));
	// JRExporter exporter = new JRPdfExporter();
	// exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	// exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
	// nomeArquivoSaida);
	//
	// exporter.exportReport();
	//
	// File pdf = new File(nomeArquivoSaida);
	// Desktop.getDesktop().open(pdf);
	// } catch (JRException | IOException e) {
	// e.getMessage();
	// e.getStackTrace();
	// }
	//
	//
	// }

	/**
	 * Metodo que gera o relatorio
	 * 
	 * @param param
	 * @throws JRException
	 * @throws IOException
	 */
	public static void gerarPdf(Map<String, Object> param) {

		try {
			parametros = param;
			atualizarParametros();

			nomeArquivoSaida = nomeArquivoSaida + "-"
					+ DateTimeFormat.forPattern("yyyy-MM-dd_HH-mm-ss").print(new DateTime());

			InputStream inputStream = RelatorioUtil.class.getClassLoader()
					.getResourceAsStream(diretorioRelatorioJasper + nomeArquivoJasper + ".jasper");
			JasperPrint print = JasperFillManager.fillReport(inputStream, parametros, new JRBeanCollectionDataSource(dados));

			@SuppressWarnings("rawtypes")
			JRExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, nomeArquivoSaida);
			
			exporter.exportReport();
			
			File pdf = new File(nomeArquivoSaida+".pdf");

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			// response.addHeader("Content-disposition", "attachment; filename=" +
			// nomeArquivoSaida + ".pdf");
			//response.addHeader("Content-disposition", "inline; filename=" + nomeArquivoSaida + ".pdf");

			ServletOutputStream servletOutputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfFile(nomeArquivoSaida+".pdf");
			
			servletOutputStream.flush();
			servletOutputStream.close();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (JRException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

}
