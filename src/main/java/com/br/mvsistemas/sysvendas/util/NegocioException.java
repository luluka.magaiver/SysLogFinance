package com.br.mvsistemas.sysvendas.util;

public class NegocioException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2972992647901769677L;

	public NegocioException(String msg) {
		super(msg);
	}
}
