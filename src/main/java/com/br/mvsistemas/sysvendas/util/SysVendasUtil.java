package com.br.mvsistemas.sysvendas.util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;

import javax.swing.text.MaskFormatter;
import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.model.Parametro;

import lombok.Getter;

public class SysVendasUtil implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6307111872110263272L;
	
	private static GenericDao<Parametro> parametroDao;
	@Getter
	private static Long NUMERO;
	@Getter
	private static Parametro dados;
	
	public static String formatString(String value, String pattern) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);
        } catch (ParseException ex) {
            return value;
        }
    }
	
	
	public static String numero(){
		parametroDao = new GenericDaoImpl<Parametro>(Parametro.class);
		
		dados = parametroDao.read(new Long(1));
		NUMERO = dados.getNumeroVenda();
		dados.setNumeroVenda(NUMERO+1);
		parametroDao.alter(dados, null, "");
		
		return StringUtils.leftPad(String.valueOf(NUMERO), 6, "0");

	}
	
	/**
	 * Verifica se a String passada não é Nula ou não é em branco.
	 * @param s
	 * @return
	 */
	public static Boolean isStringUsable(String s) {
		// lembre-se de que a comparação
        // com nulo sempre deve vir antes,
        // para evitar chamar métodos em instâncias nulas
	    return s != null && !s.isEmpty(); 
	}
	
	/**
	 * Verifica se a String é Nula ou em branco.
	 * @param s
	 * @return
	 */
	public static Boolean isNullOrEmpty(String s) {
	    return s == null || s.isEmpty();
	}
	
}
