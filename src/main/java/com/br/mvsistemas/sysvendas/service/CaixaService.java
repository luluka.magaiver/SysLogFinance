package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.CaixaDao;
import com.br.mvsistemas.sysvendas.daoimpl.CaixaDaoImpl;
import com.br.mvsistemas.sysvendas.model.Caixa;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class CaixaService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8730620566356254948L;

	@Inject
	private FacesMessages mensagem;

	private CaixaDao<Caixa> caixaDao;

	public CaixaService() {
		caixaDao = new CaixaDaoImpl();
	}

	public List<Caixa> listaCaixa() {
		return caixaDao.list();
	}

	public void salvar(Caixa caixa) {
		caixaDao.create(caixa, mensagem, " Aberto");
	}

	public void fechar(Caixa caixa) {
		caixaDao.alter(caixa, mensagem, " Fechado");
	}

	public boolean isCaixaAberto() {
		Caixa caixa = caixaDao.caixaAberto();

		if (caixa != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public Caixa caixaAberto(){
		return caixaDao.caixaAberto();
	}

}
