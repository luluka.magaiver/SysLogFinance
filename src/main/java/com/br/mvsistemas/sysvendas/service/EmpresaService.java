package com.br.mvsistemas.sysvendas.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cidades;
import com.br.mvsistemas.sysvendas.model.Empresa;
import com.br.mvsistemas.sysvendas.model.Endereco;
import com.br.mvsistemas.sysvendas.model.Telefone;
import com.br.mvsistemas.sysvendas.model.TipoEmpresaEnum;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.SysVendasUtil;

public class EmpresaService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4855059339178945046L;

	private GenericDao<Empresa> empresaDao;
	
	@Inject
	private FacesMessages mensage;
	
	
	private CidadeService cidadeService = new CidadeService();
	
	public EmpresaService() {
		empresaDao = new GenericDaoImpl<>(Empresa.class);
	}
	
	public List<Empresa> listaEmpresas(){
		return empresaDao.list();
	}
	
	public boolean salvar(Empresa empresa){
		
		if (empresa.getTelefones() == null) {
			mensage.alert("Pelo menos 1 telefone deve ser informado...");
			return false;
		} else if (empresa.getTelefones().size() <= 0) {
			mensage.alert("Pelo menos 1 telefone deve ser informado...");
			return false;
		}else {
			Empresa e = empresaDao.createAlter(empresa, mensage, "Salvo!");
			if (e.getId() != null) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public void salvaAltera(Empresa empresa){
		empresaDao.createAlter(empresa, mensage, "Salvo!");
	}
	
	public void excluir(Empresa empresa){
		empresaDao.delete(empresa, mensage, "Deletado!");
	}
	
	
	public Empresa buscaCnpj(String cnpj) throws IOException, ParseException {
		Empresa empresa = new Empresa();
		Telefone telefone = new Telefone();
		
		String jsonString = "https://www.receitaws.com.br/v1/cnpj/"+cnpj;

		URL url = new URL(jsonString);
		Reader br = new InputStreamReader(url.openStream());

		JSONParser parser = new JSONParser();
		JSONObject jsonObjeto = (JSONObject) parser.parse(br);
		
		empresa.setCnpj(jsonObjeto.get("cnpj").toString());
		empresa.setEmail(jsonObjeto.get("email").toString());
		empresa.setNomeFantasia(jsonObjeto.get("fantasia").toString());
		empresa.setRazaoSocial(jsonObjeto.get("nome").toString());
		empresa.setTipo(TipoEmpresaEnum.valueOf(jsonObjeto.get("tipo").toString()));
		
		telefone.setNumero(SysVendasUtil.formatString(jsonObjeto.get("telefone").toString().replaceAll("\\D", ""), "(##) ####-####"));
		
		if (empresa.getTelefones() == null) {
			empresa.setTelefones(new HashSet<Telefone>());
		}
		
		empresa.getTelefones().add(telefone);
		
		if (empresa.getEndereco() == null) {
			empresa.setEndereco(new Endereco());
		}
		
		empresa.getEndereco().setBairro(jsonObjeto.get("bairro").toString());
		empresa.getEndereco().setLogradouro(jsonObjeto.get("logradouro").toString());
		empresa.getEndereco().setCep(SysVendasUtil.formatString(jsonObjeto.get("cep").toString().replaceAll("\\D", ""), "#####-###"));
		empresa.getEndereco().setComplemento(jsonObjeto.get("complemento").toString());
		empresa.getEndereco().setNumero(jsonObjeto.get("numero").toString());
		
		Cidades cid = cidadeService.porLocalidade(jsonObjeto.get("municipio").toString(), jsonObjeto.get("uf").toString());
	
		empresa.getEndereco().setCidade(cid);
		
		return empresa;
	}
	
	public List<Empresa> consultaDeEmpresa(String nomeFantasia, String razaoSocial, String cnpj, String ie){
		String sql = "From Empresa e where 1=1";
		
		if (!StringUtils.isEmpty(nomeFantasia)) {
			sql += " and e.nomeFantasia like '%"+nomeFantasia+"%'";
		}
		
		if (!StringUtils.isEmpty(razaoSocial)) {
			sql += " and e.razaoSocial like '%"+razaoSocial+"%'";
		}
		
		if (!StringUtils.isEmpty(cnpj)) {
			sql += " and e.cnpj = '"+cnpj+"'";
		}
		
		if (!StringUtils.isEmpty(ie)) {
			sql += " and e.ie = '"+ie+"'";
		}
		
		return empresaDao.listScript(sql);
	}
}
