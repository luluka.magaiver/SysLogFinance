package com.br.mvsistemas.sysvendas.service;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.naming.NamingException;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.ClienteDao;
import com.br.mvsistemas.sysvendas.daoimpl.ClienteDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.relatorios.Aniversariantes;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.RelatorioUtil;

import net.sf.jasperreports.engine.JRException;

public class ClienteService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -986424175930785878L;

	private ClienteDao<Cliente> clienteDao;

	@Inject
	private FacesMessages mensage;

	public ClienteService() {
		clienteDao = new ClienteDaoImpl();
	}

	public boolean salvar(Cliente cliente) {
//		if (cliente.getEnderecos() == null) {
//			mensage.alert("Pelo menos 1 Endereço deve ser informado...");
//			return false;
//		} else if (cliente.getTelefones() == null) {
//			mensage.alert("Pelo menos 1 Telefone deve ser informado...");
//			return false;
//		} else {
			Cliente c = clienteDao.createAlter(cliente, mensage, "Salvo!");

			if (c.getId() != null) {
				return true;
			} else {
				return false;
			}
//		}
	}

	public void deletar(Cliente cliente) {
		clienteDao.delete(cliente, mensage, "Deletado!");
	}

	public Cliente cliente(Cliente cliente) {
		return (Cliente) clienteDao.read(cliente.getId());
	}

	public List<Cliente> consultaDeClientes(String nome, String empresa, String setor) {
		String sql = "From Cliente c where 1=1";

		if (!StringUtils.isEmpty(nome)) {
			sql += " and c.nome like '%" + nome + "%'";
		}
		
		if (!StringUtils.isEmpty(empresa)) {
			sql += " and c.empresaCliente like '%" + empresa + "%'";
		}
		
		if (!StringUtils.isEmpty(setor)) {
			sql += " and c.empresaSetor like '%" + setor + "%'";
		}

		return clienteDao.listScript(sql);
	}

	public List<Cliente> todosClientes(String query, List<Cliente> listaClientes, FacesMessages mensage) {

		listaClientes = clienteDao.clientes();
		List<Cliente> sugestoes = new ArrayList<Cliente>();
		for (Cliente c : listaClientes) {
			if (c.getNome().startsWith(query.toUpperCase())) {
				sugestoes.add(c);
			}
		}
		if (sugestoes.size() == 0) {
			mensage.alert("Cliente não encontrado!");
		}
		return sugestoes;
	}
	
	
	public List<Cliente> todosFornecedores(String query, List<Cliente> listaFornecedores, FacesMessages mensage) {

		listaFornecedores = clienteDao.fornecedores();
		List<Cliente> sugestoes = new ArrayList<Cliente>();
		for (Cliente c : listaFornecedores) {
			if (c.getNome().startsWith(query.toUpperCase())) {
				sugestoes.add(c);
			}
		}
		if (sugestoes.size() == 0) {
			mensage.alert("Fornecedor não encontrado!");
		}
		return sugestoes;
	}

	public void relatorioAniversariante(Integer mes) throws IOException{
		List<Cliente> aniversarinates = clienteDao.aniversariantes(mes);
		
		if (aniversarinates.size() > 0 && aniversarinates != null) {
			
			List<Aniversariantes> listaAniversario = new ArrayList<>();

			for (Cliente cliente : aniversarinates) {
				Aniversariantes aniversariantes = new Aniversariantes();
				aniversariantes.setId(cliente.getId());
				aniversariantes.setNome(cliente.getNome());
				aniversariantes.setEmail(cliente.getEmail());
				aniversariantes.setDataNascimento(cliente.getDataNascimento());
				aniversariantes.setEmpresaCliente(cliente.getEmpresaCliente());
				aniversariantes.setEmpresaSetor(cliente.getEmpresaSetor());

				listaAniversario.add(aniversariantes);
			}

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();
		
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("dados", listaAniversario);
			param.put("diretorioRelatorioJasper", "/WEB-INF/relatorios/");
			param.put("nomeArquivoJasper", "Aniversariantes");
			param.put("nomeArquivoSaida", "Aniversariantes-" + mes);
			param.put("link_empresa", "http://mvsistema.com");
			param.put("mes", tradcao(mes));

			
			//pegando o caminho da pasta onde esta as imagens
			String pathImage = scontext.getRealPath("") 
					+ File.separator + "resources" 
					+ File.separator 
					+ File.separator + "images" 
					+ File.separator;
			//
			param.put("logo", pathImage);
			
			RelatorioUtil.gerarPdf(param);
		} else {
			mensage.alert("Nehum aniversariante no mês informado!");
		}
		
	}

	private String tradcao(int mes) {
		Calendar cal = GregorianCalendar.getInstance();
		 
		
		String mesPorExtenso = "";
		switch (mes) {
		case 1:
			mesPorExtenso = "Janeiro "+cal.get(Calendar.YEAR);
			break;
		case 2:
			mesPorExtenso = "Fevereiro "+cal.get(Calendar.YEAR);
			break;
		case 3:
			mesPorExtenso = "Março "+cal.get(Calendar.YEAR);
			break;
		case 4:
			mesPorExtenso = "Abril "+cal.get(Calendar.YEAR);
			break;
		case 5:
			mesPorExtenso = "Maio "+cal.get(Calendar.YEAR);
			break;
		case 6:
			mesPorExtenso = "Junho "+cal.get(Calendar.YEAR);
			break;
		case 7:
			mesPorExtenso = "Julho "+cal.get(Calendar.YEAR);
			break;
		case 8:
			mesPorExtenso = "Agosto "+cal.get(Calendar.YEAR);
			break;
		case 9:
			mesPorExtenso = "Setembro "+cal.get(Calendar.YEAR);
			break;
		case 10:
			mesPorExtenso = "Outubro "+cal.get(Calendar.YEAR);
			break;
		case 11:
			mesPorExtenso = "Novembro "+cal.get(Calendar.YEAR);
			break;
		case 12:
			mesPorExtenso = "Dezembro "+cal.get(Calendar.YEAR);
			break;

		default:
			break;
		}

		return mesPorExtenso;
	}
}
