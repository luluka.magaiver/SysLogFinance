package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.List;

import com.br.mvsistemas.sysvendas.dao.PlanoContasDao;
import com.br.mvsistemas.sysvendas.daoimpl.PlanoContasDaoImpl;
import com.br.mvsistemas.sysvendas.model.PlanoContas;

public class PlanoContasService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7997961774022661469L;

	
	private PlanoContasDao<PlanoContas> planoContasDao;
	
	public PlanoContasService() {
		planoContasDao = new PlanoContasDaoImpl();
	}
	
	public List<PlanoContas> listarPlanoConta(){
		return planoContasDao.listScript("From PlanoContas where planoContasPai is null");
	}
	
	public List<PlanoContas> todos(){
		return planoContasDao.list();
	}
	
	public PlanoContas planoContas(String plano){
		return planoContasDao.planoContas(plano);
	}
}
