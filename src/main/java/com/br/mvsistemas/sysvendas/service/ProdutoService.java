package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dao.ProdutoDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.ProdutoDaoImpl;
import com.br.mvsistemas.sysvendas.model.ConfiguracaoEntradaSaida;
import com.br.mvsistemas.sysvendas.model.Estoque;
import com.br.mvsistemas.sysvendas.model.GrupoProduto;
import com.br.mvsistemas.sysvendas.model.Produto;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class ProdutoService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6842714972105987521L;

	private ProdutoDao<Produto> produtoDao;
	private GenericDao<ConfiguracaoEntradaSaida> entradaSaidaDao;
	private GenericDao<GrupoProduto> grupoProdutoDao;
	@Inject
	private FacesMessages mensagem;

	public ProdutoService() {
		produtoDao = new ProdutoDaoImpl();
		entradaSaidaDao = new GenericDaoImpl<>(ConfiguracaoEntradaSaida.class);
		grupoProdutoDao = new GenericDaoImpl<>(GrupoProduto.class);
	}

	/**
	 * Gera um codigo interno para o Produto a partir de um Numero informado
	 * 
	 * @param numero
	 * @return
	 */
	public static String proximoCodigo(Long numero) {
		NumberFormat f = new DecimalFormat("00000");
		return String.valueOf(f.format(numero));
	}

	public void salvar(Produto produto) {
		if (produto.getId() == null) {
			produtoDao.createAlter(produto, mensagem, " Cadastrado!");
		} else {
			produtoDao.createAlter(produto, mensagem, " Alterado!");
		}

	}

//	public boolean salvar(Produto produto) {
//		Produto p = produtoDao.createAlter(produto, mensagem, " Salvo!");
//		if (p.getId() != null) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	public List<Produto> produtosNoEstoque() {
		return (List<Produto>) produtoDao.produtoNoEstoque();
	}
	
	public List<Produto> listaProdutos(){
		return produtoDao.produtos();
	}
	
	public List<Estoque> estoqueProduto(){
		return (List<Estoque>) produtoDao.produtoEstoque();
	}
	
	public Estoque estoque(Produto produto){
		return produtoDao.estoque(produto);
	}

	public void deletar(Produto produto) {
		// produtoDao.delete(produto);
	}

	public List<ConfiguracaoEntradaSaida> listaConfgEntradaSaida() {
		return entradaSaidaDao.list();
	}

	public List<GrupoProduto> listaGrupo() {
		return grupoProdutoDao.list();
	}

	/**
	 * Consuta de Produtos por paramentros
	 * 
	 * @param strings
	 *            0 = codProduto 1 = codBarras 2 = codFornecedor 3 = nomeProduto
	 *            4 = descricaoProduto 5 = tipoProduto 6 = precoProduto 7 =
	 *            fabricante
	 * @return
	 */
	public List<Produto> consultaDeProdutos(String... strings) {
		String sql = "From Produto p where 1=1";

		if (!StringUtils.isEmpty(strings[0])) {
			sql += " and p.codProduto like '" + strings[0] + "%'";
		}

		if (!StringUtils.isEmpty(strings[1])) {
			sql += " and p.codBarras like '" + strings[1] + "%'";
		}

		if (!StringUtils.isEmpty(strings[2])) {
			sql += " and p.codFornecedor like '" + strings[2] + "%'";
		}

		if (!StringUtils.isEmpty(strings[3])) {
			sql += " and p.nomeProduto like '%" + strings[3] + "%'";
		}

		if (!StringUtils.isEmpty(strings[4])) {
			sql += " and p.descricaoProduto like '%" + strings[4] + "%'";
		}

		if (!StringUtils.isEmpty(strings[5])) {
			sql += " and p.tipoProduto = '" + strings[5] + "'";
		}

		if (!StringUtils.isEmpty(strings[6])) {
			sql += " and p.precoUnitario = '" + strings[6] + "'";
		}

		if (!StringUtils.isEmpty(strings[7])) {
			sql += " and p.fabricante like '" + strings[7] + "%'";
		}

		return produtoDao.listScript(sql);

	}

	public Produto produto(Produto produto) {
		return (Produto) produtoDao.read(produto.getId());
	}

	public BigDecimal precoSugeridoVenda(BigDecimal pVenda, BigDecimal lucro, BigDecimal despesas) {
		BigDecimal cem = new BigDecimal(100.00);
		BigDecimal precoVenda = pVenda.divide(cem.subtract(lucro).subtract(despesas), 4, RoundingMode.HALF_UP)
				.multiply(cem);
		return precoVenda;
	}
}
