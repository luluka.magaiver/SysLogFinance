package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class UsuarioService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3572646061991226310L;
	
	private GenericDao<Usuario> usuarioDao;
	
	@Inject
	private FacesMessages mensage;
	
	public UsuarioService() {
		usuarioDao = new GenericDaoImpl<>(Usuario.class);
	}
	
	public boolean salvar(Usuario usuario){
		Usuario u = usuarioDao.createAlter(usuario, mensage, "Salvo!");
		if (u.getId() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public void deletar(Usuario usuario){
		usuarioDao.delete(usuario, mensage, "Deletado");
	}
	
	public Usuario usuario(Long l){
		return (Usuario) usuarioDao.read(l);
	}
	
	public List<Usuario> consultaDeUsuarios(String nomeCompleto){
		String sql = "From Usuario u where 1=1";
		
		if (!StringUtils.isEmpty(nomeCompleto)) {
			sql += " and u.nomeCompleto like '%"+nomeCompleto+"%'";
		}
		
		sql += "and u.cpf <> '000.000.000-00'";
		
		return usuarioDao.listScript(sql);
	}

	public List<Usuario> listaDeUsuarios(){
		return usuarioDao.list();
	}
	
	public List<Usuario> listaDeUsuariosDash(){
		return usuarioDao.listScript("from Usuario u Where u.cpf <> '000.000.000-00'");
	}
}
