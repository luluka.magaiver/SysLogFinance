package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.loader.custom.Return;
import org.primefaces.component.checkbox.Checkbox;
import org.primefaces.component.selectoneradio.SelectOneRadio;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dao.LancamentoDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.daoimpl.LancamentoDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.model.Usuario;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class LancamentoService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1309940914441763306L;

	private LancamentoDao<Lancamento> lancamentoDao;
	private GenericDao<CondicaoPagamento> condicaoPagamentoDao;
	private static Integer PARCELA = 0;

	@Inject
	private FacesMessages mensage;

	public LancamentoService() {
		lancamentoDao = new LancamentoDaoImpl();
		condicaoPagamentoDao = new GenericDaoImpl<>(CondicaoPagamento.class);
	}

	public boolean salvar(Lancamento lancamento, String msg) {
		Lancamento l = lancamentoDao.createAlter(lancamento, mensage, msg);
//		Lancamento l = lancamentoDao.create(lancamento, mensage, msg);
		if (l.getId() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public Lancamento conultarLancamentoPorMovimento(Long id){
		return lancamentoDao.buscaLancamento(id);
	}
	
	public List<Lancamento> consultarLancamentosPorMovimentos(Long id){
		return lancamentoDao.buscarLancamentos(id);
	}
	
	public void lancar(Lancamento lancamento,String opcao){
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		
		String parcela[] = lancamento.getCondicaoPagamento().getFormulaCondicao().split("/");
		PARCELA = parcela.length;
//		lancamento.setDataOperacao(DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
		lancamento.setDataOperacao(lancamento.getMovimento() != null ? lancamento.getMovimento().getDataMovimento() : new Date());
				
		BigDecimal valorParcela = new BigDecimal(valorParcelas(parcela.length, lancamento));
					 
		if (lancamento.getCondicaoPagamento().getFormulaCondicao() != null) {
			for (int i = 1; i <= PARCELA; i++) {
				StringBuilder obs = new StringBuilder();
				StringBuilder doc = new StringBuilder();
				
				Calendar c = Calendar.getInstance();
				 c.setTime(lancamento.getDataOperacao());
				 
				Integer numeroParcela = Integer.parseInt(parcela[i-1]);
				c.add(Calendar.DATE, + numeroParcela);
				
				lancamento.setParcela(i);
				if (lancamento.getDocumento().indexOf(".") != -1) {
					String vetor[] = lancamento.getDocumento().replace(".", ":").split(":");
					lancamento.setDocumento(doc.append(vetor[0]).append("."+i).toString());
				} else {
					lancamento.setDocumento(doc.append(lancamento.getDocumento()).append("."+i).toString());
				}
				
				if (lancamento.getDataBaixa() != null) {
					lancamento.setObservacao(obs.append(lancamento.getDescricao()).append(" - A Vista").toString());
				} else {
					lancamento.setObservacao(obs.append(lancamento.getDescricao()).append(" - Parcela "+i+"/"+PARCELA).toString());
				}
				
				if (opcao.equals("2")) {
					lancamento.setValor(valorParcela);
				}
				
//				if (lancamento.getTipoLancamento().equals("P")) {
//					lancamento.setValor(lancamento.getValor().negate());
//				}
				
				lancamento.setDataVencimento(c.getTime());
				lancamento.setEmpresa(usuario.getEmpresa());
				lancamento.setUsuario(usuario);
				salvar(lancamento, i == PARCELA ? " Salvo!" : null);
			}
			
		} else{
			mensage.alert("Parcelas e Intervalos tem que esta preenchidos...");
			return;
		}
	}
	
	private double roundToDecimals(double d, int c) {
        int temp=(int)((d*Math.pow(10,c)));
        return (((double)temp)/Math.pow(10,c));
    }
	
	public double valorParcelas(Integer qtdParcelas, Lancamento lancamento){
        double d = lancamento.getValor().doubleValue()/qtdParcelas;
        return roundToDecimals(d, 3);
	}

	public void deletar(Lancamento lancamento) {
		lancamentoDao.delete(lancamento, mensage, "Deletado !");
	}

	public Lancamento lancamento(Lancamento lancamento) {
		return (Lancamento) lancamentoDao.read(lancamento.getId());
	}

	public List<Lancamento> listaLancamento() {
		return lancamentoDao.list();
	}

	public List<Lancamento> lancamentosPorCliente(Cliente cliente) {
		return lancamentoDao.listScript("From Lancamento l where l.cliente.id = " + cliente.getId());
	}

	public List<Lancamento> consultaDeLancamentos(String nome, String documento, Date inicial, Date datefinal,
			String opcao, String tipoLancamento) {
		return lancamentoDao.consultaDeLancamentos(nome, documento, inicial, datefinal, opcao, tipoLancamento);
	}

	public List<CondicaoPagamento> listaCondicaoPagamento() {
		return condicaoPagamentoDao.list();
	}
	
	public boolean isLancamentoNaoBaixado(Movimento movimento){
		if (lancamentoDao.isLancamentoNaoBaixado(movimento)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Integer getPARCELA() {
		return PARCELA;
	}

	public static void setPARCELA(Integer pARCELA) {
		PARCELA = pARCELA;
	}
}
