package com.br.mvsistemas.sysvendas.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.EnderecoDao;
import com.br.mvsistemas.sysvendas.daoimpl.EnderecoDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cidades;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Empresa;
import com.br.mvsistemas.sysvendas.model.Endereco;
import com.br.mvsistemas.sysvendas.model.Telefone;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.github.gilbertotorrezan.viacep.se.ViaCEPClient;
import com.github.gilbertotorrezan.viacep.shared.ViaCEPEndereco;

public class EnderecoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5447960899478389635L;
	
	private EnderecoDao enderecoDao;
	private Endereco enderecoSelecionado;
	private ViaCEPClient cepClient = new ViaCEPClient();
    private ViaCEPEndereco cepEndereco = new ViaCEPEndereco();

    private CidadeService cidadeService = new CidadeService();

    private FacesMessages mensage = new FacesMessages();
	
	public void preencherEndereco(Cliente cliente, Empresa empresa, Endereco endereco) {
		enderecoDao = new EnderecoDaoImpl();
		
		if (cliente != null) {
			String script = "FROM Endereco e WHERE e.cep = '" + endereco.getCep() + "'";

			List<Endereco> cep = enderecoDao.listScript(script);

			if (cep.size() > 0) {
				for (Endereco end : cep) {
					if (end.getCep().equals(endereco.getCep())) {
						endereco.setBairro(end.getBairro());
						endereco.setCidade(end.getCidade());
						endereco.setComplemento(end.getComplemento());
						endereco.setLogradouro(end.getLogradouro());
					}
				}
			} else {
				try {
					cepEndereco = cepClient.getEndereco(endereco.getCep());
					if (cepEndereco == null) {
						mensage.alert("Cep Nao Encontrado..");
					} else {
						
						Cidades cid = cidadeService.porLocalidade(cepEndereco.getLocalidade(), cepEndereco.getUf());

						endereco.setBairro(cepEndereco.getBairro());
						endereco.setCidade(cid);
						endereco.setComplemento(cepEndereco.getComplemento());
						endereco.setLogradouro(cepEndereco.getLogradouro());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (empresa != null) {
			String script = "FROM Endereco e WHERE e.cep = '" + empresa.getEndereco().getCep() + "'";

			List<Endereco> cep = enderecoDao.listScript(script);

			if (cep.size() > 0) {
				for (Endereco end : cep) {
					if (end.getCep().equals(empresa.getEndereco().getCep())) {
						empresa.getEndereco().setBairro(end.getBairro());
						empresa.getEndereco().setCidade(end.getCidade());
						empresa.getEndereco().setComplemento(end.getComplemento());
						empresa.getEndereco().setLogradouro(end.getLogradouro());
					}
				}
			} else {
				try {
					cepEndereco = cepClient.getEndereco(empresa.getEndereco().getCep());
					if (cepEndereco == null) {
						mensage.alert("Cep Nao Encontrado..");
					} else {
						
						Cidades cid = cidadeService.porLocalidade(cepEndereco.getLocalidade(), cepEndereco.getUf());

						empresa.getEndereco().setBairro(cepEndereco.getBairro());
						empresa.getEndereco().setCidade(cid);
						empresa.getEndereco().setComplemento(cepEndereco.getComplemento());
						empresa.getEndereco().setLogradouro(cepEndereco.getLogradouro());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void adicionarTelefone(Cliente cliente, Empresa empresa, Telefone telefone){
        if (cliente != null) {
        	if(cliente.getTelefones() == null){
                cliente.setTelefones(new HashSet<>());

            }else if(telefone != null){
            	
                cliente.getTelefones().remove(telefone);

            }
            cliente.getTelefones().add(telefone);
		}
		
        if (empresa != null) {
        	if(empresa.getTelefones() == null){
                empresa.setTelefones(new HashSet<Telefone>());

            }else if(telefone != null){
            	
            	empresa.getTelefones().remove(telefone);

            }
        	empresa.getTelefones().add(telefone);
		}
        
        telefone = null;
        
    }

    public void removerTelefone(Cliente cliente, Empresa empresa, Telefone telefoneSelecionado){
    	if (cliente != null) {
    		 cliente.getTelefones().remove(telefoneSelecionado);
		}
    	
    	if (empresa != null) {
    		 empresa.getTelefones().remove(telefoneSelecionado);
		}
        telefoneSelecionado = null;
    }
    
    public void adicionarEndereco(Cliente cliente, Endereco endereco){
    	
    	
    	if(cliente.getEnderecos() == null){
            cliente.setEnderecos(new ArrayList<Endereco>());

        }else if(enderecoSelecionado != null){
        	
            cliente.getEnderecos().remove(enderecoSelecionado);

        }
    
		cliente.getEnderecos().add(endereco);
    	
        enderecoSelecionado = null;
    	
    }
    
    public void removerEndereco(Cliente cliente, Endereco enderecoSelecionado, List<String> list){
    	list.add(enderecoSelecionado.getTipo());
		for (String endList : list) {
			for (int i = 0; i < cliente.getEnderecos().size(); i++) {
				if (cliente.getEnderecos().get(i).getTipo().equals(endList)) {
					cliente.getEnderecos().remove(i);
					enderecoSelecionado = null;
				}
			}
		}
			
    }
    
    public Endereco getEnderecoSelecionado() {
		return enderecoSelecionado;
	}
    
    public void setEnderecoSelecionado(Endereco enderecoSelecionado) {
		this.enderecoSelecionado = enderecoSelecionado;
	}
}
