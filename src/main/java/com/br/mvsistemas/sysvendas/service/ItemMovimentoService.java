package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.ProdutoDao;
import com.br.mvsistemas.sysvendas.daoimpl.ProdutoDaoImpl;
import com.br.mvsistemas.sysvendas.model.ItemMovimento;
import com.br.mvsistemas.sysvendas.model.Produto;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import lombok.Getter;

public class ItemMovimentoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3391492433859965400L;
	
	
	@Inject
	private FacesMessages mensagem;
	
	@Getter
	private ItemMovimento itemMovimento;
	
	private ProdutoDao<Produto> produtoDao;
	
	public ItemMovimentoService() {
		produtoDao = new ProdutoDaoImpl();
	}
	
	public void baixarEstoqueItemProduto(ItemMovimento itemMovimento){
		Produto produto = produtoDao.read(itemMovimento.getItemMovimento().getId());
		produto.setQuantidade(produto.getQuantidade().subtract(itemMovimento.getQuantidadeItemMovimento()));
		produtoDao.alter(produto, mensagem, "");
	}
	
	public void baixarEstoqueProduto(Produto produto, BigDecimal quantidade){
		produto.setQuantidade(produto.getQuantidade().subtract(quantidade));
		produtoDao.alter(produto, mensagem, "");
	}
	
	public void saldoEstoqueProduto(Produto produto, BigDecimal quantidade){
		produto.setQuantidade(produto.getQuantidade().add(quantidade));
		produtoDao.alter(produto, mensagem, "");
	}

}
