package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class CondicaoPagamentoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1447555665799515843L;
	
	@Inject
	private FacesMessages mensagem;
	
	private GenericDao<CondicaoPagamento> condicaoPagamentoDao;
	
	public CondicaoPagamentoService() {
		condicaoPagamentoDao = new GenericDaoImpl<>(CondicaoPagamento.class);
	}
	
	public void salvar(CondicaoPagamento condicaoPagamento){
		condicaoPagamentoDao.createAlter(condicaoPagamento, mensagem, " Salvo!");
	}
	
	public void deletar(CondicaoPagamento condicaoPagamento){
		condicaoPagamentoDao.delete(condicaoPagamento, mensagem, " Deletado!");
	}
	
	public List<CondicaoPagamento> listaCondicaoPagamento(){
		return condicaoPagamentoDao.list();
	}
	
	public CondicaoPagamento condicaoPagamento(Long id){
		return condicaoPagamentoDao.read(id);
	}

}
