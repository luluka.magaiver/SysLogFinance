package com.br.mvsistemas.sysvendas.service;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.dao.MovimentoDao;
import com.br.mvsistemas.sysvendas.daoimpl.MovimentoDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.ItemMovimento;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;
import com.br.mvsistemas.sysvendas.model.TipoStatusMovimento;
import com.br.mvsistemas.sysvendas.relatorios.CompasDeClientes;
import com.br.mvsistemas.sysvendas.relatorios.ImpressaoPedidoDevolucao;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.RelatorioUtil;

import net.sf.jasperreports.engine.JRException;

public class MovimentoService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6889625965079457739L;

	private MovimentoDao<Movimento> movimentoDao;
	private GenericDao<Lancamento> lancamentoDao;

	@Inject
	private LancamentoService lancamentoService;

	@Inject
	private MovimentoCaixaService movCaixaService;

	@Inject
	private CaixaService caixaService;

	@Inject
	private ItemMovimentoService itemMovimentoService;

	@Inject
	private FacesMessages mensagem;

	public MovimentoService() {
		movimentoDao = new MovimentoDaoImpl();

	}

	public void alterarIdMovimentoOrigem(Movimento movimento) {
		movimentoDao.alter(movimento, mensagem, "");
	}

	public void Salvar(Movimento movimento) {
		if (movimento.getItensMovimento() == null) {
			mensagem.alert("Impossivel finalizar movimento sem itens!");
			return;
		}

		try {
			
			if (movimento.getStatus().equals(TipoStatusMovimento.C)) {
				movimentoDao.createAlter(movimento, mensagem, "");
			} else {
				movimentoDao.create(movimento, mensagem, " Finalizado!");
			}
			
			if (movimento.getCondicaoPagamento().getNomeCondicao().equalsIgnoreCase("A Vista")) {

				MovimentoCaixa movCaixa = new MovimentoCaixa();

				movCaixa.setCaixa(caixaService.caixaAberto());
				movCaixa.setHora(new Date());
				movCaixa.setUsuario(movimento.getUsuarioMovimento());
				

				if (movimento.getStatus() == TipoStatusMovimento.V) {
					movCaixa.setEntradaSaida("E");
				} else if (movimento.getStatus() == TipoStatusMovimento.D) {
					movCaixa.setEntradaSaida("S");
				} else if(movimento.getStatus() == TipoStatusMovimento.C) {
					movCaixa.setEntradaSaida("S");
				}
				
				movCaixa.setValorMovimentoCaixa(movimento.getStatus() == TipoStatusMovimento.C ? 
											   movimento.getTotalMovimento().negate() : 
											   movimento.getTotalMovimento());

				movCaixa.setObservacao("Referente a " + statusMovimento(movimento.getStatus()) + " de Nº: "
						+ movimento.getNumeroMovimento());

				movCaixaService.registrarMovimentoCaixa(movCaixa);
			}

			switch (movimento.getStatus().ordinal()) {
			case 0:
				for (ItemMovimento item : movimento.getItensMovimento()) {
					itemMovimentoService.baixarEstoqueItemProduto(item);
				}
				break;
			case 1:
				for (ItemMovimento item : movimento.getItensMovimento()) {
					itemMovimentoService.saldoEstoqueProduto(item.getItemMovimento(),
							item.getQuantidadeItemMovimento());
				}
			default:
				break;
			}

			if (movimento.getStatus() != TipoStatusMovimento.C) {
				Lancamento lancamento = new Lancamento();

				lancamento.setCliente(movimento.getClienteMovimento());
				lancamento.setCondicaoPagamento(movimento.getCondicaoPagamento());
				lancamento.setDataOperacao(new Date());
				lancamento.setDataVencimento(new Date());
				lancamento.setDataBaixa(
						lancamento.getCondicaoPagamento().getNomeCondicao().equalsIgnoreCase("A Vista") ? new Date()
								: null);
				if (movimento.getStatus() == TipoStatusMovimento.V) {
					lancamento.setObservacao("Venda");
					lancamento.setDescricao("Venda");
					lancamento.setTipoLancamento("R");
				} else if (movimento.getStatus() == TipoStatusMovimento.D) {
					lancamento.setObservacao("Devolução");
					lancamento.setDescricao("Devolução");
					lancamento.setTipoLancamento("P");
				}

				lancamento.setValor(movimento.getTotalMovimento());
				lancamento.setDocumento(movimento.getNumeroMovimento());
				lancamento.setMovimento(movimento);

				lancamentoService.lancar(lancamento,
						lancamento.getCondicaoPagamento().getNomeCondicao().equalsIgnoreCase("A Vista") ? "1" : "2");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deletar(Movimento movimento) {
		movimentoDao.delete(movimento, mensagem, "Deletado!");
	}

	public List<Movimento> listaDeMovimentos(String nome, String numeroVenda, Date inicial, Date datefinal) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sql = "From Movimento m where 1=1";

		Calendar dataAtual = Calendar.getInstance();

		if (datefinal != null) {
			dataAtual.setTime(datefinal);
			dataAtual.add(Calendar.DAY_OF_MONTH, +1);
		}

		if (!StringUtils.isEmpty(nome)) {
			sql += " and m.clienteMovimento.nome like '%" + nome + "%'";
		}

		if (!StringUtils.isEmpty(numeroVenda)) {
			sql += " and m.numeroMovimento like '%" + numeroVenda + "%'";
		}

		if (inicial != null && datefinal != null) {
			sql += " and m.dataMovimento between '" + sdf.format(inicial) + "' and '" + sdf.format(dataAtual.getTime())
					+ "'";
		}

		if (inicial != null && datefinal == null) {
			sql += " and m.dataMovimento >= '" + sdf.format(inicial) + "'";
		}

		if (inicial == null && datefinal != null) {
			sql += " and m.dataMovimento <= '" + sdf.format(dataAtual.getTime()) + "'";
		}

		System.out.println(sql);
		return movimentoDao.listScript(sql);
	}

	public boolean isCanecelarMovimento(Movimento movimento) {
		if (lancamentoService.isLancamentoNaoBaixado(movimento) && (movimento.getIdMovimentoOrgem() == null)) {
			return true;
		} else {
			return false;
		}
	}

	public void relatorioCompasDeCliente(Cliente cliente) throws IOException {
		List<Object[]> compras = movimentoDao.comprasDeCliente(cliente);

		if (compras != null) {
			if (compras.size() > 0) {
				FacesContext fc = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
				session.setAttribute("compras", compras);
				session.setAttribute("cliente", cliente);
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("RelatorioComprasDeClientes");
			} else {
				mensagem.alert("Sem dados para exibir!!");
			}
		} else {
			mensagem.alert("Informar Cliente!!");
		}
	}

	public void impressaoVendaDevolucao(Movimento movimento) throws IOException {
		
		List<Object[]> impressao = movimentoDao.impressaoPedidoDevolucao(movimento);

		if (impressao != null) {
			if (impressao.size() > 0) {
				FacesContext fc = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
				session.setAttribute("movimento", impressao);
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("RelatorioVendaDevolucao");
			} else {
				mensagem.alert("Sem dados para exibir!!");
			}
		} else {
			mensagem.alert("Informar Venda!!");
		}
	}

	private String statusMovimento(TipoStatusMovimento statusMovimento) {
		return statusMovimento.getDescricao();
	}

}
