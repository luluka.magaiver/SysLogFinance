package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.MovimentoCaixaDao;
import com.br.mvsistemas.sysvendas.daoimpl.MovimentoCaixaDaoImpl;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class MovimentoCaixaService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1994750140575561509L;
	
	private MovimentoCaixaDao<MovimentoCaixa> movCaixaDao;
	
	@Inject
	private FacesMessages mensagem;
	
	public MovimentoCaixaService() {
		movCaixaDao = new MovimentoCaixaDaoImpl();
	}
	
	public void registrarMovimentoCaixa(MovimentoCaixa movimentoCaixa) {
		movCaixaDao.createAlter(movimentoCaixa, mensagem, "");
	}
	
	public List<MovimentoCaixa> listaMovimentoCaixa(){
		return movCaixaDao.list();
	}
	
	public List<MovimentoCaixa> consultaDeMovimentoCaixa(Date dataInicial, Date dataFinal){
		return movCaixaDao.consultaDeMovimentoCaixa(dataInicial, dataFinal);
	}
	
}
