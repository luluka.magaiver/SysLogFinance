package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.daoimpl.GenericDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cfop;
import com.br.mvsistemas.sysvendas.model.ConfiguracaoEntradaSaida;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class ConfiguracaoEntradaSaidaService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2022902919920025637L;
	
	private GenericDao<ConfiguracaoEntradaSaida> dao;
	
	private GenericDao<Cfop> cfopDao;
	
	@Inject
	private FacesMessages mensage;
	
	public ConfiguracaoEntradaSaidaService() {
		dao = new GenericDaoImpl<>(ConfiguracaoEntradaSaida.class);
		cfopDao = new GenericDaoImpl<>(Cfop.class);
	}
	
	public void salvar(ConfiguracaoEntradaSaida entradaSaida){
		dao.createAlter(entradaSaida, mensage, " Salvo!");
	}
	
	public void deletar(ConfiguracaoEntradaSaida entradaSaida){
		dao.delete(entradaSaida, mensage, " Deletado!");
	}
	
	public ConfiguracaoEntradaSaida configuracaoEntradaSaida(ConfiguracaoEntradaSaida entradaSaida){
		return dao.read(entradaSaida.getId());
	}
	
	public List<Cfop> cfops(){
		return cfopDao.list();
	}

}
