package com.br.mvsistemas.sysvendas.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.br.mvsistemas.sysvendas.dao.CidadeDao;
import com.br.mvsistemas.sysvendas.daoimpl.CidadeDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cidades;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

public class CidadeService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7580951088837073446L;
	
	private CidadeDao cidadeDao;
	
	public CidadeService() {
		cidadeDao = new CidadeDaoImpl();
	}
	
	public List<Cidades> listCidades(){
		return cidadeDao.list();
	}
	
	public Cidades porLocalidade(String localidade, String uf){
		return (Cidades) cidadeDao.cidadePorLocalidade(localidade, uf);
	}
	
	public List<Cidades> todasCidades(String query, List<Cidades> listaCidades, FacesMessages mensage) {

		String letraMauscula = query.toUpperCase();
		
		listaCidades = listCidades();
		List<Cidades> sugestoes = new ArrayList<Cidades>();
		for (Cidades c : listaCidades) {
			if (c.getNome().startsWith(letraMauscula)) {
				sugestoes.add(c);
			}
		}
		if (sugestoes.size() == 0 ) {
			mensage.alert("Cidade Não Cadastrada!");
		}
		return sugestoes;
	}

}
