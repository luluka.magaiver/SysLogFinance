package com.br.mvsistemas.sysvendas.dao;

import java.util.List;

import com.br.mvsistemas.sysvendas.model.Cfop;

public interface CfopDao<T> extends GenericDao<T>{
	public List<Cfop> cfopEntrada();
	public List<Cfop> cfopSaida();
}
