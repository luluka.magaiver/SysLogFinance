package com.br.mvsistemas.sysvendas.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;

public interface MovimentoDao<T> extends GenericDao<T>{
	public List<Object[]> comprasDeCliente(Cliente cliente);
	public BigDecimal totalizador(Cliente cliente);
	public List<Object[]> impressaoPedidoDevolucao(Movimento movimento);
}
