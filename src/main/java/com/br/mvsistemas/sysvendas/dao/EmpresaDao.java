package com.br.mvsistemas.sysvendas.dao;

import java.util.List;

import com.br.mvsistemas.sysvendas.model.Empresa;

public interface EmpresaDao<T> extends GenericDao<T>{
	public List<Empresa> listEmpresa();
}
