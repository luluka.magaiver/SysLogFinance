package com.br.mvsistemas.sysvendas.dao;

import com.br.mvsistemas.sysvendas.model.Caixa;

public interface CaixaDao<T> extends GenericDao<T> {
	public Caixa caixaAberto();
}
