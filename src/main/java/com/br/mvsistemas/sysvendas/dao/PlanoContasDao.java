package com.br.mvsistemas.sysvendas.dao;

import com.br.mvsistemas.sysvendas.model.PlanoContas;

public interface PlanoContasDao<T> extends GenericDao<T> {
	public PlanoContas planoContas(String plano);
}
