package com.br.mvsistemas.sysvendas.dao;

import java.time.LocalDate;
import java.util.List;

import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.relatorios.Aniversariantes;

public interface ClienteDao<T> extends GenericDao<T>{
	public List<Cliente> aniversariantes(Integer mes);
	public List<Cliente> clientes();
	public List<Cliente> fornecedores();
}
