package com.br.mvsistemas.sysvendas.dao;

import java.util.List;

import com.br.mvsistemas.sysvendas.model.Estoque;
import com.br.mvsistemas.sysvendas.model.Produto;

public interface ProdutoDao<T> extends GenericDao<T>{
	public Long ultimoIdDoProduto();
	public List<Produto> produtoNoEstoque();
	public Estoque estoque(Produto produto);
	public List<Estoque> produtoEstoque();
	public List<Produto> produtos();
}
