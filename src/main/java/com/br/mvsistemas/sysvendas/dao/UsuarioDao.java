package com.br.mvsistemas.sysvendas.dao;

import com.br.mvsistemas.sysvendas.model.Usuario;

public interface UsuarioDao<T> extends GenericDao<T>{
	public Usuario login(String u, String s);
	
	public Usuario usuario(Usuario usuario);
}
