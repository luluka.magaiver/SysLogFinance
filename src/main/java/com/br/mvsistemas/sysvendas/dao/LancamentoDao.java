package com.br.mvsistemas.sysvendas.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.vo.LancamentoVO;

public interface LancamentoDao<T> extends GenericDao<T> {

	public BigDecimal valorTotalDosLancamento();
	public BigDecimal valorTotalDosLancamento(String baixado);
	public BigDecimal valorTotalVencidos();
	public LancamentoVO valorTotalPorCliente(String baixado, Cliente cliente);
	public List<Lancamento> consultaDeLancamentos(String nome, String documento, Date inicial, Date datefinal, String opcao, String tipoLancamento);
	public boolean isLancamentoNaoBaixado(Movimento movimento);
	public Lancamento buscaLancamento(Long id);
	public List<Lancamento> buscarLancamentos(Long id);
}
