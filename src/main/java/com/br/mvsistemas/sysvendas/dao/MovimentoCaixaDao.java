package com.br.mvsistemas.sysvendas.dao;

import java.util.Date;
import java.util.List;

import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;

public interface MovimentoCaixaDao<T> extends GenericDao<T> {
	public List<MovimentoCaixa> consultaDeMovimentoCaixa(Date dataInicial, Date dataFinal);
}
