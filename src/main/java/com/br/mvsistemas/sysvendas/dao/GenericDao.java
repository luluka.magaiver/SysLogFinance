package com.br.mvsistemas.sysvendas.dao;

import java.io.Serializable;
import java.util.List;

import com.br.mvsistemas.sysvendas.util.FacesMessages;

/**
 * Interface Gererica para Implementação de CRUD
 * @author Marcus Vinicius
 *
 * @param <T>
 */
public interface GenericDao<T> extends Serializable {

	/**
	 * Consultar por ID
	 * @param menu
	 * @return
	 */
	public T read(Long l);
	
	/**
	 * Consulta por Codigo
	 * @param query
	 * @return
	 */
	public T consult(String query);
	
	/**
	 * Listar os Dados
	 * @return
	 */
	public List<T> list();
	
	/**
	 * Lista Customizada
	 * @param script
	 * @return
	 */
	public List<T> listScript(String script);
	
	/**
	 * Criar registro no Banco
	 * @param t
	 * @return
	 */
	public T create(T t, FacesMessages mensagem, String msg);
	
	/**
	 * Alterar registro do Banco
	 * @param t
	 * @return
	 */
	public T alter(T t, FacesMessages mensagem, String msg);
	
	/**
	 * Cria ou Altera no mesmo metodo
	 * @param t
	 * @return
	 */
	public T createAlter(T t, FacesMessages mensagem, String msg);
	
	/**
	 * Excluir registro do Banco
	 * @param t
	 */
	public void delete(T t, FacesMessages mensagem, String msg);
	
	public void beginTransaction();

    public void commit();

    public void rollback();

    public void closeTransaction();
}
