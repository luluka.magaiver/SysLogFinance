package com.br.mvsistemas.sysvendas.dao;

import java.util.List;

import com.br.mvsistemas.sysvendas.model.Cidades;

public interface CidadeDao<T> extends GenericDao<T>{
	public Cidades cidadePorLocalidade(String localidade, String uf);
}
