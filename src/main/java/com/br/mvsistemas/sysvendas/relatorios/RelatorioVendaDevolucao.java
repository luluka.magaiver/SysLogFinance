package com.br.mvsistemas.sysvendas.relatorios;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@WebServlet("/RelatorioVendaDevolucao")
public class RelatorioVendaDevolucao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RelatorioVendaDevolucao() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = (HttpSession) request.getSession();

		@SuppressWarnings("unchecked")
		List<Object[]> impressao = (List<Object[]>) session.getAttribute("movimento");

		List<ImpressaoPedidoDevolucao> vendaDevolucao = new ArrayList<ImpressaoPedidoDevolucao>();
		for (Object[] pedidoDevolucao : impressao) {
			ImpressaoPedidoDevolucao impressaoPedidoDevolucao = new ImpressaoPedidoDevolucao();
			impressaoPedidoDevolucao.setDataMovimento((Date) pedidoDevolucao[0]);
			impressaoPedidoDevolucao.setDescontoMovimento((BigDecimal) pedidoDevolucao[1]);
			impressaoPedidoDevolucao.setFreteMovimento((BigDecimal) pedidoDevolucao[2]);
			impressaoPedidoDevolucao.setNumeroMovimento((String) pedidoDevolucao[3]);
			impressaoPedidoDevolucao.setStatusMovimento((String) pedidoDevolucao[4]);
			impressaoPedidoDevolucao.setTotalMovimento((BigDecimal) pedidoDevolucao[5]);
			impressaoPedidoDevolucao.setNome((String) pedidoDevolucao[6]);
			impressaoPedidoDevolucao.setEmpresaCliente((String) pedidoDevolucao[7]);
			impressaoPedidoDevolucao.setSetorCliente((String) pedidoDevolucao[8]);
			impressaoPedidoDevolucao.setNomeCondicao((String) pedidoDevolucao[9]);
			impressaoPedidoDevolucao.setDescricaoProduto((String) pedidoDevolucao[10]);
			impressaoPedidoDevolucao.setQuantidadeItemMovimento((BigDecimal) pedidoDevolucao[11]);
			impressaoPedidoDevolucao.setValorItemMovimento((BigDecimal) pedidoDevolucao[12]);
			impressaoPedidoDevolucao.setTotalItemMovimento((BigDecimal) pedidoDevolucao[13]);

			vendaDevolucao.add(impressaoPedidoDevolucao);
		}
		
		ServletContext servletContext = getServletContext();
		String diretorio = servletContext.getRealPath("/WEB-INF/relatorios/VendaDevolucao.jasper");
		
		Map<String, Object> param = new HashMap<String, Object>();
		String pathImage = servletContext.getRealPath("") 
				+ File.separator + "resources" 
				+ File.separator 
				+ File.separator + "images" 
				+ File.separator;

		param.put("link_empresa", "http://mvsistema.com");
		param.put("logo", pathImage);
		
		JasperPrint print = null;
		
		try {
			print = JasperFillManager.fillReport(diretorio, param, new JRBeanCollectionDataSource(vendaDevolucao));
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
		
		String aquivoSaida = vendaDevolucao.get(0).getStatusMovimento() + "-" + vendaDevolucao.get(0).getNumeroMovimento();
		
		response.addHeader("Content-Disposition", "attachment; filename="+aquivoSaida+".pdf");
		
		ServletOutputStream outStream = response.getOutputStream();
		try {
			JasperExportManager.exportReportToPdfStream(print, outStream);
		} catch (JRException e) {
			e.printStackTrace();
		}
		outStream.flush();
		outStream.close();
		session.removeAttribute("movimento");
	}

}
