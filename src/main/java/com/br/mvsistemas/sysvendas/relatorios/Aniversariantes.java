package com.br.mvsistemas.sysvendas.relatorios;

import java.io.Serializable;
import java.util.Date;

public class Aniversariantes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7695035349288370274L;

	private Long id;
	private String nome;
	private String email;
	private Date dataNascimento;
	private String empresaCliente;
	private String empresaSetor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmpresaCliente() {
		return empresaCliente;
	}

	public void setEmpresaCliente(String empresaCliente) {
		this.empresaCliente = empresaCliente;
	}

	public String getEmpresaSetor() {
		return empresaSetor;
	}

	public void setEmpresaSetor(String empresaSetor) {
		this.empresaSetor = empresaSetor;
	}

}
