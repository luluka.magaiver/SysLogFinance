package com.br.mvsistemas.sysvendas.relatorios;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.br.mvsistemas.sysvendas.dao.MovimentoDao;
import com.br.mvsistemas.sysvendas.daoimpl.MovimentoDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Movimento;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@WebServlet("/RelatorioComprasDeClientes")
public class RelatorioComprasDeClientes extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MovimentoDao<Movimento> movimentoDao;

	public RelatorioComprasDeClientes() {
		super();
		movimentoDao = new MovimentoDaoImpl();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = (HttpSession) request.getSession();

		@SuppressWarnings("unchecked")
		List<Object[]> compras = (List<Object[]>) session.getAttribute("compras");
		Cliente cliente = (Cliente) session.getAttribute("cliente");
		
		List<CompasDeClientes> comprasClientes = new ArrayList<>();
		for (Object[] compra : compras) {
			CompasDeClientes c = new CompasDeClientes();

			c.setDtVendaPag((Date) compra[1]);
			c.setCliente((String) compra[2]);
			c.setEmpresaCliente((String) compra[3]);
			c.setEmpresaSetor((String) compra[4]);
			c.setQtV((BigDecimal) compra[5]);
			c.setProdutoV((String) compra[6]);
			c.setVlItemV((BigDecimal) compra[7]);
			c.setTotalItemV((BigDecimal) compra[8]);
			c.setQtD((BigDecimal) compra[9]);
			c.setProdutoD((String) compra[10]);
			c.setVlItemD((BigDecimal) compra[11]);
			c.setTotalItemD((BigDecimal) compra[12]);
			c.setVlPago((BigDecimal) compra[13]);
			c.setTotalMov((BigDecimal) compra[14]);

			comprasClientes.add(c);
		}

		BigDecimal valorTotal = movimentoDao.totalizador(cliente);
		
		ServletContext servletContext = getServletContext();
		String diretorio = servletContext.getRealPath("/WEB-INF/relatorios/ComprasClientes.jasper");
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("link_empresa", "http://mvsistema.com");
		param.put("totalizador", valorTotal);
		param.put("empresaCliente", comprasClientes.get(0).getEmpresaCliente());
		param.put("empresaSetor", comprasClientes.get(0).getEmpresaSetor());
		
		String pathImage = servletContext.getRealPath("") 
				+ File.separator + "resources" 
				+ File.separator 
				+ File.separator + "images" 
				+ File.separator;
		//
		param.put("logo", pathImage);
		
		JasperPrint print = null;
		
		try {
			print = JasperFillManager.fillReport(diretorio, param, new JRBeanCollectionDataSource(comprasClientes));
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
		
		response.addHeader("Content-Disposition", "attachment; filename=ComprasClientes"+DateTimeFormat.forPattern("dd-MM-yyyy_HH-mm-ss").print(new DateTime())+".pdf");
		
		ServletOutputStream outStream = response.getOutputStream();
		try {
			JasperExportManager.exportReportToPdfStream(print, outStream);
		} catch (JRException e) {
			e.printStackTrace();
		}
		outStream.flush();
		outStream.close();
		session.removeAttribute("compras");
		session.removeAttribute("cliente");
	}

}
