package com.br.mvsistemas.sysvendas.relatorios;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.br.mvsistemas.sysvendas.controller.ClienteController;
import com.br.mvsistemas.sysvendas.dao.ClienteDao;
import com.br.mvsistemas.sysvendas.daoimpl.ClienteDaoImpl;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.util.FacesMessages;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@WebServlet("/RelatorioAniversariante")
public class RelatorioAniversariante extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ClienteDao<Cliente> clienteDao;

	@Inject
	private FacesMessages mensage;

	public RelatorioAniversariante() {
		super();
		clienteDao = new ClienteDaoImpl();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = (HttpSession) request.getSession();

		@SuppressWarnings("unchecked")
		List<Cliente> aniversarinates = (List<Cliente>) session.getAttribute("aniversariantes");
		int mesAniversario = (int) session.getAttribute("mesAniversariantes");

		List<Aniversariantes> listaAniversario = new ArrayList<>();

		for (Cliente cliente : aniversarinates) {
			Aniversariantes aniversariantes = new Aniversariantes();
			aniversariantes.setId(cliente.getId());
			aniversariantes.setNome(cliente.getNome());
			aniversariantes.setEmail(cliente.getEmail());
			aniversariantes.setDataNascimento(cliente.getDataNascimento());
			aniversariantes.setEmpresaCliente(cliente.getEmpresaCliente());
			aniversariantes.setEmpresaSetor(cliente.getEmpresaSetor());

			listaAniversario.add(aniversariantes);
		}

		try {

			ServletContext servletContext = getServletContext();
			String diretorio = servletContext.getRealPath("/WEB-INF/relatorios/Aniversariantes.jasper");

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("link_empresa", "http://mvsistema.com");
			param.put("mes", tradcao(mesAniversario));

			String pathImage = servletContext.getRealPath("") + File.separator + "resources" + File.separator
					+ File.separator + "images" + File.separator;
			//
			param.put("logo", pathImage);

			JasperPrint print = null;

			try {
				print = JasperFillManager.fillReport(diretorio, param, new JRBeanCollectionDataSource(aniversarinates));
			} catch (Exception e) {
				System.out.println("ERRO: " + e.getMessage());
			}

			response.addHeader("Content-Disposition", "attachment; filename=Aniversariantes"
					+ DateTimeFormat.forPattern("dd-MM-yyyy_HH-mm-ss").print(new DateTime()) + ".pdf");

			ServletOutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(print, outStream);
			outStream.flush();
			outStream.close();
			
			session.removeAttribute("aniversariantes");
		} catch (JRException e) {
			e.printStackTrace();
		}

	}

	private String tradcao(int mes) {
		Calendar cal = GregorianCalendar.getInstance();

		String mesPorExtenso = "";
		switch (mes) {
		case 1:
			mesPorExtenso = "Janeiro " + cal.get(Calendar.YEAR);
			break;
		case 2:
			mesPorExtenso = "Fevereiro " + cal.get(Calendar.YEAR);
			break;
		case 3:
			mesPorExtenso = "Março " + cal.get(Calendar.YEAR);
			break;
		case 4:
			mesPorExtenso = "Abril " + cal.get(Calendar.YEAR);
			break;
		case 5:
			mesPorExtenso = "Maio " + cal.get(Calendar.YEAR);
			break;
		case 6:
			mesPorExtenso = "Junho " + cal.get(Calendar.YEAR);
			break;
		case 7:
			mesPorExtenso = "Julho " + cal.get(Calendar.YEAR);
			break;
		case 8:
			mesPorExtenso = "Agosto " + cal.get(Calendar.YEAR);
			break;
		case 9:
			mesPorExtenso = "Setembro " + cal.get(Calendar.YEAR);
			break;
		case 10:
			mesPorExtenso = "Outubro " + cal.get(Calendar.YEAR);
			break;
		case 11:
			mesPorExtenso = "Novembro " + cal.get(Calendar.YEAR);
			break;
		case 12:
			mesPorExtenso = "Dezembro " + cal.get(Calendar.YEAR);
			break;

		default:
			break;
		}

		return mesPorExtenso;
	}

}
