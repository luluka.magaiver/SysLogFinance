package com.br.mvsistemas.sysvendas.relatorios;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class ImpressaoPedidoDevolucao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 987269528383081068L;
	
	@Getter
	@Setter
	private Date dataMovimento;
	
	@Getter
	@Setter
	private BigDecimal descontoMovimento;
	
	@Getter
	@Setter
	private BigDecimal freteMovimento;
	
	@Getter
	@Setter
	private String numeroMovimento;
	
	@Getter
	@Setter
	private String statusMovimento;
	
	@Getter
	@Setter
	private BigDecimal totalMovimento;
	
	@Getter
	@Setter
	private String nome;
	
	@Getter
	@Setter
	private String empresaCliente;
	
	@Getter
	@Setter
	private String setorCliente;
	
	@Getter
	@Setter
	private String nomeCondicao;
	
	@Getter
	@Setter
	private String descricaoProduto;
	
	@Getter
	@Setter
	private BigDecimal quantidadeItemMovimento;
	
	@Getter
	@Setter
	private BigDecimal valorItemMovimento;
	
	@Getter
	@Setter
	private BigDecimal totalItemMovimento;
	
}
