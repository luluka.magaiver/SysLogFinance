package com.br.mvsistemas.sysvendas.relatorios;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class CompasDeClientes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 686703368939148382L;
	
	@Getter
	@Setter
	private Date dtVendaPag;
	
	@Getter
	@Setter
	private String cliente;
	
	@Getter
	@Setter
	private String empresaCliente;
	
	@Getter
	@Setter
	private String empresaSetor;
	
	@Getter
	@Setter
	private BigDecimal qtV;
	
	@Getter
	@Setter
	private String produtoV;
	
	@Getter
	@Setter
	private BigDecimal vlItemV;
	
	@Getter
	@Setter
	private BigDecimal totalItemV;
	
	@Getter
	@Setter
	private BigDecimal qtD;
	
	@Getter
	@Setter
	private String produtoD;
	
	@Getter
	@Setter
	private BigDecimal vlItemD;
	
	@Getter
	@Setter
	private BigDecimal totalItemD;
	
	@Getter
	@Setter
	private BigDecimal vlPago;
	
	@Getter
	@Setter
	private BigDecimal totalMov;
	
}
