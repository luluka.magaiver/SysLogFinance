package com.br.mvsistemas.sysvendas.daoimpl;

import javax.persistence.NoResultException;

import com.br.mvsistemas.sysvendas.dao.UsuarioDao;
import com.br.mvsistemas.sysvendas.model.Usuario;

public class UsuarioDaoImpl extends GenericDaoImpl<Usuario> implements UsuarioDao<Usuario>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6109972457284223081L;

	public UsuarioDaoImpl() {
		super(Usuario.class);
	}
	
	@Override
	public Usuario login(String u, String s){
		try {
			return (Usuario) manager.createQuery("From Usuario u Where u.cpf = :cpf And u.password = :password").setParameter("cpf", u).setParameter("password", s).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@Override
	public Usuario usuario(Usuario usuario) {
		try {
			manager.getTransaction().begin();
			usuario = (Usuario) manager.merge(usuario);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.getStackTrace();
		} finally {
			manager.close();
		}
		
		return usuario;
	}
	
}
