package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.CidadeDao;
import com.br.mvsistemas.sysvendas.model.Cidades;

public class CidadeDaoImpl extends GenericDaoImpl<Cidades> implements CidadeDao<Cidades>{

	private static final long serialVersionUID = 2763016271291992442L;

	public CidadeDaoImpl() {
		super(Cidades.class);
	}
	
	@Override
	public Cidades cidadePorLocalidade(String localidade, String uf){
		return (Cidades) manager.createQuery("select c from Cidades c join c.estado e where c.nome = :nomeCidade and e.uf = :ufCidade")
								.setParameter("nomeCidade", localidade)
								.setParameter("ufCidade", uf).getSingleResult();
	}

}
