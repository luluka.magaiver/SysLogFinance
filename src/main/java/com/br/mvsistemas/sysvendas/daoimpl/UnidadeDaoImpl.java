package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.UnidadeDao;
import com.br.mvsistemas.sysvendas.model.Unidade;

public class UnidadeDaoImpl extends GenericDaoImpl<Unidade> implements UnidadeDao<Unidade>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5108911781436772175L;

	public UnidadeDaoImpl() {
		super(Unidade.class);
	}

}
