package com.br.mvsistemas.sysvendas.daoimpl;

import java.util.List;

import com.br.mvsistemas.sysvendas.dao.CfopDao;
import com.br.mvsistemas.sysvendas.model.Cfop;

public class CfopDaoImpl extends GenericDaoImpl<Cfop> implements CfopDao<Cfop>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2585759333166076897L;

	public CfopDaoImpl() {
		super(Cfop.class);
	}

	@Override
	public List<Cfop> cfopEntrada() {
		return manager.createQuery("from Cfop c where c.tipoCfop = 'E'").getResultList();
	}

	@Override
	public List<Cfop> cfopSaida() {
		return manager.createQuery("from Cfop c where c.tipoCfop = 'S'").getResultList();
	}

	
}
