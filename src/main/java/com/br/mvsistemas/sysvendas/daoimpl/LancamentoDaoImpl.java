package com.br.mvsistemas.sysvendas.daoimpl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.br.mvsistemas.sysvendas.dao.LancamentoDao;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Lancamento;
import com.br.mvsistemas.sysvendas.model.Movimento;
import com.br.mvsistemas.sysvendas.vo.LancamentoVO;

public class LancamentoDaoImpl extends GenericDaoImpl<Lancamento> implements LancamentoDao<Lancamento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -973833990675538900L;
	

	public LancamentoDaoImpl() {
		super(Lancamento.class);
	}

	@Override
	public LancamentoVO valorTotalPorCliente(String baixado, Cliente cliente) {
		
		return null;
	}

	@Override
	public BigDecimal valorTotalDosLancamento() {
		BigDecimal valor = (BigDecimal) manager.createQuery(" Select sum(l.valor) from Lancamento l").getSingleResult();
		return valor;
	}
	
	@Override
	public BigDecimal valorTotalDosLancamento(String nulo) {
		BigDecimal valor = (BigDecimal) manager.createQuery("Select sum(l.valor) from Lancamento l WHERE l.dataBaixa "+nulo).getSingleResult();
		return valor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Lancamento> consultaDeLancamentos(String nome, String documento, Date inicial, 
			Date datefinal, String opcao, String tipoLancamento) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "From Lancamento l where 1=1";
		
		if (!StringUtils.isEmpty(tipoLancamento)) {
			if (tipoLancamento.equals("P")) {
				sql +=" and l.tipoLancamento = '"+tipoLancamento+"'";
			} else if(tipoLancamento.equals("R")){
				sql +=" and l.tipoLancamento = '"+tipoLancamento+"'";
			} 
		}

		if (!StringUtils.isEmpty(nome)) {
			sql += " and l.cliente.nome like '%" + nome + "%'";
		}

		if (!StringUtils.isEmpty(documento)) {
			sql += " and l.documento like '%" + documento + "%'";
		}

		if (inicial != null && datefinal != null) {
			if (opcao.equals("1")) {

				sql += " and DATE_FORMAT(l.dataOperacao,'%Y-%m-%d') between '" + sdf.format(inicial) + "' and '"
						+ sdf.format(datefinal) + "'";
			}

			if (opcao.equals("2")) {
				sql += " and DATE_FORMAT(l.dataVencimento,'%Y-%m-%d') between '" + sdf.format(inicial) + "' and '"
						+ sdf.format(datefinal) + "'";
			}
		}
		
		if (inicial != null && datefinal == null) {
			if (opcao.equals("1")) {

				sql += " and DATE_FORMAT(l.dataOperacao,'%Y-%m-%d') >= '" + sdf.format(inicial) + "'";
			}

			if (opcao.equals("2")) {
				sql += " and DATE_FORMAT(l.dataVencimento,'%Y-%m-%d') >= '" + sdf.format(inicial) + "'";
			}
		}
		
		if (inicial == null && datefinal != null) {
			if (opcao.equals("1")) {

				sql += " and DATE_FORMAT(l.dataOperacao,'%Y-%m-%d') <= '" + sdf.format(datefinal) + "'";
			}

			if (opcao.equals("2")) {
				sql += " and DATE_FORMAT(l.dataVencimento,'%Y-%m-%d') <= '" + sdf.format(datefinal) + "'";
			}
		}
		
		return manager.createQuery(sql).getResultList();
	}

	@Override
	public BigDecimal valorTotalVencidos() {
		Calendar hoje = Calendar.getInstance();
		BigDecimal valoresVencidos = BigDecimal.ZERO;
		for (Lancamento l : list()) {
			if (l.getDataVencimento().before(hoje.getTime())) {
				if (l.getDataBaixa() == null) {
					valoresVencidos = valoresVencidos.add(l.getValor());
				}
			}
		}
		return valoresVencidos;
	}

	@Override
	public boolean isLancamentoNaoBaixado(Movimento movimento) {
		Lancamento titulo = (Lancamento) manager.createQuery("Select l From Lancamento l where l.movimento.id = "+movimento.getId()).getSingleResult();
		return titulo.getDataBaixa() == null;
	}

	@Override
	public Lancamento buscaLancamento(Long id) {
		return (Lancamento) manager.createQuery("Select l From Lancamento l where l.movimento.id = :id")
				.setParameter("id", id)
				.getSingleResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Lancamento> buscarLancamentos(Long id){
		return (List<Lancamento>) manager.createQuery("Select l From Lancamento l where l.movimento.id = :id")
				.setParameter("id", id)
				.getResultList();
	}

}
