package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.CondicaoPagamentoDao;
import com.br.mvsistemas.sysvendas.model.CondicaoPagamento;

public class CondicaoPagamentoDaoImpl extends GenericDaoImpl<CondicaoPagamento> implements CondicaoPagamentoDao<CondicaoPagamento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1813619132992377263L;

	public CondicaoPagamentoDaoImpl() {
		super(CondicaoPagamento.class);
	}

}
