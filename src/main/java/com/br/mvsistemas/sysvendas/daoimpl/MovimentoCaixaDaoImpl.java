package com.br.mvsistemas.sysvendas.daoimpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.br.mvsistemas.sysvendas.dao.MovimentoCaixaDao;
import com.br.mvsistemas.sysvendas.model.MovimentoCaixa;

public class MovimentoCaixaDaoImpl extends GenericDaoImpl<MovimentoCaixa> implements MovimentoCaixaDao<MovimentoCaixa> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7184300768566265501L;

	public MovimentoCaixaDaoImpl() {
		super(MovimentoCaixa.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovimentoCaixa> consultaDeMovimentoCaixa(Date dataInicial, Date dataFinal) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "From MovimentoCaixa mc where 1=1";

		if (dataInicial != null && dataFinal != null) {
			sql += " and DATE_FORMAT(mc.hora,'%Y-%m-%d') between '" + sdf.format(dataInicial) + "' and '"
					+ sdf.format(dataFinal) + "'";
		}

		if (dataInicial != null && dataFinal == null) {
			sql += " and DATE_FORMAT(mc.hora,'%Y-%m-%d') >= '" + sdf.format(dataInicial) + "'";
		}

		if (dataInicial == null && dataFinal != null) {
			sql += " and DATE_FORMAT(mc.hora,'%Y-%m-%d') <= '" + sdf.format(dataFinal) + "'";
		}

		return manager.createQuery(sql).getResultList();
	}

}
