package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.PlanoContasDao;
import com.br.mvsistemas.sysvendas.model.PlanoContas;

public class PlanoContasDaoImpl extends GenericDaoImpl<PlanoContas> implements PlanoContasDao<PlanoContas> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1097530605806278470L;

	public PlanoContasDaoImpl() {
		super(PlanoContas.class);
	}

	@Override
	public PlanoContas planoContas(String plano) {
		return (PlanoContas) manager.createQuery("Select p From PlanoContas p Where p.descricaoCompleta = :descricaoCompleta")
				.setParameter("descricaoCompleta", plano).getSingleResult();
	}

}
