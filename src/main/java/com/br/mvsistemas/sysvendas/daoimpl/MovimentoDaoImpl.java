package com.br.mvsistemas.sysvendas.daoimpl;

import java.math.BigDecimal;
import java.util.List;

import com.br.mvsistemas.sysvendas.dao.MovimentoDao;
import com.br.mvsistemas.sysvendas.model.Cliente;
import com.br.mvsistemas.sysvendas.model.Movimento;

public class MovimentoDaoImpl extends GenericDaoImpl<Movimento> implements MovimentoDao<Movimento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5463143766108117424L;

	public MovimentoDaoImpl() {
		super(Movimento.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> comprasDeCliente(Cliente cliente) {
		if (cliente != null) {
			String sql = ""
					+ "SELECT l.id_lancamento AS dtMov, "
					+ "       l.data_baixa AS dtVendaPag, "
					+ "       '' AS cliente, "
					+ "       '' AS empresaCliente, "
					+ "       '' AS empresaSetor, "
					+ "       0.00 AS qtV, "
					+ "       '' AS produtoV, "
					+ "       0.00 AS vlItemV, "
					+ "       0.00 AS totalItemV, "
					+ "       0.00 AS qtD, "
					+ "       '' AS produtoD, "
					+ "       0.00 AS vlItemD, "
					+ "       0.00 AS totalItemD, "
					+ "       l.valor_pago AS vlPago, "
					+ "       (m.total_movimento - l.valor_pago) AS totalMov "
					+ "FROM lancamento l "
					+ "INNER JOIN movimento m ON l.id_movimento = m.id_movimento "
					+ "WHERE l.data_baixa IS NOT NULL "
					+ "  AND m.id_cliente = "+cliente.getId()
					+ "	UNION DISTINCT "
					+ "SELECT m.id_movimento AS dtMov, "
					+ "       m.data_movimento AS dtVendaPag, "
					+ "       c.nome AS cliente, "
					+ "       c.empresa_cliente AS empresaCliente, "
					+ "       c.empresa_setor AS empresaSetor, "
					+ "       IF(m.status = 'V',i.quantidade_item_movimento,0.00) AS qtV, "
					+ "       IF(m.status = 'V',p.descricao_produto,'') AS produtoV, "
					+ "       IF(m.status = 'V',i.valor_item_movimento,0.00) AS vlItemV, "
					+ "       IF(m.status = 'V',i.total_item_movimento,0.00) AS totalItemV, "
					+ "       IF(m.status = 'D',i.quantidade_item_movimento,0.00) AS qtD, "
					+ "       IF(m.status = 'D',p.descricao_produto,'') AS produtoD, "
					+ "       IF(m.status = 'D',i.valor_item_movimento,0.00) AS vlItemD, "
					+ "       IF(m.status = 'D',i.total_item_movimento,0.00) AS totalItemD, "
					+ "       0.00 AS vlPago, "
					+ "       m.total_movimento AS totalMov "
					+ "FROM lancamento l "
					+ "INNER JOIN movimento m ON l.id_movimento = m.id_movimento AND l.id_cliente = m.id_cliente "
					+ "INNER JOIN cliente c ON m.id_cliente = c.id_cliente AND l.id_cliente = c.id_cliente "
					+ "INNER JOIN item_movimento i ON m.id_movimento = i.id_movimento "
					+ "INNER JOIN produto p ON i.id_produto = p.id_produto "
					+ "WHERE m.data_movimento IS NOT NULL "
					+ "  AND m.id_cliente = "+cliente.getId()
					+ "	ORDER BY dtVendaPag";

			System.out.println(sql);

			return (List<Object[]>) manager.createNativeQuery(sql).getResultList();
		} else {
			return null;
		}
		
	}

	@Override
	public BigDecimal totalizador(Cliente cliente) {
		BigDecimal valor = (BigDecimal) manager.createNativeQuery("SELECT (SUM(l.valor) - SUM(l.valor_pago)) from lancamento l Where l.id_cliente = :idcliente")
				.setParameter("idcliente", cliente)
				.getSingleResult();
		return valor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> impressaoPedidoDevolucao(Movimento movimento) {
		String  sql = ""
		+"select  m.data_movimento, "
		+"		m.desconto_movimento, "
		+"		m.frete_movimento, "
		+"		m.numero_movimento, "
		+"		if(m.status = 'V', 'Venda', 'Devolução') as StatusVenda, "
		+"		m.total_movimento, "
		+"		c.nome, "
		+"		c.empresa_cliente, "
		+"		c.empresa_setor, "
		+"		cp.nome_condicao, "
		+"		p.descricao_produto, "
		+"		im.quantidade_item_movimento, "
		+"		im.valor_item_movimento, "
		+"		im.total_item_movimento "
		+" from movimento m "
		+" 	inner join cliente c "
		+" 		on m.id_cliente = c.id_cliente "
		+" 	inner join condicao_pagamento cp "
		+" 		on m.id_condicao_pagamento = cp.id_condicao_pagamento "
		+" 	inner join item_movimento im "
		+" 		on m.id_movimento = im.id_movimento "
		+" 	inner join produto p "
		+" 		on p.id_produto = im.id_produto "
		+" where m.numero_movimento = :numero";
		
		return (List<Object[]>) manager.createNativeQuery(sql).setParameter("numero", movimento.getNumeroMovimento()).getResultList();
	}

}
