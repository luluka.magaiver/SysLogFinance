package com.br.mvsistemas.sysvendas.daoimpl;

import java.util.List;

import com.br.mvsistemas.sysvendas.dao.ClienteDao;
import com.br.mvsistemas.sysvendas.model.Cliente;

public class ClienteDaoImpl extends GenericDaoImpl<Cliente> implements ClienteDao<Cliente>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3713035964324320668L;

	public ClienteDaoImpl() {
		super(Cliente.class);
	}

	
	@SuppressWarnings("unchecked")
	public List<Cliente> aniversariantes(Integer mes){
		return (List<Cliente>) manager.createNativeQuery("Select * From cliente Where MONTH(data_nascimento) = :mes Order By date_format(data_nascimento, '%d') ASC",Cliente.class)
				.setParameter("mes", mes).getResultList();
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> clientes() {
		return (List<Cliente>) manager.createQuery("Select c From Cliente c where c.clienteFornecedor = 'C'").getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> fornecedores() {
		return (List<Cliente>) manager.createQuery("Select c From Cliente c where c.clienteFornecedor = 'F'").getResultList();
	}
	
	
}
