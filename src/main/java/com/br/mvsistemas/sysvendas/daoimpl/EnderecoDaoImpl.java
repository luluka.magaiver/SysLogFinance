package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.EnderecoDao;
import com.br.mvsistemas.sysvendas.model.Endereco;

public class EnderecoDaoImpl extends GenericDaoImpl<Endereco> implements EnderecoDao<Endereco> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5059975123009734028L;

	public EnderecoDaoImpl() {
		super(Endereco.class);
	}

}
