package com.br.mvsistemas.sysvendas.daoimpl;

import java.util.List;

import com.br.mvsistemas.sysvendas.dao.ProdutoDao;
import com.br.mvsistemas.sysvendas.model.Estoque;
import com.br.mvsistemas.sysvendas.model.Produto;

public class ProdutoDaoImpl extends GenericDaoImpl<Produto> implements ProdutoDao<Produto>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7839473886591578L;

	public ProdutoDaoImpl() {
		super(Produto.class);
	}

	@Override
	public Long ultimoIdDoProduto(){
		Long bigInteger = (Long) manager.createQuery("Select MAX(p.id) From Produto p").getSingleResult();
		if (bigInteger == null) {
			return null;
		} else {
			return bigInteger;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> produtoNoEstoque() {
		return manager.createQuery("Select p From Produto p join Estoque e on p.id = e.produto.id").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> produtos() {
		return manager.createQuery("Select p From Produto p").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Estoque> produtoEstoque() {
		return manager.createQuery("Select e From Estoque e").getResultList();
	}

	@Override
	public Estoque estoque(Produto produto) {
		return (Estoque) manager.createQuery("From Estoque e where e.produto.id = :id").setParameter("id", produto.getId()).getSingleResult();
	}
	
}
