package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.MenuDao;
import com.br.mvsistemas.sysvendas.model.Menu;

public class MenuDaoImpl extends GenericDaoImpl<Menu> implements MenuDao<Menu> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3778025594758492968L;
	
	public MenuDaoImpl() {
		super(Menu.class);
	}

}
