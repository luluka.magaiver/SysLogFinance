package com.br.mvsistemas.sysvendas.daoimpl;

import java.util.List;

import com.br.mvsistemas.sysvendas.dao.EmpresaDao;
import com.br.mvsistemas.sysvendas.model.Empresa;

public class EmpresaDaoImpl extends GenericDaoImpl<Empresa> implements EmpresaDao<Empresa> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3651437129596595511L;

	public EmpresaDaoImpl() {
		super(Empresa.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Empresa> listEmpresa(){
		return manager.createNativeQuery("SELECT id_empresa,cnpj,email,ie,SUBSTRING(nome_fantasia,1,20) as nome_fantasia, razao_social,tipo_empresa,id_endereco from EMPRESA").getResultList();
	}
}
