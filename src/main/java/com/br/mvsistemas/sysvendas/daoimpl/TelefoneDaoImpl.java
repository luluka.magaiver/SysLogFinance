package com.br.mvsistemas.sysvendas.daoimpl;

import com.br.mvsistemas.sysvendas.dao.TelefoneDao;
import com.br.mvsistemas.sysvendas.model.Telefone;

public class TelefoneDaoImpl extends GenericDaoImpl<Telefone> implements TelefoneDao<Telefone> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8788967853457971792L;

	public TelefoneDaoImpl() {
		super(Telefone.class);
	}

}
