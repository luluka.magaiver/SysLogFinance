package com.br.mvsistemas.sysvendas.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.br.mvsistemas.sysvendas.dao.GenericDao;
import com.br.mvsistemas.sysvendas.util.FacesMessages;
import com.br.mvsistemas.sysvendas.util.SysVendasUtil;

import antlr.StringUtils;

/**
 * Classe Generica CRUD
 * 
 * @author Marcus Vinicius
 *
 * @param <T>
 */
public class GenericDaoImpl<T> implements GenericDao<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6130511775950781237L;

	protected static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysLogFinancePU");
	public EntityManager manager;
	private Class<T> clazz;

	public GenericDaoImpl(Class<T> clazz) {
		this.clazz = clazz;
		manager = emf.createEntityManager();
	}

	@Override
	public T read(Long l) {
		return manager.find(clazz, l);
	}

	@Override
	public List<T> list() {
		return manager.createQuery("from " + clazz.getName(), clazz).getResultList();
	}

	@Override
	public List<T> listScript(String script) {
		return manager.createQuery(script, clazz).getResultList();
	}

	@Override
	public void beginTransaction() {
		if (!manager.getTransaction().isActive()) {

			manager = emf.createEntityManager();
		}
		manager.getTransaction().begin();

	}

	@Override
	public void commit() {
		if (manager.getTransaction().isActive()) {
			manager.getTransaction().commit();
		}
	}

	@Override
	public void rollback() {
		if (manager.getTransaction().isActive()) {
			manager.getTransaction().rollback();
			return;
		}
	}

	@Override
	public void closeTransaction() {
		manager.close();
	}

	@Override
	public T create(T t, FacesMessages mensagem, String msg) {
		try {
			beginTransaction();
			manager.persist(t);
			commit();
			mensagem.info(t.getClass().getSimpleName() + msg);
		} catch (Exception e) {
			rollback();
			mensagem.error("Erro ao Salvar! \n" + e.getMessage());
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public T alter(T t, FacesMessages mensagem, String msg) {
		try {
			beginTransaction();
			manager.merge(t);
			commit();
			if (SysVendasUtil.isStringUsable(msg)) {
				mensagem.alert(t.getClass().getSimpleName() + msg);
			}
		} catch (Exception e) {
			rollback();
			mensagem.error("Erro ao Alterar! \n" + e.getMessage());
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public T createAlter(T t, FacesMessages mensagem, String msg) {
		try {
			beginTransaction();
			t = manager.merge(t);
			commit();
			if (SysVendasUtil.isStringUsable(msg)) {
				mensagem.info(t.getClass().getSimpleName() + msg);
			}
		} catch (Exception e) {
			rollback();
			mensagem.error("Erro ao Criar! \n" + e.getMessage());
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public void delete(T t, FacesMessages mensagem, String msg) {
		try {
			beginTransaction();
			t = manager.merge(t);
			manager.remove(t);
			commit();
			mensagem.alert(t.getClass().getSimpleName() + msg);
		} catch (Exception e) {
			rollback();
			mensagem.error("Erro ao Deletar! \n" + e.getMessage());
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T consult(String query) {
		return (T) manager.createNativeQuery(query, clazz);
	}
}
