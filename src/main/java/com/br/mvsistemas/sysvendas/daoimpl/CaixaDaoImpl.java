package com.br.mvsistemas.sysvendas.daoimpl;

import javax.persistence.NoResultException;

import com.br.mvsistemas.sysvendas.dao.CaixaDao;
import com.br.mvsistemas.sysvendas.model.Caixa;

public class CaixaDaoImpl extends GenericDaoImpl<Caixa> implements CaixaDao<Caixa> {

	private static final long serialVersionUID = -3275982976953556826L;

	public CaixaDaoImpl() {
		super(Caixa.class);
	}

	@Override
	public Caixa caixaAberto() {
		try {
			return (Caixa) manager.createNativeQuery("select * from caixa where aberto = 'A'", Caixa.class).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
