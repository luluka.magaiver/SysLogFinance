package com.br.mvsistemas.sysvendas.dto;

import java.util.Date;

public class MovimentoCaixaDTO {
	
	private Date dateIncial;
	private Date dateFinal;
	
	public Date getDateIncial() {
		return dateIncial;
	}
	public void setDateIncial(Date dateIncial) {
		this.dateIncial = dateIncial;
	}
	public Date getDateFinal() {
		return dateFinal;
	}
	public void setDateFinal(Date dateFinal) {
		this.dateFinal = dateFinal;
	}
}
