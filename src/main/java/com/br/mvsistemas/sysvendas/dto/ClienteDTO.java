package com.br.mvsistemas.sysvendas.dto;

public class ClienteDTO {

	private String nome;
	private String cpfCnpj;
	private String emrpesa;
	private String setor;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmrpesa() {
		return emrpesa;
	}

	public void setEmrpesa(String emrpesa) {
		this.emrpesa = emrpesa;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

}
