package com.br.mvsistemas.sysvendas.dto;

import java.util.Date;

public class MovimentoDTO {
	
	private String nomeCliente;
	private String numeroVenda;
	private Date dateIncial;
	private Date dateFinal;
	
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getNumeroVenda() {
		return numeroVenda;
	}
	public void setNumeroVenda(String numeroVenda) {
		this.numeroVenda = numeroVenda;
	}
	public Date getDateIncial() {
		return dateIncial;
	}
	public void setDateIncial(Date dateIncial) {
		this.dateIncial = dateIncial;
	}
	public Date getDateFinal() {
		return dateFinal;
	}
	public void setDateFinal(Date dateFinal) {
		this.dateFinal = dateFinal;
	}
}
