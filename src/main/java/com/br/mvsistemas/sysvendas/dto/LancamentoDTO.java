package com.br.mvsistemas.sysvendas.dto;

import java.util.Date;

public class LancamentoDTO {

	private String nomeCliente;
	private String documento;
	private Date dateIncial;
	private Date dateFinal;
	private String opcao;
	private String tipoLancamento;
	
	public String getNomeCliente() {
		return nomeCliente;
	}
	
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	public String getDocumento() {
		return documento;
	}
	
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	public Date getDateIncial() {
		return dateIncial;
	}
	
	public void setDateIncial(Date dateIncial) {
		this.dateIncial = dateIncial;
	}
	
	public Date getDateFinal() {
		return dateFinal;
	}
	
	public void setDateFinal(Date dateFinal) {
		this.dateFinal = dateFinal;
	}
	
	public String getOpcao() {
		return opcao;
	}
	
	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}
	
	public String getTipoLancamento() {
		return tipoLancamento;
	}
	
	public void setTipoLancamento(String tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
}
