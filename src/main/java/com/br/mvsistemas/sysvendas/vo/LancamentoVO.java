package com.br.mvsistemas.sysvendas.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.br.mvsistemas.sysvendas.model.Cliente;

/**
 * Calsse resposavel por gera valores de lancamento por clientes
 * SysLogFinance - 2017
 * @author Marcus Vinicius G. Olievira - Desenvolvedor Java
 * 25 de jan de 2017
 */
public class LancamentoVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3264973218039605271L;
	
	private Cliente cliente;
	private BigDecimal valor;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
